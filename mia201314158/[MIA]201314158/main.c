#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <string.h>
#include "stdio.h"
#include <time.h>
//#include "exec.h"
#define TRUE 1

//STRUCTS
typedef struct{
    char path[200];
    char id[10];
    char name[100];
    int start;
    struct registro *siguiente;
    struct registro *anterior;
}registro;
typedef struct{
    registro *primero;
    registro *ultimo;
    int tam;
}tabla;
typedef struct{
    int numero;
    struct nodolista *siguiente;
    struct nodolista *anterior;
}nodolista;
typedef struct{
   nodolista *primero;
   nodolista *ultimo;
    int tam;
}lista;
typedef struct{
    int part_status;
    char part_fit[100];
    int part_start;
    int part_size;
    int part_next;
    char part_name[25];
    struct nodolistaebr *siguiente;
    struct nodolistaebr *anterior;
}nodolistaebr;
typedef struct{
    nodolistaebr *primero;
    nodolistaebr *ultimo;
    int tam;
}listaebr;
typedef struct{
    int mbr_tamanio;
    char mbr_fecha_creacion[128];
    int mbr_disk_signature;
    int  part_status;
    char part_type[100];
    char part_fit[100];
    int  part_start;
    int  part_size;
    char part_name[25];
    int  existeebr;
    int  EsMbr;
    struct reportedisk *siguiente;
    struct reportedisk *anterior;
}reportedisk;
typedef struct{
    reportedisk *primero;
    reportedisk *ultimo;
    int tam;
}listareportedisk;

typedef struct{
    char journal_tipo_operacion[20];
    int jornal_tipo;
    char jornal_nombre[200];
    char journal_contenido[200];
    char jurnal_fecha[20];
    char journal_propietario[20];
    char journal_permisos[12];
    char particion[10];
    char disco[100];
    char comando[200];
    int activo;
    struct nodojournal *siguiente;
    struct nodojournal *anterior;
}nodojournal;

typedef struct{
    nodojournal*primero;
    nodojournal*ultimo;
    int tam;
}listajournal;







typedef struct{
    int  part_status;
    char part_type[100];
    char part_fit[100];
    int  part_start;
    int  part_size;
    char part_name[25];
}partition;
typedef struct{
    int mbr_tamanio;
    char mbr_fecha_creacion[128];
    int mbr_disk_signature;
    partition mbr_partition1;
    partition mbr_partition2;
    partition mbr_partition3;
    partition mbr_partition4;
}mbr;
typedef struct{
    int part_status;
    char part_fit[100];
    int part_start;
    int part_size;
    int part_next;
    char part_name[25];
}ebr;


//STRUCTS PARA SISTEMA DE ARCHIVOS EXT2/EXT3
typedef struct{
    int s_filesytem_type;
    int s_inodes_count;
    int s_blocks_count;
    int s_free_blocks_count;
    int s_free_inodes_count;
    char s_mtime[20];
    char s_utime[20];
    int s_mnt_count;
    char s_magic[20];
    int s_inode_size;
    int s_block_size;
    int s_first_ino;
    int s_first_blo;
    int s_bm_inode_start;
    int s_bm_block_start;
    int s_inode_start;
    int s_block_start;
    int ext3;
}super_bloque;

typedef struct{
    char journal_tipo_operacion[20];
    int jornal_tipo;
    char jornal_nombre[200];
    char journal_contenido[200];
    char jurnal_fecha[20];
    char journal_propietario[20];
    char journal_permisos[12];
    //EXTRAS PARA MANEJAR LA RESTAURACION
    char particion[10];
    char disco[100];
    char comando[200];
    int activo;

}journaling;

typedef struct{
    int i_link;
    char i_pathlink[100];
    int i_ui;
    int i_gid;
    int i_size;
    char i_time[20];
    char i_ctime[20];
    char i_mtime[20];
    int i_block[15];
    int i_type;
    int i_perm;
    int num_inodo;
}inodo;
typedef struct{
    char b_name [12];
    int b_nodo;
}content;

typedef struct{
    int num_bloque;
    content b_content [4];
    char esbloque[4];
}bloque_carpeta;

typedef struct{
    char esbloque[4];
    int num_bloque;
    char b_content[64];
}bloque_archivo;

typedef struct{
    char esbloque[4];
    int num_bloque;
    int b_pointers[16];
}bloque_apuntador;



//VARIABLES GLOBALES
listajournal*lj;
lista* listanumeros;
tabla *re;
listareportedisk * reportedisk1;
listaebr *listalogicas;
int logsumasize=0;
int lognombreexiste=0;
int globalcu=0;
char* comprobarcadena[200];
char ver_id[200];
char *we;
int numcirr;
int EsExec=0;
int tam_logica=-1;
char fecha[128];
int actual;
int padre;
int in=-1;

int main(){

//INICIAR TODAS LAS ESTRUCTURAS
    lj=(listajournal*)malloc(sizeof(listajournal));
    lj->primero=(listajournal*)malloc(sizeof(listajournal));
    lj->ultimo=(listajournal*)malloc(sizeof(listajournal));
    lj->primero=NULL;
    lj->ultimo=NULL;
    lj->tam=0;

    reportedisk1=(listareportedisk*)malloc(sizeof(listareportedisk));
    reportedisk1->primero=(listareportedisk*)malloc(sizeof(listareportedisk));
    reportedisk1->ultimo=(listareportedisk*)malloc(sizeof(listareportedisk));
    reportedisk1->primero=NULL;
    reportedisk1->ultimo=NULL;
    reportedisk1->tam=0;

    listalogicas=(listaebr*)malloc(sizeof(listaebr));
    listalogicas->primero=(listaebr*)malloc(sizeof(listaebr));
    listalogicas->ultimo=(listaebr*)malloc(sizeof(listaebr));
    listalogicas->primero=NULL;
    listalogicas->ultimo=NULL;
    listalogicas->tam=0;

    re = (tabla *)malloc(sizeof(tabla));
    re->primero = (registro *)malloc(sizeof(registro));
    re->ultimo = (registro *)malloc(sizeof(registro));
    re->primero = NULL;
    re->ultimo = NULL;
    re->tam=0;

    listanumeros =(lista*)malloc(sizeof(lista));
    listanumeros->primero =(lista*)malloc(sizeof(lista));
    listanumeros->ultimo =(lista*)malloc(sizeof(lista));
    listanumeros->primero=NULL;
    listanumeros->ultimo=NULL;
    listanumeros->tam=0;

//FINN INICAR LAS ESTRUCTURAS
    char cadena[300];


    printf("Ingrese Orden\n");
int ad=0;
    while(TRUE==1){


      fgets(cadena,300,stdin);
           char *quitarsalto=strtok(cadena,"\n\t");
            strcpy(cadena,quitarsalto);
            leerComandos(cadena);







    }

  return 0;
}


// INICIO METODOS FASE 1
int leerComandos(char *cadena[300]){
   char CadenaSalto[200];
    char Comando[300];
    strcpy(CadenaSalto,"");

if(strstr(cadena,"#")){
            return -1;
        }

        int HaySalto =0;

        while(strstr(cadena,"\\")){
            char *token1 =strtok(cadena,"\\");
            strcat(CadenaSalto,token1);
            strcat(CadenaSalto," ");
            fgets(cadena,300,stdin);
            HaySalto=1;
        }

        if(HaySalto==1){
            strcpy(Comando,CadenaSalto);
            strcat(Comando," ");
            strcat(Comando,cadena);
        }else{
            strcpy(Comando,cadena);
        }

        strcpy(cadena,"");
        strcpy(CadenaSalto,"");

       //COMANDO YA TIENE LA CADENA COMPLETA;

      // printf("\n-----------------------------CADENA COMPLETA:   %s\n",Comando);

       char *Minuscula;
       for(Minuscula = Comando; *Minuscula; Minuscula++){
            *Minuscula = tolower((unsigned char)*Minuscula);
            }


        // LA CADENA COMANDO YA ESTA TODA EN MINUSCULAS
        strcpy(comprobarcadena,Comando);
        FILE* backup=fopen("bup.txt","w");
        char envia[200];
        strcpy(envia,Comando);
        fprintf(backup,"%s",envia);
        fclose(backup);
        FILE* backup2=fopen("bup2.txt","w");
        char envia2[200];
        strcpy(envia2,Comando);
        fprintf(backup2,"%s",envia2);
        fclose(backup2);

        char *token =strtok(Comando," ");

        //TOKEN CONTIENE LA PRIMERA ORDEN

        //ACONTINUACION SE LLAMA A EL METODO SEGUN LA ORDEN

        if(strcmp(token,"mkdisk")==0){
            mkdisk(token);
           /// printf("-------PASO-----\n");
        }
        else if(strcmp(token,"rmdisk")==0){
            rmdisk(token);
        }
        else if(strcmp(token,"fdisk")==0){

            fdisk(token);

        }
        else if(strcmp(token,"mount")==0){

            mount(token);

        }
        else if(strcmp(token,"umount")==0){
            umount(token);
        }
        else if(strcmp(token,"rep")==0){
            rep(token);
        }
        else if(strcmp(token,"mkfs")==0){

            mkfs(token);

        }else if(strcmp(token,"mkdir")==0){
            mkdir1(token,"","",0,0,0);
        }
        else if(strcmp(token,"mkfile")==0){
                    mkfile(token);
        }else if(strcmp(token,"cat")==0){
            cat(token);
        }else if(strcmp(token,"rm")==0){
                    rm(token);
        }else if(strcmp(token,"ren")==0){
            ren(token);
        }
        else if(strcmp(token,"tune2fs")==0){
                    tune2f(token);
        }
        else if(strcmp(token,"loss")==0){
                    loss(token);
        }
        else if(strcmp(token,"recovery")==0){
                    recovery(token);
        }
        else if(strcmp(token,"exec")==0){

            exec(token);

        }
          else if(strcmp(cadena,"\n")==0){ }
        return 0;
}
int exec (char *token){
if(strstr(comprobarcadena,"\"/") && strstr(comprobarcadena,".sh\"")){
    }else{printf("\nError al directorio compruebe la extension .sh\n"); return -1;}

    char path[150]="";
    int veces=0;

    while(token!=NULL){
       // printf(" ejecucion1-------------------------------%s \n",token);

        token=strtok(NULL,":\"\n");
      //  printf(" ejecucion2-------------------------------%s \n",token);
        if(token==NULL){
            break;
        }
        strcpy(path,token);
        veces++;

    }

     FILE *file1;

    char *c1;
    char cadena[200]="";


    file1=fopen(path,"r");

    if(file1 ==NULL){
        printf("\nArchivo de Script inexistente\n");
        return -1;
    }

    while(c1=fgets(cadena,200,file1)){

    /***********************************************************************************************************/
        int  HaySalto=0;
        char CadenaSalto[200];
        char Comando1[200];

        strcpy(CadenaSalto,"");

        while(strstr(cadena,"\\")){
            char *token1 =strtok(cadena,"\\");
            strcat(CadenaSalto,token1);
            strcat(CadenaSalto," ");
            fgets(cadena,200,file1);
            HaySalto=1;
        }

        if(HaySalto==1){
            strcat(CadenaSalto,cadena);
            strcat(cadena,CadenaSalto);
            //strcat(Comando,cadena);
        }

        strcpy(CadenaSalto,"");

    /*********************************************************************************************************************************/



    if(strstr(cadena,"-")||strstr(cadena,"m")||strstr(cadena,"i")||strstr(cadena,"u")||strstr(cadena,"s")||strstr(cadena,"+")||strstr(cadena,"f")){}
    else{break;}
       printf("%s\n",cadena);
       char *quitarsalto=strtok(cadena,"\n\t");

        strcpy(cadena,quitarsalto);

        EsExec==1;
       // printf("COMANDOS LEIDOS!!!!!!--%s\n",cadena);
       leerComandos(cadena);

        EsExec=0;
    }
       fclose(file1);

return 0;
}































int mkdisk(char*token){
if(strstr(comprobarcadena,"\"/") || strstr(comprobarcadena,"/\"")){
    }else{printf("\nError en el comando\n"); return -1;}

char size[20]="-size";
char unit[20]="+unit";
char path[20]="-path";
char name[20]="-name";

//COMPROBANDO QUE TIENE LOS PARAMETROS OBLIGATORIOS
//printf("LA CADENA ES %s\n",comprobarcadena);
if(strstr(comprobarcadena,size) &&strstr(comprobarcadena,name)&& strstr(comprobarcadena,path) && strstr(comprobarcadena,".dsk\"")&& strstr(comprobarcadena,"/\"")&& strstr(comprobarcadena,"\"/home")){
//printf("PROBANDO UNO\n");
}else{
printf("\nComando NO cuenta con parametros minimos o mal escritos\n");
return -1;
}


char comillas[2]="\"";
int control=0;
int tamanioDeDisco;
char unitletra[5]="m";
char Nombre[200];
char dpth[200];
char dpth2[200];
char dpth3[200];
while(token !=NULL){

    token = strtok(NULL," ::");
    if(token==NULL) break;

    char *estaSize;
    char *estaUnit;
    char *estaPath;
    char *estaName;

    estaSize = strstr(token,size);
    estaUnit = strstr(token,unit);
    estaPath = strstr(token,path);
    estaName = strstr(token,name);

    if(estaSize) control=1;
    if(estaUnit) control=2;
    if(estaPath) control=3;
    if(estaName) control=4;

    switch(control){
    case 1:
        token = strtok(NULL," :");
        tamanioDeDisco=atoi(token);
        if(tamanioDeDisco <=9){
            printf("\nTamaño De Disco Invalido\n");
         return -1;
        }
        break;
     case 2:
        token= strtok(NULL," ::");
        strcpy(unitletra,token);
        if((strcmp(unitletra,"m") == 0) || (strcmp(unitletra,"k") == 0)){
        }else{
            printf("\nUnidad Invalida.\n");
            return -1;
        }
        break;
     case 3:
       // printf("token1  ------%s\n",token);
        //token=strtok(NULL,":\"\n");
       // token = strtok(NULL," ");
        token=strtok(NULL,":\"\n");
        if(strstr(token,comillas)){
            printf("token3  ------%s\n",token);
            strcpy(dpth,token);
            strcpy(dpth2,dpth);
            strcpy(dpth3,dpth);
        }else{
            //token = strtok(NULL,":");
            strcpy(dpth,token);
            strcpy(dpth2,token);
            strcpy(dpth3,token);
        }
      //  printf("dpth %s\n",dpth);
       // printf("dpth2 %s\n",dpth2);
       // printf("dpth3 %s\n",dpth3);

        break;


    case 4:
        //token = strtok(NULL," ::");
        token=strtok(NULL,":\"\n");
        if(strstr(token,comillas)){
          //  token=strtok(NULL,"\"");
            strcpy(Nombre,token);

        }else{
            strcpy(Nombre,token);
        }

        if(strstr(Nombre,".dsk")){
        printf("\nNombre verificado\n");
        }else{
        printf("\nExtension de nombre invalida\n");
       return -1;
        }


        break;

      default:
        printf("\nAccion invalida o Inexistente.\n");

        return -1;

        break;
    }
}

        //VERIFICANDO PATH QUE SEA CORRECTO
        if((tamanioDeDisco <=9 && strcmp(unitletra,"m")==0)||(tamanioDeDisco <10240 && strcmp(unitletra,"k")==0)){
            printf("\nEl tamaño de disco debe ser mayor a 9MB o mayor a 10240KB\n");
         return -1;
        }
    //SE CREA EL PATH

    strcat(dpth,Nombre);
    strcat(dpth2,Nombre);
    strcat(dpth3,Nombre);
    int numPalabras =1;
    char *tem;
    tem =strtok(dpth,"/");

    while(tem!=NULL){
        tem =strtok(NULL,"/");
        if(tem==NULL) break;
        numPalabras++;
    }

    char pathReal[200]="/";
    char*aux11;
    aux11= strtok(dpth2,"/");
    strcat(pathReal,aux11);
    strcat(pathReal,"/");
    while(numPalabras > 2)
    {
        aux11 = strtok(NULL,"/");
        strcat(pathReal,aux11);
        strcat(pathReal,"/");
        mkdir(pathReal ,S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
        numPalabras--;
    }

    aux11 = strtok(NULL,"/");

    mbr *mbr1 =(mbr*)malloc(sizeof(mbr));
    FILE *archivo = fopen(dpth3,"wb");

    //obteniendo fecha del sistema
    timer_t tiempo = time(0);
    struct tm *tlocal=localtime(&tiempo);
    char output[128];
    strftime(output,128,"%d/%m/%y %H:%M:%S",tlocal);

    strcpy(mbr1->mbr_fecha_creacion,output);

    int random = rand();
    mbr1->mbr_disk_signature=random;


    mbr1->mbr_partition1.part_size =0;
    mbr1->mbr_partition2.part_size =0;
    mbr1->mbr_partition3.part_size =0;
    mbr1->mbr_partition4.part_size =0;
    mbr1->mbr_partition1.part_start=0;
    mbr1->mbr_partition2.part_start=0;
    mbr1->mbr_partition3.part_start=0;
    mbr1->mbr_partition4.part_start=0;

    strcpy(mbr1->mbr_partition1.part_type,"x");
    strcpy(mbr1->mbr_partition2.part_type,"x");
    strcpy(mbr1->mbr_partition3.part_type,"x");
    strcpy(mbr1->mbr_partition4.part_type,"x");



    if(strcmp(unitletra, "k")==0){
       mbr1->mbr_tamanio= tamanioDeDisco*1024;
       //fseek(archivo,0,SEEK_SET);
       fwrite(mbr1,sizeof(mbr),1,archivo);
       int h =0;
       for(h; h<500*tamanioDeDisco;h++){
           fputs("\\0",archivo);
       }
    }else if(strcmp(unitletra,"m") == 0){
           mbr1->mbr_tamanio=tamanioDeDisco*1024*1024;
           //fseek(archivo,0,SEEK_SET);
           fwrite(mbr1,sizeof(mbr),1,archivo);
           int i=0;
           for(i;i< 2*(500*tamanioDeDisco*500);i++){
               fputs("\\0",archivo);
           }

}
    printf("\nDisco creado Con exito\n");
fclose(archivo);
/**************************/

mbr *mbrLeido=(mbr*)malloc(sizeof(mbr));
FILE *archivo1 =fopen(dpth3,"rb");
fread(mbrLeido,sizeof(mbr),1,archivo1);
int t= sizeof(mbrLeido);
printf("\ntamannio: %d\n", mbrLeido->mbr_tamanio);

fclose(archivo1);
return 1;
/**************************/


}
int rmdisk(char *token){
if(strstr(comprobarcadena,"\"/") && strstr(comprobarcadena,".dsk\"")){
    }else{printf("\nError en el comando\n"); return -1;}
    token = strtok(NULL," :");

char* path[200];

if(strstr(token,"-path")){

}
else{
    printf("\nComando de path invalido\n");
    return -1;
}

//token =strtok(NULL,":");
token=strtok(NULL,":\"\n");
strcpy(path,token);

if(strstr(token,"\"")){

 // token=strtok(token,"\"");

  strcpy(path,token);

}


FILE* ArchivoBorrar =fopen(path,"r");
printf(path);
if(ArchivoBorrar){
    printf("\nSeguro que desea eliminar disco (escriba si o no):\n");
    char *confirmacion[10];
    fgets(confirmacion,10,stdin);
    char *quitarsalto=strtok(confirmacion,"\n");
    strcpy(confirmacion,quitarsalto);

    if(strcmp(confirmacion,"si")==0){
        remove(path);
        printf("\nDisco eliminado\n");

    }else if(strcmp(confirmacion,"no")==0){
       printf("\nDisco No eliminado\n");
       fclose(ArchivoBorrar);
    }else{
        printf("\nRespuesta invalida, no se ejecuto ninguna accion.\n");
        fclose(ArchivoBorrar);
    }

}else{
    printf("\nArchivo a Borrar No Existe\n");
}

return 1;

}
int fdisk(char *token){
//printf("ENTRO 1");
if(strstr(comprobarcadena,"\"/") && strstr(comprobarcadena,".dsk\"")){
//printf("ENTRO 2");
    }else{printf("\nError en el comando\n"); return -1;}
    int    op = 0;
    int    size1=0;
    char*  unit1[5];
    char*  path1[200];
    char*  type1[5];
    char*  fit1[5];
    char*  delete1[20];
    char*  name1[50];
    int    add1=0;
    char*  add11[20];
    int  Menos=0;
    int delete11=0;

    //aca se mira si hay comandos comprobarcadenas
    if(strstr(comprobarcadena,"add") && strstr(comprobarcadena,"delete") || strstr(comprobarcadena,"add") && strstr(comprobarcadena,"size") || strstr(comprobarcadena,"size") && strstr(comprobarcadena,"delete")){
   printf("\nError comandos comprobarcadenas\n");
   return -1;
   }
   //SE VERIFICA QUE ESTE TODA LA INFORMACION DE SIZE
    if(strstr(comprobarcadena,"-size")){

       if(strstr(comprobarcadena,"-path") && strstr(comprobarcadena,"-name")){
   //printf("ENTRO 3");
       }else{
           printf("\nFalta Informacion\n");
           return -1;
       }
   }
   //SE VERIFICA QUE ESTE TODA LA INFORMACION DEL DELETE
    if(strstr(comprobarcadena,"+delete")){


       if(strstr(comprobarcadena,"-path") && strstr(comprobarcadena,"-name")){

       }else{
           printf("\nFalta Informacion\n");
       return -1;
       }
   }
    //SE VERIFICA QUE ESTE TODA LA INFORMACIOND EL ADD
    if(strstr(comprobarcadena,"+add")){
      if(strstr(comprobarcadena,"-path") && strstr(comprobarcadena,"-name")){

       }else{
           printf("\nFalta Informacion\n");
       return -1;
       }

   }
    //SE COLOCAN LOS VALORES POR DEFECTO

    strcpy(fit1,"wf");
    strcpy(unit1,"k");
    strcpy(type1,"p");

     //   printf("\nponiendo token %s\n",token);
    //SE ANALIZA LA CADENA A EJECUTAR
     while(token !=NULL){
        token=strtok(NULL," :");
        if(token==NULL) break;
        if(strstr(token,"-size"))    op=1;
        if(strstr(token,"+unit"))    op=2;
        if(strstr(token,"-path"))    op=3;
        if(strstr(token,"+type"))    op=4;
        if(strstr(token,"+fit"))     op=5;
        if(strstr(token,"+delete"))  op=6;
        if(strstr(token,"-name"))    op=7;
        if(strstr(token,"+add"))     op=8;
     //   printf("\nponiendo token Q%sQ ---- %d\n",token, op);
     switch(op){
      case 1:
        token=strtok(NULL," :");
        size1=atoi(token);
        if(token==NULL){
        printf("\nComando invalido o mal escrito\n");
        return -1;
        }else if(size1 <=0){
            printf("\nTamaño de particion invalido\n");
            return -1;
        }
          break;
      case 2:
        token=strtok(NULL," :");
        strcpy(unit1,token);
        if(token==NULL){
        printf("\nComando invalido o mal escrito\n");
        return -1;
        }else if(strcmp(unit1,"m")!=0 && strcmp(unit1,"k")!=0 && strcmp(unit1,"b")!=0){

            printf("\nUnidad  invalida.\n");
            return -1;
        }
        break;
      case 3:
         token=strtok(NULL,":\"\n");
          //token = strtok(NULL,": ");
      char *tok;
      if(strstr(token,"\"")){
       // tok=strtok(token,"\"");
        strcpy(path1,token);
      }else{
          strcpy(path1,token);
      }
      FILE* comprobar =fopen(path1,"r");
      if(comprobar){
      fclose(comprobar);
      }else{
      printf("\nArchivo Inexistente\n");
      return -1;
      }

          break;
      case 4:
          token=strtok(NULL," :");
          strcpy(type1,token);
          if(strcmp(type1,"p")!=0 && strcmp(type1,"e")!=0 && strcmp(type1,"l")!=0){
              printf("\nTipo de particion invalida\n");
              return -1;
          }
          break;
      case 5:
          token =strtok(NULL," :");
          strcpy(fit1,token);
          if(strcmp(fit1,"bf")!=0 && strcmp(fit1,"ff")!=0 && strcmp(fit1,"wf")!=0){
              printf("\nAjuste de particion invalido\n");
              return -1;
          }
          break;
      case 6:
          token =strtok(NULL," :");
          strcpy(delete1,token);
          delete11=1;
          if(strcmp(delete1,"full")!=0 && strcmp(delete1,"fast")!=0){
              printf("\nTipo de eliminacion invalido\n");
              return -1;
          }
          break;

      case 7:
      token=strtok(NULL,":\"\n");
          //token =strtok(NULL," :");
          strcpy(name1,token);
          if(name1==NULL){
              printf("\nNombre invalido\n");
              return -1;
          }
          break;
      case 8:
          token =strtok(NULL," :");
          strcpy(add11,token);
          if(add11!=NULL){
          if(strstr(add11,"-")){
           Menos = 1;
           char* tok1;
           tok1 = strtok(add11,"- :");
           strcpy(add11,tok1);
           add1 =atoi(add11);
          }else{
              add1=atoi(add11);
          }
          }else{
              printf("\nvalor de add incorrecto o inexistente.\n");
          return -1;
          }
          break;
      default:
          printf("\ncomandos invalidos\n");
          return -1;
          break;

        }

    }
 //   printf("ENTRO 1 slio while");

    mbr* mbraux=(mbr*)malloc(sizeof(mbr));
    FILE* file = fopen(path1,"r+b");
    fseek(file,0,SEEK_SET);
    fread(mbraux,sizeof(mbr),1,file);
    fclose(file);

    if(strcmp(unit1,"b")==0){
        size1= size1;
        add1 = add1;
    }else if(strcmp(unit1,"k")==0){
        size1=size1*1024;
        add1 =add1 *1024;
    }else if(strcmp(unit1, "m")==0){
        size1=size1*1024*1024;
        add1 =add1 *1024*1024;
    }
    //comprobando que hay suficiente espacio en el disco
    int EspacioLibreEnDisco;
       EspacioLibreEnDisco=mbraux->mbr_tamanio-mbraux->mbr_partition1.part_size-mbraux->mbr_partition2.part_size-mbraux->mbr_partition3.part_size-mbraux->mbr_partition4.part_size;
    int cabeParticion;

    cabeParticion=EspacioLibreEnDisco-size1;
    //SI LA PARTICION A CREAR CABE EN EL DISCO ENTONCES CabeParticion SERA 1 en otro caso sera -1
    if(cabeParticion>0){
    cabeParticion=1;
    }else{
    cabeParticion=-1;
    }

    //COMPROVANO RESTRICCIONES POR TEORIA DE PARTICIONES

    mbr *mbr01=(mbr*)malloc(sizeof(mbr));
    FILE *partc =fopen(path1,"r+b");

    fseek(partc,0,SEEK_SET);
    fread(mbr01,sizeof(mbr),1,partc);
    fclose(partc);

   if(strstr(comprobarcadena,"type")){


       if(strcmp(type1,"e")==0){
            if(strcmp(mbr01->mbr_partition1.part_type,"e")==0){printf("\nYa existe una particion Extendida\n"); return -1;}
            if(strcmp(mbr01->mbr_partition2.part_type,"e")==0){printf("\nYa existe una particion Extendida\n"); return -1;}
            if(strcmp(mbr01->mbr_partition3.part_type,"e")==0){printf("\nYa existe una particion Extendida\n"); return -1;}
            if(strcmp(mbr01->mbr_partition4.part_type,"e")==0){printf("\nYa existe una particion Extendida\n"); return -1;}
        }
        if(strcmp(type1,"l")==0){
        if(strcmp(mbr01->mbr_partition1.part_type,"e")!=0 && strcmp(mbr01->mbr_partition2.part_type,"e")!=0 && strcmp(mbr01->mbr_partition3.part_type,"e")!=0 && strcmp(mbr01->mbr_partition4.part_type,"e")!=0)
        {
            printf("\nNO existe una particion Extendida\n");
               return -1;
        }
        }
        if(strcmp(type1,"l")!=0 && mbr01->mbr_partition1.part_size>0 && mbr01->mbr_partition2.part_size>0 && mbr01->mbr_partition3.part_size>0 && mbr01->mbr_partition4.part_size>0){
           printf("\nNo hay espacio para otra particion\n");
           return -1;
        }
    }


    //SI SE CREA UNA NUEVA PARTICION
    if(size1>0){
   // printf("ENTRO en size>0");
    //VEROFOCANDO QUE EL NOMBRE NO EXISTA AUN
    if(strcmp(mbraux->mbr_partition1.part_name,name1)==0 || strcmp(mbraux->mbr_partition2.part_name,name1)==0 || strcmp(mbraux->mbr_partition3.part_name,name1)==0 || strcmp(mbraux->mbr_partition4.part_name,name1)==0){
          printf("/nnombre de particion ya existe/n");
          return -1;
       }
       if(mbraux->mbr_partition1.part_size>0 && mbraux->mbr_partition2.part_size>0 && mbraux->mbr_partition3.part_size>0 && mbraux->mbr_partition4.part_size>0){
       printf("/nYa Existen cuatro particiones, ya no cabe otra/n");

       }


      if(strcmp("l",type1)!=0){
        //SI PARTICION UNO ESTA VACIA
      if(mbr01->mbr_partition1.part_size==0){

        int inicio=0;
        inicio=IniciarParticion(mbraux->mbr_partition2.part_start,mbraux->mbr_partition2.part_size,mbraux->mbr_partition3.part_start,mbraux->mbr_partition3.part_size,mbraux->mbr_partition4.part_start,mbraux->mbr_partition4.part_size,size1,mbraux->mbr_tamanio);

        if(inicio<0){
        printf("\nParticion demasiado grande\n");
        return -1;
        }


    strcpy(mbr01->mbr_partition1.part_fit,fit1);
    strcpy(mbr01->mbr_partition1.part_name,name1);
    strcpy(mbr01->mbr_partition1.part_type,type1);
    mbr01->mbr_partition1.part_size    =size1;
    mbr01->mbr_partition1.part_start   =inicio;
    mbr01->mbr_partition1.part_status  =1;


    FILE *partc11 =fopen(path1,"rb+");
    fseek(partc11,0,SEEK_SET);
    fwrite(mbr01,sizeof(mbr),1,partc11);
    fclose(partc11);

    if(strcmp(type1,"e")==0){
        ebr *ebr1 =(ebr*)malloc(sizeof(ebr));
        ebr1->part_status=1;
        FILE* amigo=fopen(path1,"rb+");
        fseek(amigo,0,SEEK_SET);
        mbr* auxiliar=(mbr*)malloc(sizeof(mbr));
        fread(auxiliar,sizeof(mbr),1,amigo);
        fseek(amigo,inicio,SEEK_CUR);
        strcpy(ebr1->part_fit,"");
        ebr1->part_status=0;
        ebr1->part_start=-1;
        ebr1->part_size=0;
        ebr1->part_next=-1;
        strcpy(ebr1->part_name,"");
        fwrite(ebr1,sizeof(ebr),1,amigo);

        fclose(amigo);

    }
    printf("\nParticion Creada Con exito\n");

   }


         //SI PARTICION DOS ESTA VACIA
        else   if(mbr01->mbr_partition2.part_size==0){

        int inicio=0;
        inicio=IniciarParticion(mbraux->mbr_partition1.part_start,mbraux->mbr_partition1.part_size,mbraux->mbr_partition3.part_start,mbraux->mbr_partition3.part_size,mbraux->mbr_partition4.part_start,mbraux->mbr_partition4.part_size,size1,mbraux->mbr_tamanio);

        if(inicio<0){
        printf("\nParticion demasiado grande\n");
        return -1;
        }

        strcpy(mbr01->mbr_partition2.part_fit,fit1);
        strcpy(mbr01->mbr_partition2.part_name,name1);
        strcpy(mbr01->mbr_partition2.part_type,type1);
        mbr01->mbr_partition2.part_size    =size1;
        mbr01->mbr_partition2.part_start   =inicio;
        mbr01->mbr_partition2.part_status  =1;

    FILE *partc11 =fopen(path1,"rb+");
    fseek(partc11,0,SEEK_SET);
    fwrite(mbr01,sizeof(mbr),1,partc11);
    fclose(partc11);

    if(strcmp(type1,"e")==0){
        ebr *ebr1 =(ebr*)malloc(sizeof(ebr));
        ebr1->part_status=1;
        FILE* amigo=fopen(path1,"rb+");
        fseek(amigo,0,SEEK_SET);
        mbr* auxiliar=(mbr*)malloc(sizeof(mbr));
        fread(auxiliar,sizeof(mbr),1,amigo);
        fseek(amigo,inicio,SEEK_CUR);
        strcpy(ebr1->part_fit,"");
        ebr1->part_status=0;
        ebr1->part_start=-1;
        ebr1->part_size=0;
        ebr1->part_next=-1;
        strcpy(ebr1->part_name,"");
        fwrite(ebr1,sizeof(ebr),1,amigo);

        fclose(amigo);

    }
printf("\nParticion Creada Con exito\n");
   }

         //SI PARTICION TRES ESTA VACIA
        else if(mbr01->mbr_partition3.part_size==0){

        int inicio=0;
        inicio=IniciarParticion(mbraux->mbr_partition1.part_start,mbraux->mbr_partition1.part_size,mbraux->mbr_partition2.part_start,mbraux->mbr_partition2.part_size,mbraux->mbr_partition4.part_start,mbraux->mbr_partition4.part_size,size1,mbraux->mbr_tamanio);

        if(inicio<0){
        printf("\nParticion demasiado grande\n");
        return -1;
        }

        strcpy(mbr01->mbr_partition3.part_fit,fit1);
        strcpy(mbr01->mbr_partition3.part_name,name1);
        strcpy(mbr01->mbr_partition3.part_type,type1);
        mbr01->mbr_partition3.part_size    =size1;
        mbr01->mbr_partition3.part_start   =inicio;
        mbr01->mbr_partition3.part_status  =1;

    FILE *partc11 =fopen(path1,"rb+");
    fseek(partc11,0,SEEK_SET);
    fwrite(mbr01,sizeof(mbr),1,partc11);
    fclose(partc11);

    if(strcmp(type1,"e")==0){
        ebr *ebr1 =(ebr*)malloc(sizeof(ebr));
        ebr1->part_status=1;
        FILE* amigo=fopen(path1,"rb+");
        fseek(amigo,0,SEEK_SET);
        mbr* auxiliar=(mbr*)malloc(sizeof(mbr));
        fread(auxiliar,sizeof(mbr),1,amigo);
        fseek(amigo,inicio,SEEK_CUR);
        strcpy(ebr1->part_fit,"");
        ebr1->part_status=0;
        ebr1->part_start=-1;
        ebr1->part_size=0;
        ebr1->part_next=-1;
        strcpy(ebr1->part_name,"");
        fwrite(ebr1,sizeof(ebr),1,amigo);

        fclose(amigo);

    }
    printf("\nParticion Creada Con exito\n");
   }

           //SI PARTICION CUATRO ESTA VACIA
        else if(mbr01->mbr_partition4.part_size==0){
      //   printf("ENTRO particion cuatro");
        int inicio=0;
        inicio=IniciarParticion(mbraux->mbr_partition1.part_start,mbraux->mbr_partition1.part_size,mbraux->mbr_partition3.part_start,mbraux->mbr_partition3.part_size,mbraux->mbr_partition2.part_start,mbraux->mbr_partition2.part_size,size1,mbraux->mbr_tamanio);

        if(inicio<0){
        printf("\nParticion demasiado grande\n");
        return -1;
        }

        strcpy(mbr01->mbr_partition4.part_fit,fit1);
        strcpy(mbr01->mbr_partition4.part_name,name1);
        strcpy(mbr01->mbr_partition4.part_type,type1);
        mbr01->mbr_partition4.part_size    =size1;
        mbr01->mbr_partition4.part_start   =inicio;
        mbr01->mbr_partition4.part_status  =1;

    FILE *partc11 =fopen(path1,"rb+");
    fseek(partc11,0,SEEK_SET);
    fwrite(mbr01,sizeof(mbr),1,partc11);
    fclose(partc11);

    if(strcmp(type1,"e")==0){
        ebr *ebr1 =(ebr*)malloc(sizeof(ebr));
        ebr1->part_status=1;
        FILE* amigo=fopen(path1,"rb+");
        fseek(amigo,0,SEEK_SET);
        mbr* auxiliar=(mbr*)malloc(sizeof(mbr));
        fread(auxiliar,sizeof(mbr),1,amigo);
        fseek(amigo,inicio,SEEK_CUR);
        strcpy(ebr1->part_fit,"");
        ebr1->part_status=0;
        ebr1->part_start=-1;
        ebr1->part_size=0;
        ebr1->part_next=-1;
        strcpy(ebr1->part_name,"");
        fwrite(ebr1,sizeof(ebr),1,amigo);

        fclose(amigo);

    }
    printf("\nParticion Creada Con exito\n");
   }
}
   if(strcmp("l",type1)==0){

/*******************************************************************************************************************************************************************************************************************************/
          if(strcmp(mbr01->mbr_partition1.part_type,"e")==0){
              FILE* file = fopen(path1,"rb+");
              mbr* mbraux=(mbr*)malloc(sizeof(mbr));
              fseek(file,0,SEEK_SET);
              fread(mbraux,sizeof(mbr),1,file);
              fclose(file);
             // printf("\ntam en if de logica %d\n",mbraux->mbr_tamanio);

                /* enviarebrs(path1);
                 if(mbr01->mbr_partition1.part_size<logsumasize){
                     vaciarlisebr();
                     logsumasize=0;
                     printf("\nya no hay espacio para una particion tan grande.\n");
                    return -1;
                 }
                 vaciarlisebr();
                 logsumasize=0;*/
              int inicio =mbr01->mbr_partition1.part_start;
                int size =mbr01->mbr_partition1.part_size;

                if(size<size1){
                    printf("\nparticion logica mas grande que la extendida total\n");
                            return -1;
                }
//CA<
int asasdadadasdas;
                FILE *archivo=fopen(path1,"rb+");

                fseek(archivo,0,SEEK_SET);
                mbr *aux2 =(mbr *)malloc(sizeof(mbr));
                ebr *aux3=(ebr *)malloc(sizeof(ebr));
                fread(aux2,sizeof(mbr),1,archivo);
                fseek(archivo,inicio,SEEK_CUR);
                int cur=ftell(archivo);
                fread(aux3,sizeof(ebr),1,archivo);
                ebr *aux4=(ebr *)malloc(sizeof(ebr));
                int o;
                int contador=0;
                int st;
                FILE* file1 = fopen(path1,"r+b");
                mbr* mbraux1=(mbr*)malloc(sizeof(mbr));
                fseek(file1,0,SEEK_SET);
                fread(mbraux1,sizeof(mbr),1,file1);
                fclose(file1);
                //printf("\n\n\ntam en if de logica FINAL! %d\n\n\n",mbraux1->mbr_tamanio);

                int bandera=0;
                if(aux3->part_next==-1 && aux3->part_size>0){
                    aux3->part_next=aux3->part_size+aux3->part_start;

                    fseek(archivo,0,SEEK_SET);
                    fread(aux2,sizeof(mbr),1,archivo);
                    fseek(archivo,inicio,SEEK_CUR);
                    fwrite(aux3,sizeof(ebr),1,archivo);

                    fseek(archivo,0,SEEK_SET);

                    fread(aux2,sizeof(mbr),1,archivo);
                    fseek(archivo,inicio,SEEK_CUR);
                    cur=ftell(archivo);
                    fread(aux3,sizeof(ebr),1,archivo);
                    bandera=1;
                }




               // printf("\nsiguiente de primera particion. %d\n",aux3->part_next);
             /*   while(bandera==0){



                  o=aux3->part_size;
                  int lk;
                  fseek(archivo,o,SEEK_CUR);
                   contador++;

                   if(aux3->part_next==-1){

                       aux3->part_next=aux3->part_size+aux3->part_start;

                       fseek(archivo,lk,SEEK_SET);

                       fwrite(aux3,sizeof(ebr),1,archivo);

                       fseek(archivo,-sizeof(ebr),SEEK_CUR);
                       st=ftell(archivo);
                       fread(aux3,sizeof(ebr),1,archivo);


                       break;
                   }else{
                       fseek(archivo,0,SEEK_CUR);
                         lk=ftell(archivo);
                       fread(aux3,sizeof(ebr),1,archivo);
;
                   }



                }
                bandera=0;

*/
int lk;
                while(bandera==0){
                  o=aux3->part_size;
                   contador++;
                   if(aux3->part_next==-1){
                        aux3->part_next=aux3->part_size+aux3->part_start;
                        fseek(archivo,-sizeof(ebr),SEEK_CUR);
                        fwrite(aux3,sizeof(ebr),1,archivo);
                        fseek(archivo,-sizeof(ebr),SEEK_CUR);
                        st=ftell(archivo);
                        fread(aux3,sizeof(ebr),1,archivo);
                        break;
                   }else{
                        fseek(archivo,o,SEEK_CUR);
                       //fseek(archivo,0,SEEK_CUR);
                        lk=ftell(archivo);
                        fread(aux3,sizeof(ebr),1,archivo);
                   }
                }
                bandera=0;

                if(aux3->part_size>0){
                fseek(archivo,aux3->part_size,SEEK_CUR);
                }else{

                    fseek(archivo,0,SEEK_SET);
                    fread(aux2,sizeof(mbr),1,archivo);
                    fseek(archivo,inicio,SEEK_CUR);

                }

                FILE* file11 = fopen(path1,"r+b");
                mbr* mbraux11=(mbr*)malloc(sizeof(mbr));
                fseek(file11,0,SEEK_SET);
                fread(mbraux11,sizeof(mbr),1,file11);
                fclose(file11);
               // printf("\n\n\ntam en if de logica FINAL22! %d\n\n\n",mbraux11->mbr_tamanio);

                strcpy(aux4->part_fit,fit1);
                aux4->part_status=1;
                if(aux3->part_size>0){
                aux4->part_start=aux3->part_next;

               // printf("\ninicio de nueva particion. %d\n",aux4->part_start);
                }else{
                    aux4->part_start=aux2->mbr_partition1.part_start;
                  //  printf("\ninicio de nueva particion. %d",aux4->part_start);

                }
                aux4->part_size=size1;
                aux4->part_next=-1;
                strcpy(aux4->part_name,name1);




                fwrite(aux4,sizeof(ebr),1,archivo);


/****/

                fseek(archivo,st,SEEK_SET);
                globalcu=ftell(archivo);
                ebr *aux44=(ebr *)malloc(sizeof(ebr));
                fread(aux44,sizeof(ebr),1,archivo);





                fclose(archivo);

                FILE* file12 = fopen(path1,"r+b");
                mbr* mbraux13=(mbr*)malloc(sizeof(mbr));
                fseek(file12,0,SEEK_SET);
                fread(mbraux13,sizeof(mbr),1,file12);
                fclose(file12);
                //printf("\n\n\ntam en if de logica FINAL11! %s\n\n\n",mbraux13->mbr_nam);

                printf("\nDatos de nueva particion Logica.\n");
                printf("\nfit: %s\n",aux4->part_fit);
                printf("\nname: %s\n",aux4->part_name);
                printf("\nsiguiente: %d\n",aux4->part_next);
                printf("\ntama;o: %d\n",aux4->part_size);
                printf("\ninicio: %d\n",aux4->part_start);
                printf("\nestatus: %d\n",aux4->part_status);

          }




 /*********************************************************************************************************************************************************************************************************************/
          if(strcmp(mbr01->mbr_partition2.part_type,"e")==0){
             /* enviarebrs(path1);
              if(mbr01->mbr_partition2.part_size<logsumasize){
                  vaciarlisebr();
                  logsumasize=0;
                  printf("\nya no hay espacio para una particion tan grande.\n");
                 return -1;
              }
              vaciarlisebr();
              logsumasize=0;*/
              int inicio =mbr01->mbr_partition2.part_start;
                int size =mbr01->mbr_partition2.part_size;

                if(size<size1){
                    printf("\nparticion logica mas grande que la extendida total\n");
                            return -1;
                }

                FILE *archivo=fopen(path1,"rb+");

                fseek(archivo,0,SEEK_SET);
                mbr *aux2 =(mbr *)malloc(sizeof(mbr));
                ebr *aux3=(ebr *)malloc(sizeof(ebr));
                fread(aux2,sizeof(mbr),1,archivo);
                fseek(archivo,inicio,SEEK_CUR);
                int cur=ftell(archivo);
                fread(aux3,sizeof(ebr),1,archivo);
                ebr *aux4=(ebr *)malloc(sizeof(ebr));
                int o;
                int contador=0;
                int st;


                int bandera=0;
                if(aux3->part_next==-1 && aux3->part_size>0){
                    aux3->part_next=aux3->part_size+aux3->part_start;

                    fseek(archivo,0,SEEK_SET);
                    fread(aux2,sizeof(mbr),1,archivo);
                    fseek(archivo,inicio,SEEK_CUR);
                    fwrite(aux3,sizeof(ebr),1,archivo);

                    fseek(archivo,0,SEEK_SET);

                    fread(aux2,sizeof(mbr),1,archivo);
                    fseek(archivo,inicio,SEEK_CUR);
                    cur=ftell(archivo);
                    fread(aux3,sizeof(ebr),1,archivo);
                    bandera=1;
                }




              //  printf("\nsiguiente de primera particion. %d\n",aux3->part_next);
                /*while(bandera==0){



                  o=aux3->part_size;
                  int lk;
                  fseek(archivo,o,SEEK_CUR);
                   contador++;

                   if(aux3->part_next==-1){

                       aux3->part_next=aux3->part_size+aux3->part_start;

                       fseek(archivo,lk,SEEK_SET);

                       fwrite(aux3,sizeof(ebr),1,archivo);

                       fseek(archivo,-sizeof(ebr),SEEK_CUR);
                       st=ftell(archivo);
                       fread(aux3,sizeof(ebr),1,archivo);


                       break;
                   }else{
                       fseek(archivo,0,SEEK_CUR);
                         lk=ftell(archivo);
                       fread(aux3,sizeof(ebr),1,archivo);
;
                   }



                }
                bandera=0;
*/
int lk;
                while(bandera==0){
                  o=aux3->part_size;
                   contador++;
                   if(aux3->part_next==-1){
                        aux3->part_next=aux3->part_size+aux3->part_start;
                        fseek(archivo,-sizeof(ebr),SEEK_CUR);
                        fwrite(aux3,sizeof(ebr),1,archivo);
                        fseek(archivo,-sizeof(ebr),SEEK_CUR);
                        st=ftell(archivo);
                        fread(aux3,sizeof(ebr),1,archivo);
                        break;
                   }else{
                        fseek(archivo,o,SEEK_CUR);
                       //fseek(archivo,0,SEEK_CUR);
                        lk=ftell(archivo);
                        fread(aux3,sizeof(ebr),1,archivo);
                   }
                }
                bandera=0;

                if(aux3->part_size>0){
                fseek(archivo,aux3->part_size,SEEK_CUR);
                }else{

                    fseek(archivo,0,SEEK_SET);
                    fread(aux2,sizeof(mbr),1,archivo);
                    fseek(archivo,inicio,SEEK_CUR);

                }

                strcpy(aux4->part_fit,fit1);
                aux4->part_status=1;
                if(aux3->part_size>0){
                aux4->part_start=aux3->part_next;

             //   printf("\ninicio de nueva particion. %d\n",aux4->part_start);
                }else{
                    aux4->part_start=aux2->mbr_partition2.part_start;
                   // printf("\ninicio de nueva particion. %d",aux4->part_start);

                }
                aux4->part_size=size1;
                aux4->part_next=-1;
                strcpy(aux4->part_name,name1);

                fwrite(aux4,sizeof(ebr),1,archivo);


                fseek(archivo,st,SEEK_SET);
                globalcu=ftell(archivo);
                ebr *aux44=(ebr *)malloc(sizeof(ebr));
                fread(aux44,sizeof(ebr),1,archivo);

                fclose(archivo);


                printf("\nDatos de NUeva particion logica creada\n");
                printf("\nfit: %s\n",aux4->part_fit);
                printf("\nname: %s\n",aux4->part_name);
                printf("\nsiguiente: %d\n",aux4->part_next);
                printf("\ntama;o: %d\n",aux4->part_size);
                printf("\ninicio: %d\n",aux4->part_start);
                printf("\nestatus: %d\n",aux4->part_status);

          }

/***************************************************************************************************************************************************************************************************************************************/
          if(strcmp(mbr01->mbr_partition3.part_type,"e")==0){

              int inicio =mbr01->mbr_partition3.part_start;
                int size =mbr01->mbr_partition3.part_size;

                if(size<size1){
                    printf("\nparticion logica mas grande que la extendida total\n");
                            return -1;
                }

                FILE *archivo=fopen(path1,"rb+");

                fseek(archivo,0,SEEK_SET);
                mbr *aux2 =(mbr *)malloc(sizeof(mbr));
                ebr *aux3=(ebr *)malloc(sizeof(ebr));
                fread(aux2,sizeof(mbr),1,archivo);
                fseek(archivo,inicio,SEEK_CUR);
                int cur=ftell(archivo);
                fread(aux3,sizeof(ebr),1,archivo);
                ebr *aux4=(ebr *)malloc(sizeof(ebr));
                int o;
                int contador=0;
                int st;


                int bandera=0;
                if(aux3->part_next==-1 && aux3->part_size>0){



                 mbr* cargambr1=(mbr*)malloc(sizeof(mbr));

        //fseek(assa,0,SEEK_SET);


                    aux3->part_next=aux3->part_size+aux3->part_start;

                    fseek(archivo,0,SEEK_SET);
                    fread(aux2,sizeof(mbr),1,archivo);
                    fseek(archivo,inicio,SEEK_CUR);
                    fwrite(aux3,sizeof(ebr),1,archivo);

                    fseek(archivo,0,SEEK_SET);

                    fread(aux2,sizeof(mbr),1,archivo);
                    fseek(archivo,inicio,SEEK_CUR);
                    cur=ftell(archivo);
                    fread(aux3,sizeof(ebr),1,archivo);
                    bandera=1;
                }




                //printf("\nsiguiente de primera particion. %d\n",aux3->part_next);

int lk;
                while(bandera==0){
                  o=aux3->part_size;
                   contador++;
                   if(aux3->part_next==-1){
                        aux3->part_next=aux3->part_size+aux3->part_start;
                        fseek(archivo,-sizeof(ebr),SEEK_CUR);
                        fwrite(aux3,sizeof(ebr),1,archivo);
                        fseek(archivo,-sizeof(ebr),SEEK_CUR);
                        st=ftell(archivo);
                        fread(aux3,sizeof(ebr),1,archivo);
                        break;
                   }else{
                        fseek(archivo,o,SEEK_CUR);
                       //fseek(archivo,0,SEEK_CUR);
                        lk=ftell(archivo);
                        fread(aux3,sizeof(ebr),1,archivo);
                   }
                }
                bandera=0;


                if(aux3->part_size>0){
                fseek(archivo,aux3->part_size,SEEK_CUR);
                }else{

                    fseek(archivo,0,SEEK_SET);
                    fread(aux2,sizeof(mbr),1,archivo);
                    fseek(archivo,inicio,SEEK_CUR);

                }

                strcpy(aux4->part_fit,fit1);
                aux4->part_status=1;
                if(aux3->part_size>0){
                aux4->part_start=aux3->part_next;

                printf("\ninicio de nueva particion. %d\n",aux4->part_start);
                }else{
                    aux4->part_start=aux2->mbr_partition3.part_start;
                    printf("\ninicio de nueva particion. %d",aux4->part_start);

                }
                aux4->part_size=size1;
                aux4->part_next=-1;
                strcpy(aux4->part_name,name1);

                fwrite(aux4,sizeof(ebr),1,archivo);


                fseek(archivo,st,SEEK_SET);
                globalcu=ftell(archivo);
                ebr *aux44=(ebr *)malloc(sizeof(ebr));
                fread(aux44,sizeof(ebr),1,archivo);

                fclose(archivo);


                printf("\nDatos de nueva particion logica:\n");
                printf("\nfit: %s\n",aux4->part_fit);
                printf("\nname: %s\n",aux4->part_name);
                printf("\nsiguiente: %d\n",aux4->part_next);
                printf("\ntama;o: %d\n",aux4->part_size);
                printf("\ninicio: %d\n",aux4->part_start);
                printf("\nestatus: %d\n",aux4->part_status);


          }

/****************************************************************************************************************************************************************************************************************************************************************************/


          if(strcmp(mbr01->mbr_partition4.part_type,"e")==0){
            /*  enviarebrs(path1);
              if(mbr01->mbr_partition1.part_size<logsumasize){
                  vaciarlisebr();
                  logsumasize=0;
                  printf("\nya no hay espacio para una particion tan grande.\n");
                 return -1;
              }
              vaciarlisebr();
              logsumasize=0;
*/
              int inicio =mbr01->mbr_partition4.part_start;
                int size =mbr01->mbr_partition4.part_size;

                if(size<size1){
                    printf("\nparticion logica mas grande que la extendida total\n");
                            return -1;
                }

                FILE *archivo=fopen(path1,"rb+");

                fseek(archivo,0,SEEK_SET);
                mbr *aux2 =(mbr *)malloc(sizeof(mbr));
                ebr *aux3=(ebr *)malloc(sizeof(ebr));
                fread(aux2,sizeof(mbr),1,archivo);
                fseek(archivo,inicio,SEEK_CUR);
                int cur=ftell(archivo);
                fread(aux3,sizeof(ebr),1,archivo);
                ebr *aux4=(ebr *)malloc(sizeof(ebr));
                int o;
                int contador=0;
                int st;


                int bandera=0;
                if(aux3->part_next==-1 && aux3->part_size>0){
                    aux3->part_next=aux3->part_size+aux3->part_start;

                    fseek(archivo,0,SEEK_SET);
                    fread(aux2,sizeof(mbr),1,archivo);
                    fseek(archivo,inicio,SEEK_CUR);
                    fwrite(aux3,sizeof(ebr),1,archivo);

                    fseek(archivo,0,SEEK_SET);

                    fread(aux2,sizeof(mbr),1,archivo);
                    fseek(archivo,inicio,SEEK_CUR);
                    cur=ftell(archivo);
                    fread(aux3,sizeof(ebr),1,archivo);
                    bandera=1;
                }




               // printf("\nsiguiente de primera particion. %d\n",aux3->part_next);
               /* while(bandera==0){



                  o=aux3->part_size;
                  int lk;
                  fseek(archivo,o,SEEK_CUR);
                   contador++;

                   if(aux3->part_next==-1){

                       aux3->part_next=aux3->part_size+aux3->part_start;

                       fseek(archivo,lk,SEEK_SET);

                       fwrite(aux3,sizeof(ebr),1,archivo);

                       fseek(archivo,-sizeof(ebr),SEEK_CUR);
                       st=ftell(archivo);
                       fread(aux3,sizeof(ebr),1,archivo);


                       break;
                   }else{
                       fseek(archivo,0,SEEK_CUR);
                         lk=ftell(archivo);
                       fread(aux3,sizeof(ebr),1,archivo);
;
                   }



                }
                bandera=0;
*/
                int lk;
                while(bandera==0){
                  o=aux3->part_size;
                   contador++;
                   if(aux3->part_next==-1){
                        aux3->part_next=aux3->part_size+aux3->part_start;
                        fseek(archivo,-sizeof(ebr),SEEK_CUR);
                        fwrite(aux3,sizeof(ebr),1,archivo);
                        fseek(archivo,-sizeof(ebr),SEEK_CUR);
                        st=ftell(archivo);
                        fread(aux3,sizeof(ebr),1,archivo);
                        break;
                   }else{
                        fseek(archivo,o,SEEK_CUR);
                       //fseek(archivo,0,SEEK_CUR);
                        lk=ftell(archivo);
                        fread(aux3,sizeof(ebr),1,archivo);
                   }
                }
                bandera=0;

                if(aux3->part_size>0){
                fseek(archivo,aux3->part_size,SEEK_CUR);
                }else{

                    fseek(archivo,0,SEEK_SET);
                    fread(aux2,sizeof(mbr),1,archivo);
                    fseek(archivo,inicio,SEEK_CUR);

                }

                strcpy(aux4->part_fit,fit1);
                aux4->part_status=1;
                if(aux3->part_size>0){
                aux4->part_start=aux3->part_next;

                //printf("\ninicio de nueva particion. %d\n",aux4->part_start);
                }else{
                    aux4->part_start=aux2->mbr_partition4.part_start;
                  //  printf("\ninicio de nueva particion. %d",aux4->part_start);

                }
                aux4->part_size=size1;
                aux4->part_next=-1;
                strcpy(aux4->part_name,name1);

                fwrite(aux4,sizeof(ebr),1,archivo);


                fseek(archivo,st,SEEK_SET);
                globalcu=ftell(archivo);
                ebr *aux44=(ebr *)malloc(sizeof(ebr));
                fread(aux44,sizeof(ebr),1,archivo);

                fclose(archivo);


                printf("\nDatos de nueva particion logica\n");
                printf("\nfit: %s\n",aux4->part_fit);
                printf("\nname: %s\n",aux4->part_name);
                printf("\nsiguiente: %d\n",aux4->part_next);
                printf("\ntama;o: %d\n",aux4->part_size);
                printf("\ninicio: %d\n",aux4->part_start);
                printf("\nestatus: %d\n",aux4->part_status);

          }

/**************************************************************************************************************************************************************************************************************************************************************************************************************************/
      }

    }

    //SI SE ELIMINA UNA PARTICION
     if(delete11>0){
        FILE *eliminar=fopen(path1,"rb+");
        fseek(eliminar,0,SEEK_SET);
        mbr* mod=(mbr*)malloc(sizeof(mbr));
        fread(mod,sizeof(mbr),1,eliminar);
        fclose(eliminar);
        printf("\nDesea eliminar particion (si o no)\n");
        char*des[20];
        fgets(des,20,stdin);
        if(strstr(des,"no")){
            printf("\nParticion NO eliminada\n");
            return-1;
        }else if(!strstr(des,"si")){
            printf("\nRespuesta invalida, se aborto la mision.\n");
            return-1;
        }
        //PARA PARTICION UNO
        if(strcmp(mod->mbr_partition1.part_name,name1)==0){


            if(strcmp(delete1,"fast")==0){
                mod->mbr_partition1.part_status=0;
                mod->mbr_partition1.part_size=0;
                mod->mbr_partition1.part_start=0;
         strcpy(mod->mbr_partition1.part_fit,"");
         strcpy(mod->mbr_partition1.part_name,"");
         strcpy(mod->mbr_partition1.part_type,"x");


            }
            else if(strcmp(delete1,"full")==0){

                       mod->mbr_partition1.part_status=0;
                       mod->mbr_partition1.part_size=0;
                       mod->mbr_partition1.part_start=0;
                strcpy(mod->mbr_partition1.part_fit,"");
                strcpy(mod->mbr_partition1.part_name,"");
                strcpy(mod->mbr_partition1.part_type,"x");

            }else{
                printf("\nError en comando delete.\n");
                return -1;
            }
        }
        //PARA PARTICION DOS
        else if(strcmp(mod->mbr_partition2.part_name,name1)==0){
            if(strcmp(delete1,"fast")==0){
                mod->mbr_partition2.part_status=0;
                mod->mbr_partition2.part_size=0;
                mod->mbr_partition2.part_start=0;
         strcpy(mod->mbr_partition2.part_fit,"");
         strcpy(mod->mbr_partition2.part_name,"");
         strcpy(mod->mbr_partition2.part_type,"x");

            }
            else if(strcmp(delete1,"full")==0){
                       mod->mbr_partition2.part_status=0;
                       mod->mbr_partition2.part_size=0;
                       mod->mbr_partition2.part_start=0;
                strcpy(mod->mbr_partition2.part_fit,"");
                strcpy(mod->mbr_partition2.part_name,"");
                strcpy(mod->mbr_partition2.part_type,"x");

            }else{
                printf("\nError en comando delete.\n");
                return -1;
            }

        }
        //PARA PARTICION TRES
        else if(strcmp(mod->mbr_partition3.part_name,name1)==0){
            if(strcmp(delete1,"fast")==0){
                mod->mbr_partition3.part_status=0;
                mod->mbr_partition3.part_size=0;
                mod->mbr_partition3.part_start=0;
         strcpy(mod->mbr_partition3.part_fit,"");
         strcpy(mod->mbr_partition3.part_name,"");
         strcpy(mod->mbr_partition3.part_type,"x");

            }
            else if(strcmp(delete1,"full")==0){
                       mod->mbr_partition3.part_status=0;
                       mod->mbr_partition3.part_size=0;
                       mod->mbr_partition3.part_start=0;
                strcpy(mod->mbr_partition3.part_fit,"");
                strcpy(mod->mbr_partition3.part_name,"");
                strcpy(mod->mbr_partition3.part_type,"x");

            }else{
                printf("\nError en comando delete.\n");
                return -1;
            }

        }
        //PARA PARTICION CUATRO
        else if(strcmp(mod->mbr_partition4.part_name,name1)==0){
            if(strcmp(delete1,"fast")==0){


                mod->mbr_partition4.part_status=0;
                mod->mbr_partition4.part_size=0;
                mod->mbr_partition4.part_start=0;
         strcpy(mod->mbr_partition4.part_fit,"");
         strcpy(mod->mbr_partition4.part_name,"");
         strcpy(mod->mbr_partition4.part_type,"x");

            }
            else if(strcmp(delete1,"full")==0){

                       mod->mbr_partition4.part_status=0;
                       mod->mbr_partition4.part_size=0;
                       mod->mbr_partition4.part_start=0;
                strcpy(mod->mbr_partition4.part_fit,"");
                strcpy(mod->mbr_partition4.part_name,"");
                strcpy(mod->mbr_partition4.part_type,"x");

            }else{
                printf("\nError en comando delete.\n");
                return -1;
            }
        }else{
            printf("\nNOMBRE DE PARTICION NO EXISTE\n");
            return -1;
        }

        FILE *eliminar1=fopen(path1,"rb+");
        fseek(eliminar1,0,SEEK_SET);
        fwrite(mod,sizeof(mbr),1,eliminar1);
        fclose(eliminar1);
        printf("\nPARTICION ELIMINADA\n");


    }

     if(add1!=0){
        FILE *agregar=fopen(path1,"rb+");
        fseek(agregar,0,SEEK_SET);
        mbr *mod=(mbr*)malloc(sizeof(mbr));
        fread(mod,sizeof(mbr),1,agregar);
        fclose(agregar);

        if(strcmp(mod->mbr_partition1.part_name,name1)==0){

            if(Menos==0){
                int chule  = mod->mbr_partition1.part_start+mod->mbr_partition1.part_size;
                int chule0 =chule+add1;
                int chule1 = mod->mbr_partition2.part_start;
                int chule2 = mod->mbr_partition3.part_start;
                int chule3 = mod->mbr_partition4.part_start;

                int britany1 = mod->mbr_partition2.part_size;
                int britany2 = mod->mbr_partition3.part_size;
                int britany3 = mod->mbr_partition4.part_size;

                int brayan =mod->mbr_tamanio;


                if(chule1>chule && (chule1<chule2 || britany2<=0) && (chule1<chule3 || britany3<=0) ){
                    if(chule0<chule1){
                     mod->mbr_partition1.part_size=chule0;
                     FILE *betzy = fopen(path1,"rb+");
                     fseek(betzy,0,SEEK_SET);
                     fwrite(mod,sizeof(mbr),1,betzy);
                     fclose(betzy);
                     printf("\nESPACIO MODIFICADO\n");
                     return -1;
                    }else{
                        printf("\nNo hay suficiente espacio libre para agrandar la particion\n");
                        return -1;
                    }
                }else if(chule2>chule && (chule2<chule1 || britany1<=0) && (chule2<chule3 || britany3<=0) ){
                    if(chule0<chule2){
                     mod->mbr_partition1.part_size=chule0;
                     FILE *betzy = fopen(path1,"rb+");
                     fseek(betzy,0,SEEK_SET);
                     fwrite(mod,sizeof(mbr),1,betzy);
                     fclose(betzy);
                     printf("\nESPACIO MODIFICADO\n");
                     return -1;
                    }else{
                        printf("\nNo hay suficiente espacio libre para agrandar la particion\n");
                        return -1;
                    }


                }else if(chule3>chule && (chule3<chule1 || britany1<=0) && (chule3<chule2 || britany2<=0) ){
                    if(chule0<chule3){
                     mod->mbr_partition1.part_size=chule0;
                     FILE *betzy = fopen(path1,"rb+");
                     fseek(betzy,0,SEEK_SET);
                     fwrite(mod,sizeof(mbr),1,betzy);
                     fclose(betzy);
                     printf("\nESPACIO MODIFICADO\n");
                     return -1;
                    }else{
                        printf("\nNo hay suficiente espacio libre para agrandar la particion\n");
                        return -1;
                    }
                }else if(chule0<brayan){


                     mod->mbr_partition1.part_size=chule0;
                     FILE *betzy = fopen(path1,"rb+");
                     fseek(betzy,0,SEEK_SET);
                     fwrite(mod,sizeof(mbr),1,betzy);
                     fclose(betzy);
                     printf("\nESPACIO MODIFICADO\n");



                }

            }

            else if(Menos!=0){
                int tam   =mod->mbr_partition1.part_size;
                int valor =tam-add1;
                if(valor>0){
                    mod->mbr_partition1.part_size=valor;
                    FILE *betzy = fopen(path1,"rb+");
                    fseek(betzy,0,SEEK_SET);
                    fwrite(mod,sizeof(mbr),1,betzy);
                    fclose(betzy);
                    printf("\nESPACIO MODIFICADO\n");


                }else{
                    printf("\nNo hay suficiente espacio libre para modificar la particion\n");
                    return -1;
                }

            }else{
                printf("\nNo hay suficiente espacio libre para modificar la particion\n");
                return -1;
            }

        }
        else if(strcmp(mod->mbr_partition2.part_name,name1)==0){

            if(Menos==0){
                int chule  = mod->mbr_partition2.part_start+mod->mbr_partition2.part_size;
                int chule0 =chule+add1;
                int chule1 = mod->mbr_partition1.part_start;
                int chule2 = mod->mbr_partition3.part_start;
                int chule3 = mod->mbr_partition4.part_start;

                int britany1 = mod->mbr_partition1.part_size;
                int britany2 = mod->mbr_partition3.part_size;
                int britany3 = mod->mbr_partition4.part_size;

                int brayan =mod->mbr_tamanio;


                if(chule1>chule && (chule1<chule2 || britany2<=0) && (chule1<chule3 || britany3<=0) ){
                    if(chule0<chule1){
                     mod->mbr_partition2.part_size=chule0;
                     FILE *betzy = fopen(path1,"rb+");
                     fseek(betzy,0,SEEK_SET);
                     fwrite(mod,sizeof(mbr),1,betzy);
                     fclose(betzy);
                     printf("\nESPACIO MODIFICADO\n");
                     return -1;
                    }else{
                        printf("\nNo hay suficiente espacio libre para agrandar la particion\n");
                        return -1;
                    }
                }else if(chule2>chule && (chule2<chule1 || britany1<=0) && (chule2<chule3 || britany3<=0) ){
                    if(chule0<chule2){
                     mod->mbr_partition2.part_size=chule0;
                     FILE *betzy = fopen(path1,"rb+");
                     fseek(betzy,0,SEEK_SET);
                     fwrite(mod,sizeof(mbr),1,betzy);
                     fclose(betzy);
                     printf("\nESPACIO MODIFICADO\n");
                     return -1;
                    }else{
                        printf("\nNo hay suficiente espacio libre para agrandar la particion\n");
                        return -1;
                    }


                }else if(chule3>chule && (chule3<chule1 || britany1<=0) && (chule3<chule2 || britany2<=0) ){
                    if(chule0<chule3){
                     mod->mbr_partition2.part_size=chule0;
                     FILE *betzy = fopen(path1,"rb+");
                     fseek(betzy,0,SEEK_SET);
                     fwrite(mod,sizeof(mbr),1,betzy);
                     fclose(betzy);
                     printf("\nESPACIO MODIFICADO\n");
                     return -1;
                    }else{
                        printf("\nNo hay suficiente espacio libre para agrandar la particion\n");
                        return -1;
                    }
                }else if(chule0<brayan){


                     mod->mbr_partition2.part_size=chule0;
                     FILE *betzy = fopen(path1,"rb+");
                     fseek(betzy,0,SEEK_SET);
                     fwrite(mod,sizeof(mbr),1,betzy);
                     fclose(betzy);
                     printf("\nESPACIO MODIFICADO\n");


                }

            }

            else if(Menos!=0){
                int tam   =mod->mbr_partition2.part_size;
                int valor =tam-add1;
                if(valor>0){
                    mod->mbr_partition2.part_size=valor;
                    FILE *betzy = fopen(path1,"rb+");
                    fseek(betzy,0,SEEK_SET);
                    fwrite(mod,sizeof(mbr),1,betzy);
                    fclose(betzy);
                    printf("\nESPACIO MODIFICADO\n");


                }else{
                    printf("\nNo hay suficiente espacio libre para modificar la particion\n");
                    return -1;
                }

            }else{
                printf("\nNo hay suficiente espacio libre para modificar la particion\n");
                return -1;
            }

        }
        else if(strcmp(mod->mbr_partition3.part_name,name1)==0){
            if(Menos==0){
                int chule  = mod->mbr_partition3.part_start+mod->mbr_partition3.part_size;
                int chule0 =chule+add1;
                int chule1 = mod->mbr_partition2.part_start;
                int chule2 = mod->mbr_partition1.part_start;
                int chule3 = mod->mbr_partition4.part_start;

                int britany1 = mod->mbr_partition2.part_size;
                int britany2 = mod->mbr_partition1.part_size;
                int britany3 = mod->mbr_partition4.part_size;

                int brayan =mod->mbr_tamanio;


                if(chule1>chule && (chule1<chule2 || britany2<=0) && (chule1<chule3 || britany3<=0) ){
                    if(chule0<chule1){
                     mod->mbr_partition3.part_size=chule0;
                     FILE *betzy = fopen(path1,"rb+");
                     fseek(betzy,0,SEEK_SET);
                     fwrite(mod,sizeof(mbr),1,betzy);
                     fclose(betzy);
                     printf("\nESPACIO MODIFICADO\n");
                     return -1;
                    }else{
                        printf("\nNo hay suficiente espacio libre para agrandar la particion\n");
                        return -1;
                    }
                }else if(chule2>chule && (chule2<chule1 || britany1<=0) && (chule2<chule3 || britany3<=0) ){
                    if(chule0<chule2){
                     mod->mbr_partition3.part_size=chule0;
                     FILE *betzy = fopen(path1,"rb+");
                     fseek(betzy,0,SEEK_SET);
                     fwrite(mod,sizeof(mbr),1,betzy);
                     fclose(betzy);
                     printf("\nESPACIO MODIFICADO\n");
                     return -1;
                    }else{
                        printf("\nNo hay suficiente espacio libre para agrandar la particion\n");
                        return -1;
                    }


                }else if(chule3>chule && (chule3<chule1 || britany1<=0) && (chule3<chule2 || britany2<=0) ){
                    if(chule0<chule3){
                     mod->mbr_partition3.part_size=chule0;
                     FILE *betzy = fopen(path1,"rb+");
                     fseek(betzy,0,SEEK_SET);
                     fwrite(mod,sizeof(mbr),1,betzy);
                     fclose(betzy);
                     printf("\nESPACIO MODIFICADO\n");
                     return -1;
                    }else{
                        printf("\nNo hay suficiente espacio libre para agrandar la particion\n");
                        return -1;
                    }
                }else if( chule0<brayan){


                     mod->mbr_partition3.part_size=chule0;
                     FILE *betzy = fopen(path1,"rb+");
                     fseek(betzy,0,SEEK_SET);
                     fwrite(mod,sizeof(mbr),1,betzy);
                     fclose(betzy);
                     printf("\nESPACIO MODIFICADO\n");



                }

            }

            else if(Menos!=0){
                int tam   =mod->mbr_partition3.part_size;
                int valor =tam-add1;
                if(valor>0){
                    mod->mbr_partition3.part_size=valor;
                    FILE *betzy = fopen(path1,"rb+");
                    fseek(betzy,0,SEEK_SET);
                    fwrite(mod,sizeof(mbr),1,betzy);
                    fclose(betzy);
                    printf("\nESPACIO MODIFICADO\n");


                }else{
                    printf("\nNo hay suficiente espacio libre para modificar la particion\n");
                    return -1;
                }

            }else{
                printf("\nNo hay suficiente espacio libre para modificar la particion\n");
                return -1;
            }

        }
        else if(strcmp(mod->mbr_partition4.part_name,name1)==0){

            if(Menos==0){
                int chule  = mod->mbr_partition4.part_start+mod->mbr_partition4.part_size;
                int chule0 =chule+add1;
                int chule1 = mod->mbr_partition2.part_start;
                int chule2 = mod->mbr_partition3.part_start;
                int chule3 = mod->mbr_partition1.part_start;

                int britany1 = mod->mbr_partition2.part_size;
                int britany2 = mod->mbr_partition3.part_size;
                int britany3 = mod->mbr_partition1.part_size;

                int brayan =mod->mbr_tamanio;


                if(chule1>chule && (chule1<chule2 || britany2<=0) && (chule1<chule3 || britany3<=0) ){
                    if(chule0<chule1){
                     mod->mbr_partition4.part_size=chule0;
                     FILE *betzy = fopen(path1,"rb+");
                     fseek(betzy,0,SEEK_SET);
                     fwrite(mod,sizeof(mbr),1,betzy);
                     fclose(betzy);
                     printf("\nESPACIO MODIFICADO\n");
                     return -1;
                    }else{
                        printf("\nNo hay suficiente espacio libre para agrandar la particion\n");
                        return -1;
                    }
                }else if(chule2>chule && (chule2<chule1 || britany1<=0) && (chule2<chule3 || britany3<=0) ){
                    if(chule0<chule2){
                     mod->mbr_partition4.part_size=chule0;
                     FILE *betzy = fopen(path1,"rb+");
                     fseek(betzy,0,SEEK_SET);
                     fwrite(mod,sizeof(mbr),1,betzy);
                     fclose(betzy);
                     printf("\nESPACIO MODIFICADO\n");
                     return -1;
                    }else{
                        printf("\nNo hay suficiente espacio libre para agrandar la particion\n");
                        return -1;
                    }


                }else if(chule3>chule && (chule3<chule1 || britany1<=0) && (chule3<chule2 || britany2<=0) ){
                    if(chule0<chule3){
                     mod->mbr_partition4.part_size=chule0;
                     FILE *betzy = fopen(path1,"rb+");
                     fseek(betzy,0,SEEK_SET);
                     fwrite(mod,sizeof(mbr),1,betzy);
                     fclose(betzy);
                     printf("\nESPACIO MODIFICADO\n");
                     return -1;
                    }else{
                        printf("\nNo hay suficiente espacio libre para agrandar la particion\n");
                        return -1;
                    }
                }else if(chule0<brayan){


                     mod->mbr_partition4.part_size=chule0;
                     FILE *betzy = fopen(path1,"rb+");
                     fseek(betzy,0,SEEK_SET);
                     fwrite(mod,sizeof(mbr),1,betzy);
                     fclose(betzy);
                     printf("\nESPACIO MODIFICADO\n");
                     return -1;


                }

            }

            else if(Menos!=0){
                int tam   =mod->mbr_partition4.part_size;
                int valor =tam-add1;
                if(valor>0){
                    mod->mbr_partition4.part_size=valor;
                    FILE *betzy = fopen(path1,"rb+");
                    fseek(betzy,0,SEEK_SET);
                    fwrite(mod,sizeof(mbr),1,betzy);
                    fclose(betzy);
                    printf("\nESPACIO MODIFICADO\n");


                }else{
                    printf("\nNo hay suficiente espacio libre para modificar la particion\n");
                    return -1;
                }

            }else{
                printf("\nNo hay suficiente espacio libre para modificar la particion\n");
                return -1;
            }
        }else{
            printf("\nNOMBRE DE PARTICION NO EXISTE\n");
            return -1;
        }


    }

    return 1;
}
int IniciarParticion(int a1,int a2, int b1, int b2, int c1, int c2, int size, int sizedisco){
    //a1, b1 ,c1 son los inicios de las particiones, y a2, b2, c2 son el size de cada particion
    int aux1;
    int ref=-10;

    int cero   =-1;
    int uno    =-1;
    int dos    =-1;
    int tres   =-1;
    int cuatro =-1;
    int cinco  =-1;
    int seis   =-1;
    int siete  =-1;
    int nueve  =-1;
    int diez   =-1;
    int once   =-1;
    int doce   =-1;

    int arr[15];
    arr[1]=cero;
    arr[2]=uno;
    arr[3]=dos;
    arr[4]=tres;
    arr[5]=cuatro;
    arr[6]=cinco;
    arr[7]=seis;
    arr[8]=siete;
    arr[9]=nueve;
    arr[10]=diez;
    arr[11]=once;
    arr[12]=doce;
    //printf("\nvalor a2: %d\n",a2);
    if(a2==0 && b2==0 && c2==0){
          return sizeof(mbr);
      }
    if((a1<=b1 || b2<1) && (a1<=c1|| c2<1) && a2>0){
        /**************PARA A MENOR***********************************/
        ref=1;
        if(size<=a1){
            cero =0;
            arr[1]=a1;
        }
        if(size<=a1){
            return 0;
        }
        if((b1<c1 ||c2<=0) && b2>0){//MODIFIQUE PUSE LOS IGUALES

            /***********************************************/

            aux1=b1-(a1+a2);
            if(size<=aux1 ){
                uno= a1+a2;
                arr[2]=aux1;
            }

            if(c2<=0){
             aux1=sizedisco-(b1+b2);
             if(size<=aux1){
                 arr[3]=aux1;
                dos =b1+b2;
             }

            }else{
                aux1=c1-(b1+b2);
                if(size<=aux1){
                    tres =b1+b2;
                    arr[4]=aux1;
                }else if((sizedisco-c1-c2)>=size){
                    tres =c1+c2;
                    arr[4]=sizedisco-c1-c2;
                }

            }

            /**************************************/


        }
        if((c1<b1 || b2<=0) && c2>0){

            /*************************************/
            aux1=c1-(a1+a2);
            if(size<=aux1 ){
                cuatro= a1+a2;
                arr[5]=aux1;
            }

            if(b2<=0){
             aux1=sizedisco-(c1+c2);
             if(size<=aux1){
                cinco =c1+c2;
                arr[6]=aux1;
             }

            }else{
                aux1=b1-(c1+c2);
                if(size<=aux1){
                    seis =c1+c2;
                    arr[7]=aux1;
                }else if((sizedisco-b1-b2)>=size){
                    seis =b1+b2;
                    arr[7]=sizedisco-b1-b2;
                }
            }


            /************************************/


        }
        if(c2>0 && b2<=0){

            /*************************************/
            aux1=c1-(a1+a2);
            if(size<=aux1 ){
                nueve= a1+a2;//crear variable int nueve;
                arr[9]=aux1;
            }

             aux1=sizedisco-(c1+c2);
             if(size<=aux1){
                diez =c1+c2;//crear variable int diez
                arr[10]=aux1;
             }

            /************************************/
            }
        if(b2>0 && c2<=0){

                    /*************************************/
                    aux1=b1-(a1+a2);
                    if(size<=aux1 ){
                        once= a1+a2;//crear variable int once;
                        arr[11]=aux1;
                    }

                     aux1=sizedisco-(b1+b2);
                     if(size<=aux1){
                        doce =b1+b2;//crear variable int doce
                        arr[12]=aux1;
                     }

                    /************************************/
                    }
         aux1= sizedisco-(a1+a2);
        if(size<=aux1 && b2<=0 && c2<=0){
                siete =a1+a2;
                arr[8]=aux1;
            }

        /**************FIN A MENOR***********************************/
    }
    else if((b1<=a1|| a2<1) && (b1<=c1 || c2<1) && b2>0){
        /**************PARA B MENOR***********************************/
        ref=1;
        if(size<=b1){
                cero=0;
               arr[1]=b1;
        }
        if(size<=b1 ){
            return 0;
        }
        if((a1<c1 || c2<=0) && a2>0){

            /***********************************************/

            aux1=(a1)-(b1+b2);
            if(size<=aux1 ){
                uno= b1+b2;
                arr[2]=aux1;
            }

            if(c2<=0){
             aux1=sizedisco-(a1+a2);
             if(size<=aux1){
                dos =a1+a2;
                arr[3]=aux1;
             }

            }else{
                aux1=c1-(a1+a2);
                if(size<=aux1){
                    tres =a1+a2;
                    arr[4]=aux1;
                }else if((sizedisco-c1-c2)>=size){
                    tres =c1+c2;
                    arr[4]=sizedisco-c1-c2;
                }

            }

            /**************************************/


        }
        if((c1<a1 || a2<=0)&& c2>0){

            /*************************************/
            aux1=(c1)-(b1+b2);
            if(size<=aux1 ){
                cuatro= b1+b2;
                arr[5]=aux1;
            }

            if(a2<=0){
             aux1=sizedisco-(c1+c2);
             if(size<=aux1){
                cinco =c1+c2;
                arr[6]=aux1;
             }

            }else{
                aux1=a1-(c1+c2);
                if(size<=aux1){
                    seis =c1+c2;
                    arr[7]=aux1;
                }else if((sizedisco-a1-a2)>=size){
                    seis =a1+a2;
                    arr[4]=sizedisco-a1-a2;
                }

               }

            /************************************/


        }
        if(c2>0 && a2<=0){

            /*************************************/
            aux1=c1-(b1+b2);
            if(size<=aux1 ){
                nueve= b1+b2;//crear variable int nueve;
                arr[9]=aux1;
            }

             aux1=sizedisco-(c1+c2);
             if(size<=aux1){
                diez =c1+c2;//crear variable int diez
                arr[10]=aux1;
             }

            /************************************/
            }
        if(a2>0 && c2<=0){

                    /*************************************/
                    aux1=a1-(b1+b2);
                    if(size<=aux1 ){
                        once= a1+a2;//crear variable int once;
                        arr[11]=aux1;
                    }

                     aux1=sizedisco-(a1+a2);
                     if(size<=aux1){
                        doce =a1+a2;//crear variable int doce
                        arr[12]=aux1;
                     }

                    /************************************/
                    }
        aux1= sizedisco-(b1+b2);
        if(size<=aux1 && a2<=0 && c2<=0){
                siete =b1+b2;
                arr[8]=aux1;
            }



        /**************FIN B MENOR***********************************/

    }
    else if((c1<=a1|| a2<1) && (c1<=b1|| b2<1) && c2>0){

        /**************PARA C MENOR***********************************/
        ref=1;
        if(size<=c1){
                cero=0;
                arr[1]=aux1;
        }
        if(size<=c1 ){
            return 0;
        }
        if((a1<b1 || b2<=0) && a2>0){

            /***********************************************/

            aux1=(a1)-(c1+c2);
            if(size<=aux1 ){
                uno= c1+c2;
                arr[2]=aux1;
            }

            if(b2<=0){
             aux1=sizedisco-(a1+a2);
             if(size<=aux1){
                dos =a1+a2;
                arr[3]=aux1;
             }

            }else{
                aux1=b1-(a1+a2);
                if(size<=aux1){
                    tres =a1+a2;
                    arr[4]=aux1;
                }else if((sizedisco-b1-b2)>=size){
                    tres =b1+b2;
                    arr[4]=sizedisco-b1-b2;
                }
            }


            /**************************************/


        }
        if((b1<a1 || a2<=0)&& b2>0){

            /*************************************/
            aux1=(b1)-(c1+c2);
            if(size<=aux1 ){
                cuatro= c1+c2;
                arr[5]=aux1;
            }

            if(a2<=0){
             aux1=sizedisco-(b1+b2);
             if(size<=aux1){
                cinco =b1+b2;
                arr[6]=aux1;
             }

            }else{
                aux1=a1-(b1+b2);
                if(size<=aux1){
                    seis =b1+b2;
                    arr[7]=aux1;
                }else if((sizedisco-a1-a2)>=size){
                    seis =a1+a2;
                    arr[4]=sizedisco-a1-a2;
                }

            }

            /************************************/


        }
        if(b2>0 && a2<=0){

                    /*************************************/
                    aux1=b1-(c1+c2);
                    if(size<=aux1 ){
                        nueve= c1+c2;//crear variable int nueve;
                        arr[9]=aux1;
                    }

                     aux1=sizedisco-(b1+b2);
                     if(size<=aux1){
                        diez =b1+b2;//crear variable int diez
                        arr[10]=aux1;
                     }

                    /************************************/
                    }
        if(a2>0 && b2<=0){

                            /*************************************/
                            aux1=a1-(c1+c2);
                            if(size<=aux1 ){
                                once= a1+a2;//crear variable int once;
                                arr[11]=aux1;
                            }

                             aux1=sizedisco-(a1+a2);
                             if(size<=aux1){
                                doce =a1+a2;//crear variable int doce
                                arr[12]=aux1;
                             }

                            /************************************/
                            }
        aux1= (c1+c2)-sizedisco;
        if(size<=aux1 && a2<=0 && b2<=0){
                siete =c1+c2;
                arr[8]=aux1;
            }



        /**************FIN C MENOR***********************************/
    }
    else{
        printf("\nError al crear el disco\n");
        return -1;
    }
     int u[15];
     u[1]=cero;
     u[2]=uno;
     u[3]=dos;
     u[4]=tres;
     u[5]=cuatro;
     u[6]=cinco;
     u[7]=seis;
     u[8]=siete;
     u[9]=nueve;
     u[10]=diez;
     u[11]=once;
     u[12]=doce;
     int l=1;
     for(l;l<13;l++){
         //printf("\n--------------------------------------------------valor de espacio: %d  valor de l: %d\n",arr[l],l);
     }
    // printf("\n-----------------------------------------------------------valor de size: %d\n",size);



                int n=1;
                int auxiliar =-1;
                int auxiliar2=-1;
                int auxiliar3=-1;
                int auxiliar4=-1;

                for(n;n<=11;n++){

                    if(size<=arr[n] && arr[n]!=-1){
                       auxiliar4=auxiliar3;
                       auxiliar3=u[n];
                       if(auxiliar3<auxiliar4 || auxiliar4==-1){
                           auxiliar =arr[n];
                           auxiliar2=u[n];
                       }else{
                           auxiliar =1;
                           auxiliar2=auxiliar4;
                       }

                    }
                }

                if(auxiliar!=-1){
                 return auxiliar2;
                }else{printf("\nEspacio fragmentado insuficiente para contener esta particion.\n"); return -1;}


}
int mount(char*token){

if((strstr(comprobarcadena,"\"/") && strstr(comprobarcadena,".dsk\"") )|| (strcmp(comprobarcadena,"mount")==0)){
    }else{printf("\nError en el comando\n"); return -1;}

int mostrar=0;
    if(strstr(comprobarcadena,"path") && strstr(comprobarcadena,"name")){}

    else{
    int tm1=re->tam;
     registro *ho=(registro*)malloc(sizeof(registro));
     ho=re->primero;
     while(tm1>=1){
         printf("\nMONTADA: id: %s  name: %s    path: %s \n",ho->id,ho->name,ho->path);
         ho=ho->siguiente;
         tm1--;

     }
     return 0;
     }
    int op =0;
    char *name1[50];
    char *path1[200];
    while(token !=NULL){
        //printf("\nentro a while\n");
        token=strtok(NULL," :");
        if(token==NULL) break;

        if(strstr(token,"-path"))    op=1;
        if(strstr(token,"-name"))    op=2;
     switch(op){
      case 1:
            token=strtok(NULL,":\"\n");
         // token = strtok(NULL," :");
      strcpy(path1,"/");
      char *tok;
      if(strstr(token,"\"")){
        //tok=strtok(token,"\"");
        strcpy(path1,token);
      }else{
          strcpy(path1,token);
      }
      //printf("el path es: %s",path1);
      FILE* comprobar =fopen(path1,"r");
      if(comprobar){
      fclose(comprobar);
      }else{
      printf("\nArchivo Inexistente\n");
      return -1;
      }

          break;
      case 2:
       //   token =strtok(NULL," :");
         token=strtok(NULL,":\"\n");
          strcpy(name1,token);
        //  printf("\nEl nombre es: %s\n",name1);
          if(name1==NULL){
              printf("\nNombre invalido\n");
              return -1;
          }
          break;

      default:
          printf("\ncomandos invalidos\n");
          return -1;
          break;

        }


    }

     FILE *archivo =fopen(path1,"rb+");
     fseek(archivo,0,SEEK_SET);
     mbr*aux =(mbr*)malloc(sizeof(mbr));
     fread(aux,sizeof(mbr),1,archivo);
     fclose(archivo);
     if(archivo){

     int existe_logica= verificar_nombre_logica(name1,path1);
         //printf("\nabrio archivo\n");
     if(existe_logica==1 || strcmp(aux->mbr_partition1.part_name,name1)==0 || strcmp(aux->mbr_partition2.part_name,name1)==0 || strcmp(aux->mbr_partition3.part_name,name1)==0 ||strcmp(aux->mbr_partition4.part_name,name1)==0){

         registro* nuevo =(registro*)malloc(sizeof(registro));
         strcpy(nuevo->name,name1);
         strcpy(nuevo->path,path1);
         if(existe_logica==1){


          int no_part_extendida=0;//si no se encuentra la particion tomaremos la extendida para ver si el id pertenece a una logica
            partition particion; //en esta variable guardo los datos de la particion extendida si en dado caso existiera
            if(strcmp(aux->mbr_partition1.part_type,"e")==0){no_part_extendida=1;  particion=aux->mbr_partition1;}
            else if(strcmp(aux->mbr_partition2.part_type,"e")==0){no_part_extendida=2;  particion=aux->mbr_partition2;}
            else if(strcmp(aux->mbr_partition3.part_type,"e")==0){no_part_extendida=3;  particion=aux->mbr_partition3;}
            else if(strcmp(aux->mbr_partition4.part_type,"e")==0){no_part_extendida=4;  particion=aux->mbr_partition4;}

            nuevo->start= get_inicio_logica(particion.part_name,path1,particion); //mando a traer el inicio de donde es que tengo que escribirel inicio del istema de archivos


         }else{
                  if(strstr(aux->mbr_partition1.part_type,name1)){nuevo->start=aux->mbr_partition1.part_start;}
            else if(strstr(aux->mbr_partition1.part_type,name1)){nuevo->start=aux->mbr_partition2.part_start ;}
            else if(strstr(aux->mbr_partition1.part_type,name1)){nuevo->start=aux->mbr_partition3.part_start ;}
            else if(strstr(aux->mbr_partition1.part_type,name1)){nuevo->start=aux->mbr_partition4.part_start ;}


         }
         char *id11[10];
         if(re->tam==0){
             strcpy(nuevo->id,"vda1");
         }else{
             registro* nodo =(registro*)malloc(sizeof(registro));
             nodo=re->primero;
             int rec=re->tam;

             registro* nodo123 =(registro*)malloc(sizeof(registro));
             nodo123=re->primero;
             int rec123=re->tam;


             int existe=0;
             char *vd[10];//letra del id
             int  *num;//numero que corresponde al id
             char *to;
             char* to2;


             while(rec>=1){
                 if(strcmp(nodo->path,path1)==0){
                     addlista(nodo->id);

                     char *superid[20];
                     strcpy(superid,nodo->id);
                     to=strtok(superid,"vd1234567890");

                     if(to==NULL){
                         char *superid1[20];
                         strcpy(superid1,nodo->id);
                         to2=strtok(superid1,"v");
                         if(strstr(to2+1,"d")==0){
                             strcpy(vd,"d");
                         }else{
                             strcpy(vd,"v");
                         }
                     }else{
                     strcpy(vd,to);
                     }


                     existe=1;
                 }
                nodo =nodo->siguiente;
                    rec--;
             }
             num = traernumero();
             vaciarlista();

             if(existe==0){
              //   printf("\nid uno en existe 0: %s\n",re->primero->id);
                 nodo=re->primero;
                 rec=re->tam;
                 char* vd1[10];
                 int ban[30];
                 int i=0;
                 //printf("\nid uno en existe 0.1: %s\n",re->primero->id);
                 for(i;i<=30;i++){
                     ban[i]=-1;
                 }
                 int ii=0;
                for(ii;ii<=30;ii++){
               // printf("\ntodos menos uno valo de ban en %d:    %d \n",ii,ban[ii]);
                }
                 while(rec>=1){
                     char *superid3[20];
                     strcpy(superid3,nodo->id);
                     to=strtok(superid3,"vd1234567890");

                     if(to==NULL){
                         char *superid4[20];
                         strcpy(superid4,nodo->id);
                         to2=strtok(superid4,"v");
                         if(strstr(to2+1,"d")==0){
                             strcpy(vd1,"d");
                         }else{
                             strcpy(vd1,"v");
                         }
                     }else{
                     strcpy(vd1,to);
                    // printf("\nletra ya existe: %s\n",vd1);
                     }

                     if(strcmp(vd1,"a")==0){ ban [1]=1;};
                     if(strcmp(vd1,"b")==0){ ban [2]=1;};
                     if(strcmp(vd1,"c")==0){ ban [3]=1;};
                     if(strcmp(vd1,"d")==0){ ban [4]=1;};
                     if(strcmp(vd1,"e")==0){ ban [5]=1;};
                     if(strcmp(vd1,"f")==0){ ban [6]=1;};
                     if(strcmp(vd1,"g")==0){ ban [7]=1;};
                     if(strcmp(vd1,"h")==0){ ban [8]=1;};
                     if(strcmp(vd1,"i")==0){ ban [9]=1;};
                     if(strcmp(vd1,"j")==0){ ban [10]=1;};
                     if(strcmp(vd1,"k")==0){ ban [11]=1;};
                     if(strcmp(vd1,"l")==0){ ban [12]=1;};
                     if(strcmp(vd1,"m")==0){ ban [13]=1;};
                     if(strcmp(vd1,"n")==0){ ban [14]=1;};
                     if(strcmp(vd1,"o")==0){ ban [15]=1;};
                     if(strcmp(vd1,"p")==0){ ban [16]=1;};
                     if(strcmp(vd1,"q")==0){ ban [17]=1;};
                     if(strcmp(vd1,"r")==0){ ban [18]=1;};
                     if(strcmp(vd1,"s")==0){ ban [19]=1;};
                     if(strcmp(vd1,"t")==0){ ban [20]=1;};
                     if(strcmp(vd1,"u")==0){ ban [21]=1;};
                     if(strcmp(vd1,"v")==0){ ban [22]=1;};
                     if(strcmp(vd1,"w")==0){ ban [23]=1;};
                     if(strcmp(vd1,"x")==0){ ban [24]=1;};
                     if(strcmp(vd1,"y")==0){ ban [25]=1;};
                     if(strcmp(vd1,"z")==0){ ban [26]=1;};


                    nodo =nodo->siguiente;
                        rec--;
                 }

                 int iii=1;
                for(iii;iii<=30;iii++){
             //   printf("\nsolo el 2 debe dar uno %d:    %d \n",iii,ban[iii]);
                }
                 int j=1;
                 int guarda;
                 for(j;j<29;j++){

                     if(ban[j]==-1){
                     guarda=j;
                    // printf("\nvalor de guarda: %d\n",guarda);
                     break;
                     }
                 }



                      if(guarda==1){strcpy(vd,"a");}
                 else if(guarda==2){strcpy(vd,"b");}
                 else if(guarda==3){strcpy(vd,"c");}
                 else if(guarda==4){strcpy(vd,"d");}
                 else if(guarda==5){strcpy(vd,"e");}
                 else if(guarda==6){strcpy(vd,"f");}
                 else if(guarda==7){strcpy(vd,"g");}
                 else if(guarda==8){strcpy(vd,"h");}
                 else if(guarda==9){strcpy(vd,"i");}
                 else if(guarda==10){strcpy(vd,"j");}
                 else if(guarda==11){strcpy(vd,"k");}
                 else if(guarda==12){strcpy(vd,"l");}
                 else if(guarda==13){strcpy(vd,"m");}
                 else if(guarda==14){strcpy(vd,"n");}
                 else if(guarda==15){strcpy(vd,"o");}
                 else if(guarda==16){strcpy(vd,"p");}
                 else if(guarda==17){strcpy(vd,"q");}
                 else if(guarda==18){strcpy(vd,"r");}
                 else if(guarda==19){strcpy(vd,"s");}
                 else if(guarda==20){strcpy(vd,"t");}
                 else if(guarda==21){strcpy(vd,"u");}
                 else if(guarda==22){strcpy(vd,"v");}
                 else if(guarda==23){strcpy(vd,"w");}
                 else if(guarda==24){strcpy(vd,"x");}
                 else if(guarda==25){strcpy(vd,"y");}
                 else if(guarda==26){strcpy(vd,"z");}

             }
             char cadena1[10];

            // printf("\nnumero justo antes de pasarlo: %d\n",num);
             if(existe==0){
             sprintf(cadena1, "%d", 1);
             }else{

                 sprintf(cadena1, "%d", num);
             }
           //  printf("\nnumero justo despues de pasarlo: %s\n",cadena1);
             strcpy(id11,"vd");
             strcat(id11,vd);
             strcat(id11,cadena1);
             strcpy(nuevo->id,id11);
             printf("\nid de la nueva montada:  %s\n",nuevo->id);

         }
         nuevo->anterior=NULL;
         nuevo->siguiente=NULL;


         if(re->tam==0){
             re->primero=re->ultimo=nuevo;
             re->tam++;
         }
         else{
             nuevo->anterior=re->ultimo;
            re->ultimo->siguiente=nuevo;
            re->ultimo=nuevo;
            re->tam++;

         }

     }else{
         printf("\nParticion solicitada no existe\n");
         return -1;
     }

    }

     int tm1=re->tam;
     registro *ho=(registro*)malloc(sizeof(registro));
     ho=re->primero;
     while(tm1>=1){
         printf("\nMONTADA: id: %s  name: %s    path: %s \n",ho->id,ho->name,ho->path);
         ho=ho->siguiente;
         tm1--;

     }
     return 0;

}
int umount(char*token){

    if(strstr(comprobarcadena,"id")){ }else{printf("\nError en comando;\n"); return -1;}

    int op =0;
    char *id1[50];
    while(token !=NULL){
        token=strtok(NULL," :");

        if(token==NULL) break;

        if(strstr(token,"-id"))    op=1;
         switch(op){
            case 1:
             token = strtok(NULL," :");
             strcpy(id1,token);

    registro* aux =(registro *)malloc(sizeof(registro));
    registro* aux1 =(registro *)malloc(sizeof(registro));
    registro* aux2 =(registro *)malloc(sizeof(registro));
    aux=re->primero;
    int n=re->tam;
    int encontrado=0;
    while(n>=1){

        if(strcmp(id1,aux->id)==0){
            encontrado=1;
            break;
        }
        aux=aux->siguiente;
        n--;
    }
    //printf("\nid de auxilair: %s\n",aux->id);
    if(encontrado==1){

      if(re->tam>1){
           if(aux->siguiente==NULL){
                 aux1->anterior=NULL;
              }else{aux1=aux->siguiente;}


            if(aux->anterior==NULL){
                 aux2->siguiente=NULL;
             }else{aux2=aux->anterior;}

             re->tam--;

            if(aux1->anterior==NULL){}
            else{aux1->anterior=aux2;}

            if(aux2->siguiente==NULL){}
            else{aux2->siguiente=aux1;}

       //------------------------------
       if(re->primero==aux){
           re->primero=aux->siguiente;
       }
       else if(re->ultimo==aux){
           re->ultimo=aux->anterior;
       }//-----------------------
      free(aux);
       }else{
           re->primero=NULL;
           re->ultimo=NULL;
           re->tam=0;
           free(aux);
       }

    }else{printf("\nNo existe una particion montada con ese id\n");}


          //   printf("\nel id es: %s\n",id1);
             break;

         default:
          printf("\ncomandos invalidos\n");
          return -1;
          break;
        }
    }





    int tm1=re->tam;
    registro *ho=(registro*)malloc(sizeof(registro));
    ho=re->primero;
    while(tm1>=1){
        printf("\nMONTADA: id: %s  name: %s    path: %s \n",ho->id,ho->name,ho->path);
        ho=ho->siguiente;
        tm1--;

    }
    return 0;

}
void addlista(char* id[20]){
    char *tok;
     nodolista *nuevo=(nodolista*)malloc(sizeof(nodolista));
     nuevo->anterior=(nodolista*)malloc(sizeof(nodolista));
     nuevo->siguiente=(nodolista*)malloc(sizeof(nodolista));
     nuevo->anterior=NULL;
     nuevo->siguiente=NULL;
     tok=strtok(id,"abcdefghijklmnopqrstuvwxyz");
     int numero =atoi(tok);
     nuevo->numero=numero;
     int tam= listanumeros->tam;

     if(tam==0){
         listanumeros->primero=listanumeros->ultimo=nuevo;
         listanumeros->tam++;
     }else{
         nuevo->anterior=listanumeros->ultimo;
        listanumeros->ultimo->siguiente=nuevo;
        listanumeros->ultimo=nuevo;
        listanumeros->tam++;
     }

}
int traernumero(){
int aux1=1;
int tam =listanumeros->tam;

nodolista* auxiliar =(nodolista*)malloc(sizeof(nodolista));
auxiliar=listanumeros->primero;

    while(tam>=1){
        if(auxiliar->numero ==aux1){
            aux1++;
        }
        auxiliar=auxiliar->siguiente;
        tam--;
    }
    return aux1;
}
void vaciarlista(){
    listanumeros->primero=NULL;
    listanumeros->ultimo=NULL;
    listanumeros->tam=0;
}
int rep(char *token){
/********************************************************************************************************************************************************************/
    if(strstr(comprobarcadena,"\"/") && strstr(comprobarcadena,"\"")){
    }else{printf("\nError en el comando\n"); return -1;}
    int    op = 0;
    char*  path1[200];
    char*  name1[50];
    char*  id1[20];

    char dpth[200];
    char dpth2[200];
    char dpth3[200];



       if(strstr(comprobarcadena,"-path") && strstr(comprobarcadena,"-name") && strstr(comprobarcadena,"-id")){

       }else{
           printf("\nFalta Informacion\n");
       return -1;
       }
       if(strstr(comprobarcadena,"mbr") || strstr(comprobarcadena,"disk")|| strstr(comprobarcadena,"sb")|| strstr(comprobarcadena,"inode") || strstr(comprobarcadena,"block")|| strstr(comprobarcadena,"tree")|| strstr(comprobarcadena,"bm_inode")|| strstr(comprobarcadena,"bm_block")|| strstr(comprobarcadena,"journaling") ){

       }else{
           printf("\nnombre de reporte no especificado.\n");
       return -1;
       }

    while(token !=NULL){
        token=strtok(NULL," :");
        if(token==NULL) break;
        if(strstr(token,"-path"))    op=1;
        if(strstr(token,"-name"))    op=2;
        if(strstr(token,"-id"))    op=3;
     switch(op){
      case 1:

        token=strtok(NULL,":\"\n");
        if(strstr(token,"\"")){
           // printf("token3  ------%s\n",token);
            strcpy(dpth,token);
            strcpy(dpth2,dpth);
            strcpy(dpth3,dpth);
        }else{
            //token = strtok(NULL,":");
            strcpy(dpth,token);
            strcpy(dpth2,token);
            strcpy(dpth3,token);
        }


      //  printf("dpth %s\n",dpth);
       // printf("dpth2 %s\n",dpth2);
       // printf("dpth3 %s\n",dpth3);

        break;

       /*  token = strtok(NULL," :");

         if(strstr(token,"\"")){
             strcpy(dpth,token+1);
             token=strtok(NULL,"\"");
             strcat(dpth," ");
             strcat(dpth,token);
             strcpy(dpth2,dpth);
             strcpy(dpth3,dpth);
         }else{
             strcpy(dpth,token);
             strcpy(dpth2,token);
             strcpy(dpth3,token);
         }
         break;*/

      case 2:

      token=strtok(NULL,":\"\n");
        if(strstr(token,"\"")){
           // printf("token3  ------%s\n",token);
            strcpy(name1,token);
        }else{
            //token = strtok(NULL,":");
            strcpy(name1,token);
        }

      /******************************/
         // token =strtok(NULL," :");
       //   strcpy(name1,token);


          if(name1==NULL){
              printf("\nNombre invalido\n");
              return -1;
          }
          break;

     case 3:
         token =strtok(NULL," :");
         strcpy(id1,token);
         if(id1==NULL){
             printf("\nid invalido\n");
             return -1;
         }
         break;

      default:
          printf("\ncomandos invalidos\n");
          return -1;
          break;

        }

    }
        //printf("\n--path: %s path: %s\n",dpth3,dpth);

    /********************CREANDO PATH***********************/
    //creando path
     //SE CREA EL PATH

    //strcat(dpth,Nombre);
    //strcat(dpth2,Nombre);
    //strcat(dpth3,Nombre);
    int numPalabras =1;
    char *tem;
    tem =strtok(dpth,"/");

    while(tem!=NULL){
        tem =strtok(NULL,"/");
        if(tem==NULL) break;
        numPalabras++;
    }

    char pathReal[200]="/";
    char*aux11;
    aux11= strtok(dpth2,"/");
    strcat(pathReal,aux11);
    strcat(pathReal,"/");
    while(numPalabras > 2)
    {
        aux11 = strtok(NULL,"/");
        strcat(pathReal,aux11);
        strcat(pathReal,"/");
        mkdir(pathReal ,S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
        numPalabras--;
    }

    aux11 = strtok(NULL,"/");

        /********************FN DE CREAR PATH***********************/

    /********************VERIFICAR EXISTENCIA DE ID***********************/
    char mandar[20]="";
    char path_mandar[200]="";
    registro* aux1=(registro*)malloc(sizeof(registro));
    aux1=re->primero;
    int tam = re->tam;
    if(tam==0){
    printf("\nNo hay particiones montadas.\n");
    return -1;
    }

        while(tam>=1){

            if(strcmp(aux1->id,id1)==0){
                    strcpy(mandar,aux1->name);
                    strcpy(path_mandar,aux1->path);
                break;
            }
            aux1=aux1->siguiente;
            tam--;
            if(tam==0){printf("\nNo se encontro el id\n"); return -1;}
        }




    /********************FIN DE VERIFICAR EXISTENCIA DE ID***********************/
    /********************TRAYENDO EL DISCO COMPLETO Y CARGAR TODAS SUS PARTICIONES****************************/
        mbr* cargambr=(mbr*)malloc(sizeof(mbr));
        FILE*disco=fopen(aux1->path,"rb+");
        fseek(disco,0,SEEK_SET);
        fread(cargambr,sizeof(mbr),1,disco);
        //("--lllllllllllllllllllllll--------------------%s\n",cargambr->mbr_partition1.part_type);
        //printf("\nname1: %s\n",name1);

        if(strcmp(name1,"mbr")==0 ||strcmp(name1,"disk")==0){
            enviarebrs(aux1->path);
            reportedisk* crea =(reportedisk*)malloc(sizeof(reportedisk));
            int ext=0;
            int h=4;
            int contador =1;
            /****Agregando el mbr a la lista de reporte disk***************/
                  crea->anterior==NULL;
                  crea->siguiente==NULL;
                  strcpy(crea->mbr_fecha_creacion,cargambr->mbr_fecha_creacion);
                  crea->mbr_tamanio=cargambr->mbr_tamanio;
                  crea->EsMbr=1;
                  if(reportedisk1->tam==0){
                      reportedisk1->primero=reportedisk1->ultimo=crea;
                      reportedisk1->tam++;
                     // printf("\nmbr: agregado\n");
                  }
            /****Fin de agregar el mbr a la lista de reporte disk**********/

            while(h>0){
                if(contador==1 && cargambr->mbr_partition1.part_size>0){
                   // printf("\nnombre de la particion 1: %s\n",cargambr->mbr_partition1.part_name);
                    addlistareportedisk(cargambr->mbr_partition1);
                    if(strcmp(cargambr->mbr_partition1.part_type,"e")==0){ext=1;};
                }
                if(contador==2 && cargambr->mbr_partition2.part_size>0){
                    addlistareportedisk(cargambr->mbr_partition2);
                    if(strcmp(cargambr->mbr_partition2.part_type,"e")==0){ext=2;};
                }
                if(contador==3 && cargambr->mbr_partition3.part_size>0){
                    addlistareportedisk(cargambr->mbr_partition3);
                    if(strcmp(cargambr->mbr_partition3.part_type,"e")==0){ext=3;};
                }
                if(contador==4 && cargambr->mbr_partition4.part_size>0){
                    addlistareportedisk(cargambr->mbr_partition4);
                    if(strcmp(cargambr->mbr_partition4.part_type,"e")==0){ext=4;};
                }

                contador++;
                h--;
            }

        }


        fclose(disco);

        if(strcmp(name1,"mbr")!=0 ||strcmp(name1,"disk")!=0){
        rep2f(name1,dpth3,mandar,path_mandar,id1);//nom. reporte, direccion imagen, name particion,path disco
        return -1;
        }
        int pq =reportedisk1->tam;
        reportedisk *repaux=(reportedisk*)malloc(sizeof(reportedisk));
        repaux=(reportedisk*)malloc(sizeof(reportedisk));
        repaux=reportedisk1->primero;
        while(pq>=1){
            if(repaux->EsMbr==1){

               // printf("\nCREACION FECHA MBR: %s\n",repaux->mbr_fecha_creacion);
            }else
           // printf("\nnombre de la part: %s\n",repaux->part_name);
            repaux=repaux->siguiente;
         pq--;
        }

    /********************FIN DE TRAER EL DISCO COMPLETO Y CARGAR TODAS SUS PARTICIONES ***********************/


/**********************************************************************************************************************************************************************/

   FILE *archivo = fopen("archivosMBR.dot","w");
   if(strcmp(name1,"mbr")==0){
    char *inicio= "digraph{ \n rankdir = LR; \n node [shape = record, color = lightblue]; \n";
    fprintf(archivo, "%s \n", inicio);
   }
    fclose(archivo);
       if(strcmp(name1,"mbr")==0){
            repMBR();
        }
       if(strcmp(name1,"disk")==0){
            repDISK(aux1->path);
        }
    FILE *archivo1 = fopen("archivosMBR.dot","a");

    char *cd = "}";
    fprintf(archivo1,"%s",cd);


    fclose(archivo1);
    char*direccion[200];
    strcpy(direccion,dpth3);
printf("DIRECCIION PATH: %s\n",dpth3);
     /*******************/
         //;
         char ja[50] = "dot -Tjpg archivosMBR.dot -o ";
         strcat(ja,"\"");
         strcat(ja,dpth3);
         strcat(ja,"\"");
         system(ja);

         printf("\nReporte creado con exito.\n");

     /*******************/
         vaciarlistadereportes();
         vaciarlisebr();
         char ja1[100]="firefox ";
         strcat(ja1,"\"");
        strcat(ja1,dpth3);
        strcat(ja1,"\"");
        system(ja1);
     return 0;
}
void repMBR(){

numcirr++;
    char *nombre[200] ;
    strcpy(nombre,"archivosMBR.dot");
    FILE *archivo1 = fopen(nombre, "a");

    char *c1 = "\nsubgraph clusterESTAD";
    fprintf(archivo1, "%s", c1);
    fprintf(archivo1, "%d", numcirr);

    char *c2 = "{ \n label = ""\" Reporte MBR";
    fprintf(archivo1, "%s", c2);

    char *c3 = """\"; \n color=green; \n";
    fprintf(archivo1, "%s", c3);

    if(reportedisk1->tam==0){}
    else{

            char *cc7 = "nodoec1";
            fprintf(archivo1, "%s", cc7);
            int flor=reportedisk1->tam;
            reportedisk* ingresar =(reportedisk*)malloc(sizeof(reportedisk));
            ingresar = reportedisk1->primero;
            char *cq8 = "[label = ""\" ";
            fprintf(archivo1, "%s", cq8);
            while(flor>=1){

           if(ingresar->EsMbr==1){
               fprintf(archivo1, "mbr_disk_Signature: %d",ingresar->mbr_disk_signature);
               char *ccc8 = " | ";
               fprintf(archivo1, "%s", ccc8);
               fprintf(archivo1, "mbr_fecha_creacion: %s",ingresar->mbr_fecha_creacion);
               char *ccc88 = " | ";
               fprintf(archivo1, "%s", ccc88);
               fprintf(archivo1, "mbr_tamanio_disco: %d",ingresar->mbr_tamanio);
               if(reportedisk1->tam==1){ break;}else{
               char *ccc888 = " | ";
                fprintf(archivo1, "%s", ccc888);
               }


           }
           else if(strcmp(ingresar->part_type,"l")==0 ||  strcmp(ingresar->part_type,"1")==0){}
            else{
            fprintf(archivo1, "part_name: %s",ingresar->part_name);
            char *ccc8 = " | ";
            fprintf(archivo1, "%s", ccc8);
            fprintf(archivo1, "part_fit de %s : %s",ingresar->part_name,ingresar->part_fit);
            char *ccc88 = " | ";
            fprintf(archivo1, "%s", ccc88);
            fprintf(archivo1, "part_size de %s: %d",ingresar->part_name,ingresar->part_size);
            char *ccc888 = " | ";
            fprintf(archivo1, "%s", ccc888);
            fprintf(archivo1, "part_start de %s: %d",ingresar->part_name,ingresar->part_start);
            char *ccc8888 = " | ";
            fprintf(archivo1, "%s", ccc8888);
            fprintf(archivo1, "part_status de de %s: %d",ingresar->part_name,ingresar->part_status);
            char *ccc88888 = " | ";
            fprintf(archivo1, "%s", ccc88888);
            fprintf(archivo1, "part_typede de %s: %s",ingresar->part_name,ingresar->part_type);
            char *ccc888888 = " | ";
            fprintf(archivo1, "%s", ccc888888);
            if(flor==1){break;}
            else{
                char *ccc8888888 = " | ";
                fprintf(archivo1, "%s", ccc8888888);
            }

           }

            ingresar=ingresar->siguiente;
            flor--;
            }

            char *cq9 = """\"];\n";
            fprintf(archivo1, "%s", cq9);

        }

        char *c7 = "}\n";
        fprintf(archivo1, "%s", c7);

        if(listalogicas->tam>0){
            /********************************************************************************************************************/
            char *c1 = "\nsubgraph clusterLOGICAS";
            fprintf(archivo1, "%s", c1);
            fprintf(archivo1, "%d", numcirr);

            char *c2 = "{ \n label = ""\" Reporte MBR Particiones Logicas";
            fprintf(archivo1, "%s", c2);

            char *c3 = """\"; \n color=green; \n";
            fprintf(archivo1, "%s", c3);
                    int cuenta =0;
                    int flor=listalogicas->tam;
                    nodolistaebr* auxi =(nodolistaebr*)malloc(sizeof(nodolistaebr));
                    auxi=listalogicas->primero;

                    while(flor>=1){
                        cuenta++;
                        char *cc7 = "nodoec1";
                        fprintf(archivo1, "%s%d", cc7,cuenta);

                        char *cq8 = "[label = ""\"  ";
                        fprintf(archivo1, "%s", cq8);
                       fprintf(archivo1, "part_fit: %s",auxi->part_fit);
                       char *ccc8 = " | ";
                       fprintf(archivo1, "%s", ccc8);
                       fprintf(archivo1, "part_name: %s",auxi->part_name);
                       char *ccc188 = " | ";
                       fprintf(archivo1, "%s", ccc188);
                       fprintf(archivo1, "part_next: %d",auxi->part_next);
                       char *ccc388 = " | ";
                       fprintf(archivo1, "%s", ccc388);
                       fprintf(archivo1, "part_size: %d",auxi->part_size);

                       char *ccc883= " | ";
                       fprintf(archivo1, "%s", ccc883);
                       fprintf(archivo1, "part_start: %d",auxi->part_start);
                       char *ccc8844 = " | ";
                       fprintf(archivo1, "%s", ccc8844);
                       fprintf(archivo1, "part_status: %d",auxi->part_status);

                       char *cq9 = """\"];\n";
                       fprintf(archivo1, "%s", cq9);

                       auxi=auxi->siguiente;
                    flor--;
                    }

                char *c7 = "}\n";
                fprintf(archivo1, "%s", c7);

            /*******************************************************************************************************************/
        }


        fclose(archivo1);

    }
void enviarebrs(char path1[200]){
    mbr *mbr01=(mbr*)malloc(sizeof(mbr));
     FILE *partc =fopen(path1,"r+b");

    fseek(partc,0,SEEK_SET);
    fread(mbr01,sizeof(mbr),1,partc);
    fclose(partc);

    if(strcmp(mbr01->mbr_partition1.part_type,"e")==0){
        int inicio =mbr01->mbr_partition1.part_start;

          FILE *archivo=fopen(path1,"rb+");
          fseek(archivo,0,SEEK_SET);
          mbr *aux2 =(mbr *)malloc(sizeof(mbr));
          ebr *aux3=(ebr *)malloc(sizeof(ebr));
          fread(aux2,sizeof(mbr),1,archivo);
          fseek(archivo,inicio,SEEK_CUR);
          fread(aux3,sizeof(ebr),1,archivo);
          int o;
          int bandera=0;

          while(bandera==0){
                    logsumasize=logsumasize+aux3->part_size;
              lisebr(aux3->part_fit,aux3->part_name,aux3->part_next,aux3->part_size,aux3->part_start,aux3->part_status);
            o=aux3->part_size;
            fseek(archivo,o,SEEK_CUR);
             if(aux3->part_next==-1){break;}
             else{
                 fseek(archivo,0,SEEK_CUR);
                 fread(aux3,sizeof(ebr),1,archivo);
             }
          }

          fclose(archivo);

    }
    if(strcmp(mbr01->mbr_partition2.part_type,"e")==0){

        int inicio =mbr01->mbr_partition2.part_start;

          FILE *archivo=fopen(path1,"rb+");
          fseek(archivo,0,SEEK_SET);
          mbr *aux2 =(mbr *)malloc(sizeof(mbr));
          ebr *aux3=(ebr *)malloc(sizeof(ebr));
          fread(aux2,sizeof(mbr),1,archivo);
          fseek(archivo,inicio,SEEK_CUR);
          fread(aux3,sizeof(ebr),1,archivo);
          int o;
          int bandera=0;

          while(bandera==0){
              logsumasize=logsumasize+aux3->part_size;
              lisebr(aux3->part_fit,aux3->part_name,aux3->part_next,aux3->part_size,aux3->part_start,aux3->part_status);

            o=aux3->part_size;
            fseek(archivo,o,SEEK_CUR);
             if(aux3->part_next==-1){break;}
             else{
                 fseek(archivo,0,SEEK_CUR);
                 fread(aux3,sizeof(ebr),1,archivo);
             }
          }

          fclose(archivo);
    }
    if(strcmp(mbr01->mbr_partition3.part_type,"e")==0){
        int inicio =mbr01->mbr_partition3.part_start;

          FILE *archivo=fopen(path1,"rb+");
          fseek(archivo,0,SEEK_SET);
          mbr *aux2 =(mbr *)malloc(sizeof(mbr));
          ebr *aux3=(ebr *)malloc(sizeof(ebr));
          fread(aux2,sizeof(mbr),1,archivo);
          fseek(archivo,inicio,SEEK_CUR);
          fread(aux3,sizeof(ebr),1,archivo);
          int o;
          int bandera=0;

          while(bandera==0){
              logsumasize=logsumasize+aux3->part_size;
              lisebr(aux3->part_fit,aux3->part_name,aux3->part_next,aux3->part_size,aux3->part_start,aux3->part_status);

            o=aux3->part_size;
            fseek(archivo,o,SEEK_CUR);
             if(aux3->part_next==-1){break;}
             else{
                 fseek(archivo,0,SEEK_CUR);
                 fread(aux3,sizeof(ebr),1,archivo);
             }
          }

          fclose(archivo);
    }
    if(strcmp(mbr01->mbr_partition4.part_type,"e")==0){
        int inicio =mbr01->mbr_partition4.part_start;

          FILE *archivo=fopen(path1,"rb+");
          fseek(archivo,0,SEEK_SET);
          mbr *aux2 =(mbr *)malloc(sizeof(mbr));
          ebr *aux3=(ebr *)malloc(sizeof(ebr));
          fread(aux2,sizeof(mbr),1,archivo);
          fseek(archivo,inicio,SEEK_CUR);
          fread(aux3,sizeof(ebr),1,archivo);
          int o;
          int bandera=0;

          while(bandera==0){
              logsumasize=logsumasize+aux3->part_size;
              lisebr(aux3->part_fit,aux3->part_name,aux3->part_next,aux3->part_size,aux3->part_start,aux3->part_status);

            o=aux3->part_size;
            fseek(archivo,o,SEEK_CUR);
             if(aux3->part_next==-1){break;}
             else{
                 fseek(archivo,0,SEEK_CUR);
                 fread(aux3,sizeof(ebr),1,archivo);
             }
          }

          fclose(archivo);
    }


}
int verificar_nombre_logica(char *name1[20],char path1[200]){
/********************************************************************/
/********************************************************************/
mbr *mbr01=(mbr*)malloc(sizeof(mbr));
     FILE *partc =fopen(path1,"r+b");

    fseek(partc,0,SEEK_SET);
    fread(mbr01,sizeof(mbr),1,partc);
    fclose(partc);

    if(strcmp(mbr01->mbr_partition1.part_type,"e")==0){
        int inicio =mbr01->mbr_partition1.part_start;

          FILE *archivo=fopen(path1,"rb+");
          fseek(archivo,0,SEEK_SET);
          mbr *aux2 =(mbr *)malloc(sizeof(mbr));
          ebr *aux3=(ebr *)malloc(sizeof(ebr));
          fread(aux2,sizeof(mbr),1,archivo);
          fseek(archivo,inicio,SEEK_CUR);
          fread(aux3,sizeof(ebr),1,archivo);
          int o;
          int bandera=0;

          while(bandera==0){


                    //logsumasize=logsumasize+aux3->part_size;
              //lisebr(aux3->part_fit,aux3->part_name,aux3->part_next,aux3->part_size,aux3->part_start,aux3->part_status);
            o=aux3->part_size;
            fseek(archivo,o,SEEK_CUR);
             if( strcmp(aux3->part_name,name1)==0){
                    return 1;

                    }
             else if(aux3->part_next==-1){

             break;
             }
             else{
                 fseek(archivo,0,SEEK_CUR);
                 fread(aux3,sizeof(ebr),1,archivo);
             }
          }

          fclose(archivo);

    }
    if(strcmp(mbr01->mbr_partition2.part_type,"e")==0){

        int inicio =mbr01->mbr_partition2.part_start;

          FILE *archivo=fopen(path1,"rb+");
          fseek(archivo,0,SEEK_SET);
          mbr *aux2 =(mbr *)malloc(sizeof(mbr));
          ebr *aux3=(ebr *)malloc(sizeof(ebr));
          fread(aux2,sizeof(mbr),1,archivo);
          fseek(archivo,inicio,SEEK_CUR);
          fread(aux3,sizeof(ebr),1,archivo);
          int o;
          int bandera=0;

          while(bandera==0){
              //logsumasize=logsumasize+aux3->part_size;
              //lisebr(aux3->part_fit,aux3->part_name,aux3->part_next,aux3->part_size,aux3->part_start,aux3->part_status);

            o=aux3->part_size;
            fseek(archivo,o,SEEK_CUR);
             if( strcmp(aux3->part_name,name1)==0){
                    return 1;

                    }
             else if(aux3->part_next==-1){
             break;
             }
             else{
                 fseek(archivo,0,SEEK_CUR);
                 fread(aux3,sizeof(ebr),1,archivo);
             }
          }

          fclose(archivo);
    }
    if(strcmp(mbr01->mbr_partition3.part_type,"e")==0){
        int inicio =mbr01->mbr_partition3.part_start;

          FILE *archivo=fopen(path1,"rb+");
          fseek(archivo,0,SEEK_SET);
          mbr *aux2 =(mbr *)malloc(sizeof(mbr));
          ebr *aux3=(ebr *)malloc(sizeof(ebr));
          fread(aux2,sizeof(mbr),1,archivo);
          fseek(archivo,inicio,SEEK_CUR);
          fread(aux3,sizeof(ebr),1,archivo);
          int o;
          int bandera=0;

          while(bandera==0){
             // logsumasize=logsumasize+aux3->part_size;
            //  lisebr(aux3->part_fit,aux3->part_name,aux3->part_next,aux3->part_size,aux3->part_start,aux3->part_status);

            o=aux3->part_size;
            fseek(archivo,o,SEEK_CUR);
             if( strcmp(aux3->part_name,name1)==0){
                    return 1;

                    }
             else if(aux3->part_next==-1){
             break;
             }else{
                 fseek(archivo,0,SEEK_CUR);
                 fread(aux3,sizeof(ebr),1,archivo);
             }
          }

          fclose(archivo);
    }
    if(strcmp(mbr01->mbr_partition4.part_type,"e")==0){
        int inicio =mbr01->mbr_partition4.part_start;

          FILE *archivo=fopen(path1,"rb+");
          fseek(archivo,0,SEEK_SET);
          mbr *aux2 =(mbr *)malloc(sizeof(mbr));
          ebr *aux3=(ebr *)malloc(sizeof(ebr));
          fread(aux2,sizeof(mbr),1,archivo);
          fseek(archivo,inicio,SEEK_CUR);
          fread(aux3,sizeof(ebr),1,archivo);
          int o;
          int bandera=0;

          while(bandera==0){
           //   logsumasize=logsumasize+aux3->part_size;
         //     lisebr(aux3->part_fit,aux3->part_name,aux3->part_next,aux3->part_size,aux3->part_start,aux3->part_status);

            o=aux3->part_size;
            fseek(archivo,o,SEEK_CUR);
             if( strcmp(aux3->part_name,name1)==0){
                    return 1;

                    }
             else if(aux3->part_next==-1){
             break;
             }
             else{
                 fseek(archivo,0,SEEK_CUR);
                 fread(aux3,sizeof(ebr),1,archivo);
             }
          }

          fclose(archivo);
    }
    return 0;
/*********************************************/
/***********************************************/
}
void addlistareportedisk(partition particion){
    reportedisk *aux=(reportedisk*)malloc(sizeof(reportedisk));
    aux->EsMbr=0;
    strcpy(aux->part_fit,particion.part_fit);
    strcpy(aux->part_name,particion.part_name);
    aux->part_size=particion.part_size;
    aux->part_start=particion.part_start;
    aux->part_status=particion.part_status;
    strcpy(aux->part_type,particion.part_type);
    aux->siguiente=NULL;
    aux->anterior=NULL;


    if(reportedisk1->tam==0){
        reportedisk1->primero=reportedisk1->ultimo=aux;
        reportedisk1->tam++;
    }
    else{
        aux->anterior=reportedisk1->ultimo;
       reportedisk1->ultimo->siguiente=aux;
       reportedisk1->ultimo=aux;
       reportedisk1->tam++;
    }

}
void repDISK(char*path1[200]){
    mbr *mbr01=(mbr*)malloc(sizeof(mbr));

     FILE *partc =fopen(path1,"r+b");

    fseek(partc,0,SEEK_SET);
    fread(mbr01,sizeof(mbr),1,partc);
    //printf("\nDENTRO DE --------------------------------------------TYEPE TT%sTT \n",mbr01->mbr_partition1.part_type);
   // printf("\nDENTRO DE --------------------------------------------TYEPE TT%sTT \n",mbr01->mbr_partition2.part_name);
   // printf("\nDENTRO DE --------------------------------------------TYEPE TT%sTT \n",mbr01->mbr_partition3.part_name);

    fclose(partc);
    int part1=mbr01->mbr_partition1.part_start+mbr01->mbr_partition1.part_size;
    int part2=mbr01->mbr_partition2.part_start+mbr01->mbr_partition2.part_size;
    int part3=mbr01->mbr_partition3.part_start+mbr01->mbr_partition3.part_size;
    int part4=mbr01->mbr_partition4.part_start+mbr01->mbr_partition4.part_size;

    int parti1=mbr01->mbr_partition1.part_start;
    int parti2=mbr01->mbr_partition2.part_start;
    int parti3=mbr01->mbr_partition3.part_start;
    int parti4=mbr01->mbr_partition4.part_start;

    int size1=mbr01->mbr_partition1.part_size;
    int size2=mbr01->mbr_partition2.part_size;
    int size3=mbr01->mbr_partition3.part_size;
    int size4=mbr01->mbr_partition4.part_size;
    /************************************************/
    numcirr++;
        char *nombre[200] ;
        strcpy(nombre,"archivosMBR.dot");
        FILE *archivo1 = fopen(nombre, "a");

        char *c1 = "digraph html { abc [shape=none, margin=0,shape = record, color = lightblue, label=<<TABLE BORDER=\"0\" CELLBORDER=\"1\" CELLSPACING=\"0\" \n CELLPADDING=\"4\">\n <TR> \n <TD ROWSPAN=\"3\"><FONT COLOR=\"green\">MBR</FONT><BR/></TD> ";

        fprintf(archivo1, "%s", c1);


    //para uno menor
    if((parti1<parti2 || size2<1) && (parti1<parti3|| size3<1) && (parti1<parti4|| size4<1) && mbr01->mbr_partition1.part_size>0){
      /// printf("\nDENTRO DE IF UNO MENOR\n");

        if(parti1==0){


            if(strcmp(mbr01->mbr_partition1.part_type,"p")==0){
          //  printf("\nDENTRO DE --------------------------------------------PRI UNO\n");
            char *l1="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
            fprintf(archivo1, "%s", l1);
            }else if(strcmp(mbr01->mbr_partition1.part_type,"e")==0){
                char *l1="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                fprintf(archivo1, "%s", l1);
            }
        }else{
            char *l1="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
            fprintf(archivo1, "%s", l1);
            if(strcmp(mbr01->mbr_partition1.part_type,"p")==0){
            char *l1="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
            fprintf(archivo1, "%s", l1);
            }else if(strcmp(mbr01->mbr_partition1.part_type,"e")==0){
                char *l1="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                fprintf(archivo1, "%s", l1);
            }

        }

        if(size2<1 && size3<1 && size4<1 && part1<mbr01->mbr_tamanio){
            char *l1="<TD ROWSPAN=\"3\">LIBRE</TD>";
            fprintf(archivo1, "%s", l1);
        }
        //particion 2 menor
        if((parti2<parti4|| size4<1) &&(parti2<parti3|| size3<1) && mbr01->mbr_partition2.part_size>0)
        {
       // printf("\nDENTRO DE --------------------------------------------\n");
            int j= part1-parti2;
            if(j==0){
                if(strcmp(mbr01->mbr_partition2.part_type,"p")==0){
             //   printf("\nDENTRO DE --------------------------------------------PRI DOS\n");
                char *l1="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                fprintf(archivo1, "%s", l1);
                }else if(strcmp(mbr01->mbr_partition2.part_type,"e")==0){
                    char *l1="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                    fprintf(archivo1, "%s", l1);
                }
            }else{
                char *l1="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                fprintf(archivo1, "%s", l1);
                if(strcmp(mbr01->mbr_partition2.part_type,"p")==0){
                char *l1="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                fprintf(archivo1, "%s", l1);
                }else if(strcmp(mbr01->mbr_partition2.part_type,"e")==0){
                    char *l1="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                    fprintf(archivo1, "%s", l1);
                }

            }
            if(size3<1 && size4<1 && part2<mbr01->mbr_tamanio){
                char *l1="<TD ROWSPAN=\"3\">LIBRE</TD>";
                fprintf(archivo1, "%s", l1);
            }
            //particion 3 menor
            if((parti3<parti4 || size4<1) && mbr01->mbr_partition3.part_size>0){
                int j= parti3-part2;
                if(j==0){
                    if(strcmp(mbr01->mbr_partition3.part_type,"p")==0){
                    char *l1="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                    fprintf(archivo1, "%s", l1);
                    }else if(strcmp(mbr01->mbr_partition3.part_type,"e")==0){
                        char *l1="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                        fprintf(archivo1, "%s", l1);
                    }
                }else{
                    char *l1="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                    fprintf(archivo1, "%s", l1);
                    if(strcmp(mbr01->mbr_partition3.part_type,"p")==0){
                    char *l1="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                    fprintf(archivo1, "%s", l1);
                    }else if(strcmp(mbr01->mbr_partition3.part_type,"e")==0){
                        char *l1="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                        fprintf(archivo1, "%s", l1);
                    }

                }
                if(size4<1 && part3<mbr01->mbr_tamanio){
                    char *l1="<TD ROWSPAN=\"3\">LIBRE</TD>";
                    fprintf(archivo1, "%s", l1);
                }    else  if(mbr01->mbr_partition4.part_size>0){
                    int j= parti4-part3;
                    if(j==0){
                        if(strcmp(mbr01->mbr_partition4.part_type,"p")==0){
                        char *l1="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                        fprintf(archivo1, "%s", l1);
                        }else if(strcmp(mbr01->mbr_partition4.part_type,"e")==0){
                            char *l1="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                            fprintf(archivo1, "%s", l1);
                        }
                    }else{
                        char *l1="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                        fprintf(archivo1, "%s", l1);
                        if(strcmp(mbr01->mbr_partition4.part_type,"p")==0){
                        char *l1="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                        fprintf(archivo1, "%s", l1);
                        }else if(strcmp(mbr01->mbr_partition4.part_type,"e")==0){
                            char *l1="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                            fprintf(archivo1, "%s", l1);
                        }

                    }

                    if(part4<mbr01->mbr_tamanio){
                        char *l1="<TD ROWSPAN=\"3\">LIBRE</TD>";
                        fprintf(archivo1, "%s", l1);
                    }
                }
                     else if(part3<mbr01->mbr_tamanio){
                    char *l1="<TD ROWSPAN=\"3\">LIBRE</TD>";
                    fprintf(archivo1, "%s", l1);
                }
            }
            //particion4 menor
            if((parti4<parti3 || size3<1) && mbr01->mbr_partition4.part_size>0){
                int j= parti4-part2;
                if(j==0){
                    if(strcmp(mbr01->mbr_partition4.part_type,"p")==0){
                    char *l1="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                    fprintf(archivo1, "%s", l1);
                    }else if(strcmp(mbr01->mbr_partition4.part_type,"e")==0){
                        char *l1="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                        fprintf(archivo1, "%s", l1);
                    }
                }else{
                    char *l1="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                    fprintf(archivo1, "%s", l1);
                    if(strcmp(mbr01->mbr_partition4.part_type,"p")==0){
                    char *l1="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                    fprintf(archivo1, "%s", l1);
                    }else if(strcmp(mbr01->mbr_partition4.part_type,"e")==0){
                        char *l1="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                        fprintf(archivo1, "%s", l1);
                    }

                }
                if(size3<1 && part4<mbr01->mbr_tamanio){
                    char *l1="<TD ROWSPAN=\"3\">LIBRE</TD>";
                    fprintf(archivo1, "%s", l1);
                }    else  if(mbr01->mbr_partition3.part_size>0){
                    int j= parti3-part4;
                    if(j==0){
                        if(strcmp(mbr01->mbr_partition3.part_type,"p")==0){
                        char *l1="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                        fprintf(archivo1, "%s", l1);
                        }else if(strcmp(mbr01->mbr_partition3.part_type,"e")==0){
                            char *l1="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                            fprintf(archivo1, "%s", l1);
                        }
                    }else{
                        char *l1="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                        fprintf(archivo1, "%s", l1);
                        if(strcmp(mbr01->mbr_partition3.part_type,"p")==0){
                        char *l1="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                        fprintf(archivo1, "%s", l1);
                        }else if(strcmp(mbr01->mbr_partition3.part_type,"e")==0){
                            char *l1="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                            fprintf(archivo1, "%s", l1);
                        }

                    }

                    if(part3<mbr01->mbr_tamanio){
                        char *l1="<TD ROWSPAN=\"3\">LIBRE</TD>";
                        fprintf(archivo1, "%s", l1);
                    }
                }
                    else if( part4<mbr01->mbr_tamanio){
                    char *l1="<TD ROWSPAN=\"3\">LIBRE</TD>";
                    fprintf(archivo1, "%s", l1);

                }
            }

        }


        /**********************************************************************************************************************/
        //particion 3 menor
        if((parti3<parti4|| size4<1) &&(parti3<parti2|| size2<1) && mbr01->mbr_partition3.part_size>0){
            int j= part1-parti3;
            if(j==0){
                if(strcmp(mbr01->mbr_partition3.part_type,"p")==0){
                char *l1="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                fprintf(archivo1, "%s", l1);
                }else if(strcmp(mbr01->mbr_partition3.part_type,"e")==0){
                    char *l1="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                    fprintf(archivo1, "%s", l1);
                }
            }else{
                char *l1="<TD RO3SPAN=\"3\">LIBRE</TD >\n";
                fprintf(archivo1, "%s", l1);
                if(strcmp(mbr01->mbr_partition3.part_type,"p")==0){
                char *l1="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                fprintf(archivo1, "%s", l1);
                }else if(strcmp(mbr01->mbr_partition3.part_type,"e")==0){
                    char *l1="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                    fprintf(archivo1, "%s", l1);
                }

            }
            if(size2<1 && size4<1 && part3<mbr01->mbr_tamanio){
                char *l1="<TD ROWSPAN=\"3\">LIBRE</TD>";
                fprintf(archivo1, "%s", l1);
            }
            //particion 2 menor
            if((parti2<parti4 || size4<1) && mbr01->mbr_partition2.part_size>0){
                int j= parti2-part3;
                if(j==0){
                    if(strcmp(mbr01->mbr_partition2.part_type,"p")==0){
                    char *l1="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                    fprintf(archivo1, "%s", l1);
                    }else if(strcmp(mbr01->mbr_partition2.part_type,"e")==0){
                        char *l1="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                        fprintf(archivo1, "%s", l1);
                    }
                }else{
                    char *l1="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                    fprintf(archivo1, "%s", l1);
                    if(strcmp(mbr01->mbr_partition2.part_type,"p")==0){
                    char *l1="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                    fprintf(archivo1, "%s", l1);
                    }else if(strcmp(mbr01->mbr_partition2.part_type,"e")==0){
                        char *l1="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                        fprintf(archivo1, "%s", l1);
                    }

                }
                if(size4<1 && part2<mbr01->mbr_tamanio){
                    char *l1="<TD ROWSPAN=\"3\">LIBRE</TD>";
                    fprintf(archivo1, "%s", l1);
                }    else  if(mbr01->mbr_partition4.part_size>0){
                    int j= parti4-part2;
                    if(j==0){
                        if(strcmp(mbr01->mbr_partition4.part_type,"p")==0){
                        char *l1="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                        fprintf(archivo1, "%s", l1);
                        }else if(strcmp(mbr01->mbr_partition4.part_type,"e")==0){
                            char *l1="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                            fprintf(archivo1, "%s", l1);
                        }
                    }else{
                        char *l1="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                        fprintf(archivo1, "%s", l1);
                        if(strcmp(mbr01->mbr_partition4.part_type,"p")==0){
                        char *l1="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                        fprintf(archivo1, "%s", l1);
                        }else if(strcmp(mbr01->mbr_partition4.part_type,"e")==0){
                            char *l1="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                            fprintf(archivo1, "%s", l1);
                        }

                    }

                    if(part4<mbr01->mbr_tamanio){
                        char *l1="<TD ROWSPAN=\"3\">LIBRE</TD>";
                        fprintf(archivo1, "%s", l1);
                    }
                }
                     else if(part2<mbr01->mbr_tamanio){
                    char *l1="<TD ROWSPAN=\"3\">LIBRE</TD>";
                    fprintf(archivo1, "%s", l1);
                }
            }
            //particion4 menor
            if((parti4<parti2 || size2<1) && mbr01->mbr_partition4.part_size>0){
                int j= parti4-part3;
                if(j==0){
                    if(strcmp(mbr01->mbr_partition4.part_type,"p")==0){
                    char *l1="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                    fprintf(archivo1, "%s", l1);
                    }else if(strcmp(mbr01->mbr_partition4.part_type,"e")==0){
                        char *l1="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                        fprintf(archivo1, "%s", l1);
                    }
                }else{
                    char *l1="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                    fprintf(archivo1, "%s", l1);
                    if(strcmp(mbr01->mbr_partition4.part_type,"p")==0){
                    char *l1="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                    fprintf(archivo1, "%s", l1);
                    }else if(strcmp(mbr01->mbr_partition4.part_type,"e")==0){
                        char *l1="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                        fprintf(archivo1, "%s", l1);
                    }

                }
                if(size2<1 && part4<mbr01->mbr_tamanio){
                    char *l1="<TD ROWSPAN=\"3\">LIBRE</TD>";
                    fprintf(archivo1, "%s", l1);
                }    else  if(mbr01->mbr_partition2.part_size>0){
                    int j= parti2-part4;
                    if(j==0){
                        if(strcmp(mbr01->mbr_partition2.part_type,"p")==0){
                        char *l1="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                        fprintf(archivo1, "%s", l1);
                        }else if(strcmp(mbr01->mbr_partition2.part_type,"e")==0){
                            char *l1="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                            fprintf(archivo1, "%s", l1);
                        }
                    }else{
                        char *l1="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                        fprintf(archivo1, "%s", l1);
                        if(strcmp(mbr01->mbr_partition3.part_type,"p")==0){
                        char *l1="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                        fprintf(archivo1, "%s", l1);
                        }else if(strcmp(mbr01->mbr_partition3.part_type,"e")==0){
                            char *l1="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                            fprintf(archivo1, "%s", l1);
                        }

                    }

                    if(part2<mbr01->mbr_tamanio){
                        char *l1="<TD ROWSPAN=\"3\">LIBRE</TD>";
                        fprintf(archivo1, "%s", l1);
                    }
                }
                    else if( part4<mbr01->mbr_tamanio){
                    char *l1="<TD ROWSPAN=\"3\">LIBRE</TD>";
                    fprintf(archivo1, "%s", l1);

                }
            }

        }
        /**********************************************************************************************************************/


        /***********************************************************************************************************/
        //particion 4 menor
        if((parti4<parti2|| size2<1) &&(parti4<parti3|| size3<1) && mbr01->mbr_partition4.part_size>0){
            int j= part1-parti4;
            if(j==0){
                if(strcmp(mbr01->mbr_partition4.part_type,"p")==0){
                char *l1="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                fprintf(archivo1, "%s", l1);
                }else if(strcmp(mbr01->mbr_partition4.part_type,"e")==0){
                    char *l1="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                    fprintf(archivo1, "%s", l1);
                }
            }else{
                char *l1="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                fprintf(archivo1, "%s", l1);
                if(strcmp(mbr01->mbr_partition4.part_type,"p")==0){
                char *l1="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                fprintf(archivo1, "%s", l1);
                }else if(strcmp(mbr01->mbr_partition4.part_type,"e")==0){
                    char *l1="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                    fprintf(archivo1, "%s", l1);
                }

            }
            if(size3<1 && size2<1 && part4<mbr01->mbr_tamanio){
                char *l1="<TD ROWSPAN=\"3\">LIBRE</TD>";
                fprintf(archivo1, "%s", l1);
            }
            //particion 3 menor
            if((parti3<parti2 || size2<1) && mbr01->mbr_partition3.part_size>0){
                int j= parti3-part4;
                if(j==0){
                    if(strcmp(mbr01->mbr_partition3.part_type,"p")==0){
                    char *l1="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                    fprintf(archivo1, "%s", l1);
                    }else if(strcmp(mbr01->mbr_partition3.part_type,"e")==0){
                        char *l1="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                        fprintf(archivo1, "%s", l1);
                    }
                }else{
                    char *l1="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                    fprintf(archivo1, "%s", l1);
                    if(strcmp(mbr01->mbr_partition3.part_type,"p")==0){
                    char *l1="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                    fprintf(archivo1, "%s", l1);
                    }else if(strcmp(mbr01->mbr_partition3.part_type,"e")==0){
                        char *l1="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                        fprintf(archivo1, "%s", l1);
                    }

                }
                if(size2<1 && part3<mbr01->mbr_tamanio){
                    char *l1="<TD ROWSPAN=\"3\">LIBRE</TD>";
                    fprintf(archivo1, "%s", l1);
                }    else  if(mbr01->mbr_partition2.part_size>0){
                    int j= parti2-part3;
                    if(j==0){
                        if(strcmp(mbr01->mbr_partition2.part_type,"p")==0){
                        char *l1="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                        fprintf(archivo1, "%s", l1);
                        }else if(strcmp(mbr01->mbr_partition2.part_type,"e")==0){
                            char *l1="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                            fprintf(archivo1, "%s", l1);
                        }
                    }else{
                        char *l1="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                        fprintf(archivo1, "%s", l1);
                        if(strcmp(mbr01->mbr_partition2.part_type,"p")==0){
                        char *l1="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                        fprintf(archivo1, "%s", l1);
                        }else if(strcmp(mbr01->mbr_partition2.part_type,"e")==0){
                            char *l1="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                            fprintf(archivo1, "%s", l1);
                        }

                    }

                    if(part2<mbr01->mbr_tamanio){
                        char *l1="<TD ROWSPAN=\"3\">LIBRE</TD>";
                        fprintf(archivo1, "%s", l1);
                    }
                }
                     else if(part3<mbr01->mbr_tamanio){
                    char *l1="<TD ROWSPAN=\"3\">LIBRE</TD>";
                    fprintf(archivo1, "%s", l1);
                }
            }
            //particion2 menor
            if((parti2<parti3 || size3<1) && mbr01->mbr_partition2.part_size>0){
                int j= parti2-part4;
                if(j==0){
                    if(strcmp(mbr01->mbr_partition2.part_type,"p")==0){
                    char *l1="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                    fprintf(archivo1, "%s", l1);
                    }else if(strcmp(mbr01->mbr_partition2.part_type,"e")==0){
                        char *l1="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                        fprintf(archivo1, "%s", l1);
                    }
                }else{
                    char *l1="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                    fprintf(archivo1, "%s", l1);
                    if(strcmp(mbr01->mbr_partition2.part_type,"p")==0){
                    char *l1="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                    fprintf(archivo1, "%s", l1);
                    }else if(strcmp(mbr01->mbr_partition2.part_type,"e")==0){
                        char *l1="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                        fprintf(archivo1, "%s", l1);
                    }

                }
                if(size3<1 && part2<mbr01->mbr_tamanio){
                    char *l1="<TD ROWSPAN=\"3\">LIBRE</TD>";
                    fprintf(archivo1, "%s", l1);
                }    else  if(mbr01->mbr_partition3.part_size>0){
                    int j= parti3-part2;
                    if(j==0){
                        if(strcmp(mbr01->mbr_partition3.part_type,"p")==0){
                        char *l1="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                        fprintf(archivo1, "%s", l1);
                        }else if(strcmp(mbr01->mbr_partition3.part_type,"e")==0){
                            char *l1="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                            fprintf(archivo1, "%s", l1);
                        }
                    }else{
                        char *l1="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                        fprintf(archivo1, "%s", l1);
                        if(strcmp(mbr01->mbr_partition3.part_type,"p")==0){
                        char *l1="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                        fprintf(archivo1, "%s", l1);
                        }else if(strcmp(mbr01->mbr_partition3.part_type,"e")==0){
                            char *l1="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                            fprintf(archivo1, "%s", l1);
                        }

                    }

                    if(part3<mbr01->mbr_tamanio){
                        char *l1="<TD ROWSPAN=\"3\">LIBRE</TD>";
                        fprintf(archivo1, "%s", l1);
                    }
                }
                    else if( part2<mbr01->mbr_tamanio){
                    char *l1="<TD ROWSPAN=\"3\">LIBRE</TD>";
                    fprintf(archivo1, "%s", l1);

                }
            }

        }

        /***********************************************************************************************************/


    }



//oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo

    //para dos menor
    if((parti2<parti1 || size1<1) && (parti2<parti3|| size3<1) && (parti2<parti4|| size4<1) && mbr01->mbr_partition2.part_size>0){
       // printf("\nDENTRO DE IF\n");
        if(parti2==0){
            if(strcmp(mbr01->mbr_partition2.part_type,"p")==0){
            char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
            fprintf(archivo1, "%s", lq);
            }else if(strcmp(mbr01->mbr_partition2.part_type,"e")==0){
                char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                fprintf(archivo1, "%s", lq);
            }
        }else{
            char *lq="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
            fprintf(archivo1, "%s", lq);
            if(strcmp(mbr01->mbr_partition2.part_type,"p")==0){
            char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
            fprintf(archivo1, "%s", lq);
            }else if(strcmp(mbr01->mbr_partition2.part_type,"e")==0){
                char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                fprintf(archivo1, "%s", lq);
            }

        }

        if(size1<1 && size3<1 && size4<1 && part2<mbr01->mbr_tamanio){
            char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
            fprintf(archivo1, "%s", lq);
        }
        //particion 1 menor
        if((parti1<parti4|| size4<1) &&(parti1<parti3|| size3<1) && mbr01->mbr_partition1.part_size>0){
            int j= part2-parti1;
            if(j==0){
                if(strcmp(mbr01->mbr_partition1.part_type,"p")==0){
                char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                fprintf(archivo1, "%s", lq);
                }else if(strcmp(mbr01->mbr_partition1.part_type,"e")==0){
                    char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                    fprintf(archivo1, "%s", lq);
                }
            }else{
                char *lq="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                fprintf(archivo1, "%s", lq);
                if(strcmp(mbr01->mbr_partition1.part_type,"p")==0){
                char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                fprintf(archivo1, "%s", lq);
                }else if(strcmp(mbr01->mbr_partition1.part_type,"e")==0){
                    char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                    fprintf(archivo1, "%s", lq);
                }

            }
            if(size3<1 && size4<1 && part1<mbr01->mbr_tamanio){
                char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                fprintf(archivo1, "%s", lq);
            }
            //particion 3 menor
            if((parti3<parti4 || size4<1) && mbr01->mbr_partition3.part_size>0){
                int j= parti3-part1;
                if(j==0){
                    if(strcmp(mbr01->mbr_partition3.part_type,"p")==0){
                    char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                    fprintf(archivo1, "%s", lq);
                    }else if(strcmp(mbr01->mbr_partition3.part_type,"e")==0){
                        char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                        fprintf(archivo1, "%s", lq);
                    }
                }else{
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                    fprintf(archivo1, "%s", lq);
                    if(strcmp(mbr01->mbr_partition3.part_type,"p")==0){
                    char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                    fprintf(archivo1, "%s", lq);
                    }else if(strcmp(mbr01->mbr_partition3.part_type,"e")==0){
                        char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                        fprintf(archivo1, "%s", lq);
                    }

                }
                if(size4<1 && part3<mbr01->mbr_tamanio){
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                    fprintf(archivo1, "%s", lq);
                }    else  if(mbr01->mbr_partition4.part_size>0){
                    int j= parti4-part3;
                    if(j==0){
                        if(strcmp(mbr01->mbr_partition4.part_type,"p")==0){
                        char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                        fprintf(archivo1, "%s", lq);
                        }else if(strcmp(mbr01->mbr_partition4.part_type,"e")==0){
                            char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                            fprintf(archivo1, "%s", lq);
                        }
                    }else{
                        char *lq="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                        fprintf(archivo1, "%s", lq);
                        if(strcmp(mbr01->mbr_partition4.part_type,"p")==0){
                        char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                        fprintf(archivo1, "%s", lq);
                        }else if(strcmp(mbr01->mbr_partition4.part_type,"e")==0){
                            char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                            fprintf(archivo1, "%s", lq);
                        }

                    }

                    if(part4<mbr01->mbr_tamanio){
                        char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                        fprintf(archivo1, "%s", lq);
                    }
                }
                     else if(part3<mbr01->mbr_tamanio){
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                    fprintf(archivo1, "%s", lq);
                }
            }
            //particion4 menor
            if((parti4<parti3 || size3<1) && mbr01->mbr_partition4.part_size>0){
                int j= parti4-part1;
                if(j==0){
                    if(strcmp(mbr01->mbr_partition4.part_type,"p")==0){
                    char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                    fprintf(archivo1, "%s", lq);
                    }else if(strcmp(mbr01->mbr_partition4.part_type,"e")==0){
                        char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                        fprintf(archivo1, "%s", lq);
                    }
                }else{
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                    fprintf(archivo1, "%s", lq);
                    if(strcmp(mbr01->mbr_partition4.part_type,"p")==0){
                    char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                    fprintf(archivo1, "%s", lq);
                    }else if(strcmp(mbr01->mbr_partition4.part_type,"e")==0){
                        char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                        fprintf(archivo1, "%s", lq);
                    }

                }
                if(size3<1 && part4<mbr01->mbr_tamanio){
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                    fprintf(archivo1, "%s", lq);
                }    else  if(mbr01->mbr_partition3.part_size>0){
                    int j= parti3-part4;
                    if(j==0){
                        if(strcmp(mbr01->mbr_partition3.part_type,"p")==0){
                        char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                        fprintf(archivo1, "%s", lq);
                        }else if(strcmp(mbr01->mbr_partition3.part_type,"e")==0){
                            char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                            fprintf(archivo1, "%s", lq);
                        }
                    }else{
                        char *lq="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                        fprintf(archivo1, "%s", lq);
                        if(strcmp(mbr01->mbr_partition3.part_type,"p")==0){
                        char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                        fprintf(archivo1, "%s", lq);
                        }else if(strcmp(mbr01->mbr_partition3.part_type,"e")==0){
                            char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                            fprintf(archivo1, "%s", lq);
                        }

                    }

                    if(part3<mbr01->mbr_tamanio){
                        char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                        fprintf(archivo1, "%s", lq);
                    }
                }
                    else if( part4<mbr01->mbr_tamanio){
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                    fprintf(archivo1, "%s", lq);

                }
            }

        }


        /**********************************************************************************************************************/
        //particion 3 menor
        if((parti3<parti4|| size4<1) &&(parti3<parti1|| size1<1) && mbr01->mbr_partition3.part_size>0){
            int j= part2-parti3;
            if(j==0){
                if(strcmp(mbr01->mbr_partition3.part_type,"p")==0){
                char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                fprintf(archivo1, "%s", lq);
                }else if(strcmp(mbr01->mbr_partition3.part_type,"e")==0){
                    char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                    fprintf(archivo1, "%s", lq);
                }
            }else{
                char *lq="<TD RO3SPAN=\"3\">LIBRE</TD >\n";
                fprintf(archivo1, "%s", lq);
                if(strcmp(mbr01->mbr_partition3.part_type,"p")==0){
                char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                fprintf(archivo1, "%s", lq);
                }else if(strcmp(mbr01->mbr_partition3.part_type,"e")==0){
                    char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                    fprintf(archivo1, "%s", lq);
                }

            }
            if(size1<1 && size4<1 && part3<mbr01->mbr_tamanio){
                char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                fprintf(archivo1, "%s", lq);
            }
            //particion 1 menor
            if((parti1<parti4 || size4<1) && mbr01->mbr_partition1.part_size>0){
                int j= parti1-part3;
                if(j==0){
                    if(strcmp(mbr01->mbr_partition1.part_type,"p")==0){
                    char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                    fprintf(archivo1, "%s", lq);
                    }else if(strcmp(mbr01->mbr_partition1.part_type,"e")==0){
                        char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                        fprintf(archivo1, "%s", lq);
                    }
                }else{
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                    fprintf(archivo1, "%s", lq);
                    if(strcmp(mbr01->mbr_partition1.part_type,"p")==0){
                    char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                    fprintf(archivo1, "%s", lq);
                    }else if(strcmp(mbr01->mbr_partition1.part_type,"e")==0){
                        char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                        fprintf(archivo1, "%s", lq);
                    }

                }
                if(size4<1 && part1<mbr01->mbr_tamanio){
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                    fprintf(archivo1, "%s", lq);
                }    else  if(mbr01->mbr_partition4.part_size>0){
                    int j= parti4-part1;
                    if(j==0){
                        if(strcmp(mbr01->mbr_partition4.part_type,"p")==0){
                        char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                        fprintf(archivo1, "%s", lq);
                        }else if(strcmp(mbr01->mbr_partition4.part_type,"e")==0){
                            char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                            fprintf(archivo1, "%s", lq);
                        }
                    }else{
                        char *lq="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                        fprintf(archivo1, "%s", lq);
                        if(strcmp(mbr01->mbr_partition4.part_type,"p")==0){
                        char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                        fprintf(archivo1, "%s", lq);
                        }else if(strcmp(mbr01->mbr_partition4.part_type,"e")==0){
                            char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                            fprintf(archivo1, "%s", lq);
                        }

                    }

                    if(part4<mbr01->mbr_tamanio){
                        char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                        fprintf(archivo1, "%s", lq);
                    }
                }
                     else if(part1<mbr01->mbr_tamanio){
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                    fprintf(archivo1, "%s", lq);
                }
            }
            //particion4 menor
            if((parti4<parti1 || size1<1) && mbr01->mbr_partition4.part_size>0){
                int j= parti4-part3;
                if(j==0){
                    if(strcmp(mbr01->mbr_partition4.part_type,"p")==0){
                    char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                    fprintf(archivo1, "%s", lq);
                    }else if(strcmp(mbr01->mbr_partition4.part_type,"e")==0){
                        char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                        fprintf(archivo1, "%s", lq);
                    }
                }else{
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                    fprintf(archivo1, "%s", lq);
                    if(strcmp(mbr01->mbr_partition4.part_type,"p")==0){
                    char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                    fprintf(archivo1, "%s", lq);
                    }else if(strcmp(mbr01->mbr_partition4.part_type,"e")==0){
                        char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                        fprintf(archivo1, "%s", lq);
                    }

                }
                if(size1<1 && part4<mbr01->mbr_tamanio){
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                    fprintf(archivo1, "%s", lq);
                }    else  if(mbr01->mbr_partition1.part_size>0){
                    int j= parti1-part4;
                    if(j==0){
                        if(strcmp(mbr01->mbr_partition1.part_type,"p")==0){
                        char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                        fprintf(archivo1, "%s", lq);
                        }else if(strcmp(mbr01->mbr_partition1.part_type,"e")==0){
                            char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                            fprintf(archivo1, "%s", lq);
                        }
                    }else{
                        char *lq="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                        fprintf(archivo1, "%s", lq);
                        if(strcmp(mbr01->mbr_partition3.part_type,"p")==0){
                        char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                        fprintf(archivo1, "%s", lq);
                        }else if(strcmp(mbr01->mbr_partition3.part_type,"e")==0){
                            char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                            fprintf(archivo1, "%s", lq);
                        }

                    }

                    if(part1<mbr01->mbr_tamanio){
                        char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                        fprintf(archivo1, "%s", lq);
                    }
                }
                    else if( part4<mbr01->mbr_tamanio){
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                    fprintf(archivo1, "%s", lq);

                }
            }

        }
        /**********************************************************************************************************************/


        /***********************************************************************************************************/
        //particion 4 menor
        if((parti4<parti1|| size1<1) &&(parti4<parti3|| size3<1) && mbr01->mbr_partition4.part_size>0){
            int j= part2-parti4;
            if(j==0){
                if(strcmp(mbr01->mbr_partition4.part_type,"p")==0){
                char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                fprintf(archivo1, "%s", lq);
                }else if(strcmp(mbr01->mbr_partition4.part_type,"e")==0){
                    char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                    fprintf(archivo1, "%s", lq);
                }
            }else{
                char *lq="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                fprintf(archivo1, "%s", lq);
                if(strcmp(mbr01->mbr_partition4.part_type,"p")==0){
                char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                fprintf(archivo1, "%s", lq);
                }else if(strcmp(mbr01->mbr_partition4.part_type,"e")==0){
                    char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                    fprintf(archivo1, "%s", lq);
                }

            }
            if(size3<1 && size1<1 && part4<mbr01->mbr_tamanio){
                char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                fprintf(archivo1, "%s", lq);
            }
            //particion 3 menor
            if((parti3<parti1 || size1<1) && mbr01->mbr_partition3.part_size>0){
                int j= parti3-part4;
                if(j==0){
                    if(strcmp(mbr01->mbr_partition3.part_type,"p")==0){
                    char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                    fprintf(archivo1, "%s", lq);
                    }else if(strcmp(mbr01->mbr_partition3.part_type,"e")==0){
                        char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                        fprintf(archivo1, "%s", lq);
                    }
                }else{
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                    fprintf(archivo1, "%s", lq);
                    if(strcmp(mbr01->mbr_partition3.part_type,"p")==0){
                    char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                    fprintf(archivo1, "%s", lq);
                    }else if(strcmp(mbr01->mbr_partition3.part_type,"e")==0){
                        char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                        fprintf(archivo1, "%s", lq);
                    }

                }
                if(size1<1 && part3<mbr01->mbr_tamanio){
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                    fprintf(archivo1, "%s", lq);
                }    else  if(mbr01->mbr_partition1.part_size>0){
                    int j= parti1-part3;
                    if(j==0){
                        if(strcmp(mbr01->mbr_partition1.part_type,"p")==0){
                        char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                        fprintf(archivo1, "%s", lq);
                        }else if(strcmp(mbr01->mbr_partition1.part_type,"e")==0){
                            char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                            fprintf(archivo1, "%s", lq);
                        }
                    }else{
                        char *lq="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                        fprintf(archivo1, "%s", lq);
                        if(strcmp(mbr01->mbr_partition1.part_type,"p")==0){
                        char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                        fprintf(archivo1, "%s", lq);
                        }else if(strcmp(mbr01->mbr_partition1.part_type,"e")==0){
                            char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                            fprintf(archivo1, "%s", lq);
                        }

                    }

                    if(part1<mbr01->mbr_tamanio){
                        char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                        fprintf(archivo1, "%s", lq);
                    }
                }
                     else if(part3<mbr01->mbr_tamanio){
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                    fprintf(archivo1, "%s", lq);
                }
            }
            //particion1 menor
            if((parti1<parti3 || size3<1) && mbr01->mbr_partition1.part_size>0){
                int j= parti1-part4;
                if(j==0){
                    if(strcmp(mbr01->mbr_partition1.part_type,"p")==0){
                    char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                    fprintf(archivo1, "%s", lq);
                    }else if(strcmp(mbr01->mbr_partition1.part_type,"e")==0){
                        char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                        fprintf(archivo1, "%s", lq);
                    }
                }else{
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                    fprintf(archivo1, "%s", lq);
                    if(strcmp(mbr01->mbr_partition1.part_type,"p")==0){
                    char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                    fprintf(archivo1, "%s", lq);
                    }else if(strcmp(mbr01->mbr_partition1.part_type,"e")==0){
                        char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                        fprintf(archivo1, "%s", lq);
                    }

                }
                if(size3<1 && part1<mbr01->mbr_tamanio){
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                    fprintf(archivo1, "%s", lq);
                }    else  if(mbr01->mbr_partition3.part_size>0){
                    int j= parti3-part1;
                    if(j==0){
                        if(strcmp(mbr01->mbr_partition3.part_type,"p")==0){
                        char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                        fprintf(archivo1, "%s", lq);
                        }else if(strcmp(mbr01->mbr_partition3.part_type,"e")==0){
                            char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                            fprintf(archivo1, "%s", lq);
                        }
                    }else{
                        char *lq="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                        fprintf(archivo1, "%s", lq);
                        if(strcmp(mbr01->mbr_partition3.part_type,"p")==0){
                        char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                        fprintf(archivo1, "%s", lq);
                        }else if(strcmp(mbr01->mbr_partition3.part_type,"e")==0){
                            char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                            fprintf(archivo1, "%s", lq);
                        }

                    }

                    if(part3<mbr01->mbr_tamanio){
                        char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                        fprintf(archivo1, "%s", lq);
                    }
                }
                    else if( part1<mbr01->mbr_tamanio){
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                    fprintf(archivo1, "%s", lq);

                }
            }

        }

        /***********************************************************************************************************/


    }

   //para tres menor
    if((parti3<parti1 || size1<1) && (parti3<parti2|| size2<1) && (parti3<parti4|| size4<1) && mbr01->mbr_partition3.part_size>0){
       // printf("\nDENTRO DE IF\n");
        if(parti3==0){
            if(strcmp(mbr01->mbr_partition3.part_type,"p")==0){
            char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
            fprintf(archivo1, "%s", lq);
            }else if(strcmp(mbr01->mbr_partition3.part_type,"e")==0){
                char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                fprintf(archivo1, "%s", lq);
            }
        }else{
            char *lq="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
            fprintf(archivo1, "%s", lq);
            if(strcmp(mbr01->mbr_partition3.part_type,"p")==0){
            char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
            fprintf(archivo1, "%s", lq);
            }else if(strcmp(mbr01->mbr_partition3.part_type,"e")==0){
                char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                fprintf(archivo1, "%s", lq);
            }

        }

        if(size1<1 && size2<1 && size4<1 && part3<mbr01->mbr_tamanio){
            char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
            fprintf(archivo1, "%s", lq);
        }
        //particion 1 menor
        if((parti1<parti4|| size4<1) &&(parti1<parti2|| size2<1) && mbr01->mbr_partition1.part_size>0){
            int j= part3-parti1;
            if(j==0){
                if(strcmp(mbr01->mbr_partition1.part_type,"p")==0){
                char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                fprintf(archivo1, "%s", lq);
                }else{
                    char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                    fprintf(archivo1, "%s", lq);
                }
            }else{
                char *lq="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                fprintf(archivo1, "%s", lq);
                if(strcmp(mbr01->mbr_partition1.part_type,"p")==0){
                char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                fprintf(archivo1, "%s", lq);
                }else if(strcmp(mbr01->mbr_partition1.part_type,"e")==0){
                    char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                    fprintf(archivo1, "%s", lq);
                }

            }
            if(size2<1 && size4<1 && part1<mbr01->mbr_tamanio){
                char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                fprintf(archivo1, "%s", lq);
            }
            //particion 2 menor
            if((parti2<parti4 || size4<1) && mbr01->mbr_partition2.part_size>0){
                int j= parti2-part1;
                if(j==0){
                    if(strcmp(mbr01->mbr_partition2.part_type,"p")==0){
                    char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                    fprintf(archivo1, "%s", lq);
                    }else if(strcmp(mbr01->mbr_partition2.part_type,"e")==0){
                        char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                        fprintf(archivo1, "%s", lq);
                    }
                }else{
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                    fprintf(archivo1, "%s", lq);
                    if(strcmp(mbr01->mbr_partition2.part_type,"p")==0){
                    char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                    fprintf(archivo1, "%s", lq);
                    }else if(strcmp(mbr01->mbr_partition2.part_type,"e")==0){
                        char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                        fprintf(archivo1, "%s", lq);
                    }

                }
                if(size4<1 && part2<mbr01->mbr_tamanio){
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                    fprintf(archivo1, "%s", lq);
                }    else  if(mbr01->mbr_partition4.part_size>0){
                    int j= parti4-part2;
                    if(j==0){
                        if(strcmp(mbr01->mbr_partition4.part_type,"p")==0){
                        char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                        fprintf(archivo1, "%s", lq);
                        }else if(strcmp(mbr01->mbr_partition4.part_type,"e")==0){
                            char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                            fprintf(archivo1, "%s", lq);
                        }
                    }else{
                        char *lq="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                        fprintf(archivo1, "%s", lq);
                        if(strcmp(mbr01->mbr_partition4.part_type,"p")==0){
                        char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                        fprintf(archivo1, "%s", lq);
                        }else if(strcmp(mbr01->mbr_partition4.part_type,"e")==0){
                            char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                            fprintf(archivo1, "%s", lq);
                        }

                    }

                    if(part4<mbr01->mbr_tamanio){
                        char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                        fprintf(archivo1, "%s", lq);
                    }
                }
                     else if(part2<mbr01->mbr_tamanio){
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                    fprintf(archivo1, "%s", lq);
                }
            }
            //particion4 menor
            if((parti4<parti2 || size2<1) && mbr01->mbr_partition4.part_size>0){
                int j= parti4-part1;
                if(j==0){
                    if(strcmp(mbr01->mbr_partition4.part_type,"p")==0){
                    char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                    fprintf(archivo1, "%s", lq);
                    }else if(strcmp(mbr01->mbr_partition4.part_type,"e")==0){
                        char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                        fprintf(archivo1, "%s", lq);
                    }
                }else{
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                    fprintf(archivo1, "%s", lq);
                    if(strcmp(mbr01->mbr_partition4.part_type,"p")==0){
                    char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                    fprintf(archivo1, "%s", lq);
                    }else if(strcmp(mbr01->mbr_partition4.part_type,"e")==0){
                        char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                        fprintf(archivo1, "%s", lq);
                    }

                }
                if(size2<1 && part4<mbr01->mbr_tamanio){
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                    fprintf(archivo1, "%s", lq);
                }    else  if(mbr01->mbr_partition2.part_size>0){
                    int j= parti2-part4;
                    if(j==0){
                        if(strcmp(mbr01->mbr_partition2.part_type,"p")==0){
                        char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                        fprintf(archivo1, "%s", lq);
                        }else if(strcmp(mbr01->mbr_partition2.part_type,"e")==0){
                            char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                            fprintf(archivo1, "%s", lq);
                        }
                    }else{
                        char *lq="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                        fprintf(archivo1, "%s", lq);
                        if(strcmp(mbr01->mbr_partition2.part_type,"p")==0){
                        char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                        fprintf(archivo1, "%s", lq);
                        }else if(strcmp(mbr01->mbr_partition2.part_type,"e")==0){
                            char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                            fprintf(archivo1, "%s", lq);
                        }

                    }

                    if(part2<mbr01->mbr_tamanio){
                        char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                        fprintf(archivo1, "%s", lq);
                    }
                }
                    else if( part4<mbr01->mbr_tamanio){
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                    fprintf(archivo1, "%s", lq);

                }
            }

        }


        /**********************************************************************************************************************/
        //particion 2 menor
        if((parti2<parti4|| size4<1) &&(parti2<parti1|| size1<1) && mbr01->mbr_partition2.part_size>0){
            int j= part3-parti2;
            if(j==0){
                if(strcmp(mbr01->mbr_partition2.part_type,"p")==0){
                char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                fprintf(archivo1, "%s", lq);
                }else if(strcmp(mbr01->mbr_partition2.part_type,"e")==0){
                    char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                    fprintf(archivo1, "%s", lq);
                }
            }else{
                char *lq="<TD RO2SPAN=\"3\">LIBRE</TD >\n";
                fprintf(archivo1, "%s", lq);
                if(strcmp(mbr01->mbr_partition2.part_type,"p")==0){
                char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                fprintf(archivo1, "%s", lq);
                }else if(strcmp(mbr01->mbr_partition2.part_type,"e")==0){
                    char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                    fprintf(archivo1, "%s", lq);
                }

            }
            if(size1<1 && size4<1 && part2<mbr01->mbr_tamanio){
                char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                fprintf(archivo1, "%s", lq);
            }
            //particion 1 menor
            if((parti1<parti4 || size4<1) && mbr01->mbr_partition1.part_size>0){
                int j= parti1-part2;
                if(j==0){
                    if(strcmp(mbr01->mbr_partition1.part_type,"p")==0){
                    char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                    fprintf(archivo1, "%s", lq);
                    }else if(strcmp(mbr01->mbr_partition1.part_type,"e")==0){
                        char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                        fprintf(archivo1, "%s", lq);
                    }
                }else{
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                    fprintf(archivo1, "%s", lq);
                    if(strcmp(mbr01->mbr_partition1.part_type,"p")==0){
                    char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                    fprintf(archivo1, "%s", lq);
                    }else if(strcmp(mbr01->mbr_partition1.part_type,"e")==0){
                        char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                        fprintf(archivo1, "%s", lq);
                    }

                }
                if(size4<1 && part1<mbr01->mbr_tamanio){
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                    fprintf(archivo1, "%s", lq);
                }    else  if(mbr01->mbr_partition4.part_size>0){
                    int j= parti4-part1;
                    if(j==0){
                        if(strcmp(mbr01->mbr_partition4.part_type,"p")==0){
                        char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                        fprintf(archivo1, "%s", lq);
                        }else if(strcmp(mbr01->mbr_partition4.part_type,"e")==0){
                            char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                            fprintf(archivo1, "%s", lq);
                        }
                    }else{
                        char *lq="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                        fprintf(archivo1, "%s", lq);
                        if(strcmp(mbr01->mbr_partition4.part_type,"p")==0){
                        char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                        fprintf(archivo1, "%s", lq);
                        }else if(strcmp(mbr01->mbr_partition4.part_type,"e")==0){
                            char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                            fprintf(archivo1, "%s", lq);
                        }

                    }

                    if(part4<mbr01->mbr_tamanio){
                        char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                        fprintf(archivo1, "%s", lq);
                    }
                }
                     else if(part1<mbr01->mbr_tamanio){
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                    fprintf(archivo1, "%s", lq);
                }
            }
            //particion4 menor
            if((parti4<parti1 || size1<1) && mbr01->mbr_partition4.part_size>0){
                int j= parti4-part2;
                if(j==0){
                    if(strcmp(mbr01->mbr_partition4.part_type,"p")==0){
                    char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                    fprintf(archivo1, "%s", lq);
                    }else if(strcmp(mbr01->mbr_partition4.part_type,"e")==0){
                        char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                        fprintf(archivo1, "%s", lq);
                    }
                }else{
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                    fprintf(archivo1, "%s", lq);
                    if(strcmp(mbr01->mbr_partition4.part_type,"p")==0){
                    char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                    fprintf(archivo1, "%s", lq);
                    }else if(strcmp(mbr01->mbr_partition4.part_type,"e")==0){
                        char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                        fprintf(archivo1, "%s", lq);
                    }

                }
                if(size1<1 && part4<mbr01->mbr_tamanio){
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                    fprintf(archivo1, "%s", lq);
                }    else  if(mbr01->mbr_partition1.part_size>0){
                    int j= parti1-part4;
                    if(j==0){
                        if(strcmp(mbr01->mbr_partition1.part_type,"p")==0){
                        char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                        fprintf(archivo1, "%s", lq);
                        }else if(strcmp(mbr01->mbr_partition1.part_type,"e")==0){
                            char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                            fprintf(archivo1, "%s", lq);
                        }
                    }else{
                        char *lq="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                        fprintf(archivo1, "%s", lq);
                        if(strcmp(mbr01->mbr_partition2.part_type,"p")==0){
                        char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                        fprintf(archivo1, "%s", lq);
                        }else if(strcmp(mbr01->mbr_partition2.part_type,"e")==0){
                            char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                            fprintf(archivo1, "%s", lq);
                        }

                    }

                    if(part1<mbr01->mbr_tamanio){
                        char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                        fprintf(archivo1, "%s", lq);
                    }
                }
                    else if( part4<mbr01->mbr_tamanio){
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                    fprintf(archivo1, "%s", lq);

                }
            }

        }
        /**********************************************************************************************************************/


        /***********************************************************************************************************/
        //particion 4 menor
        if((parti4<parti1|| size1<1) &&(parti4<parti2|| size2<1) && mbr01->mbr_partition4.part_size>0){
            int j= part3-parti4;
            if(j==0){
                if(strcmp(mbr01->mbr_partition4.part_type,"p")==0){
                char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                fprintf(archivo1, "%s", lq);
                }else if(strcmp(mbr01->mbr_partition4.part_type,"e")==0){
                    char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                    fprintf(archivo1, "%s", lq);
                }
            }else{
                char *lq="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                fprintf(archivo1, "%s", lq);
                if(strcmp(mbr01->mbr_partition4.part_type,"p")==0){
                char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                fprintf(archivo1, "%s", lq);
                }else if(strcmp(mbr01->mbr_partition4.part_type,"e")==0){
                    char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                    fprintf(archivo1, "%s", lq);
                }

            }
            if(size2<1 && size1<1 && part4<mbr01->mbr_tamanio){
                char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                fprintf(archivo1, "%s", lq);
            }
            //particion 2 menor
            if((parti2<parti1 || size1<1) && mbr01->mbr_partition2.part_size>0){
                int j= parti2-part4;
                if(j==0){
                    if(strcmp(mbr01->mbr_partition2.part_type,"p")==0){
                    char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                    fprintf(archivo1, "%s", lq);
                    }else if(strcmp(mbr01->mbr_partition2.part_type,"e")==0){
                        char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                        fprintf(archivo1, "%s", lq);
                    }
                }else{
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                    fprintf(archivo1, "%s", lq);
                    if(strcmp(mbr01->mbr_partition2.part_type,"p")==0){
                    char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                    fprintf(archivo1, "%s", lq);
                    }else if(strcmp(mbr01->mbr_partition2.part_type,"e")==0){
                        char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                        fprintf(archivo1, "%s", lq);
                    }

                }
                if(size1<1 && part2<mbr01->mbr_tamanio){
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                    fprintf(archivo1, "%s", lq);
                }    else  if(mbr01->mbr_partition1.part_size>0){
                    int j= parti1-part2;
                    if(j==0){
                        if(strcmp(mbr01->mbr_partition1.part_type,"p")==0){
                        char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                        fprintf(archivo1, "%s", lq);
                        }else if(strcmp(mbr01->mbr_partition1.part_type,"e")==0){
                            char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                            fprintf(archivo1, "%s", lq);
                        }
                    }else{
                        char *lq="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                        fprintf(archivo1, "%s", lq);
                        if(strcmp(mbr01->mbr_partition1.part_type,"p")==0){
                        char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                        fprintf(archivo1, "%s", lq);
                        }else if(strcmp(mbr01->mbr_partition1.part_type,"e")==0){
                            char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                            fprintf(archivo1, "%s", lq);
                        }

                    }

                    if(part1<mbr01->mbr_tamanio){
                        char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                        fprintf(archivo1, "%s", lq);
                    }
                }
                     else if(part2<mbr01->mbr_tamanio){
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                    fprintf(archivo1, "%s", lq);
                }
            }
            //particion1 menor
            if((parti1<parti2 || size2<1) && mbr01->mbr_partition1.part_size>0){
                int j= parti1-part4;
                if(j==0){
                    if(strcmp(mbr01->mbr_partition1.part_type,"p")==0){
                    char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                    fprintf(archivo1, "%s", lq);
                    }else if(strcmp(mbr01->mbr_partition1.part_type,"e")==0){
                        char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                        fprintf(archivo1, "%s", lq);
                    }
                }else{
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                    fprintf(archivo1, "%s", lq);
                    if(strcmp(mbr01->mbr_partition1.part_type,"p")==0){
                    char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                    fprintf(archivo1, "%s", lq);
                    }else if(strcmp(mbr01->mbr_partition1.part_type,"e")==0){
                        char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                        fprintf(archivo1, "%s", lq);
                    }

                }
                if(size2<1 && part1<mbr01->mbr_tamanio){
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                    fprintf(archivo1, "%s", lq);
                }    else  if(mbr01->mbr_partition2.part_size>0){
                    int j= parti2-part1;
                    if(j==0){
                        if(strcmp(mbr01->mbr_partition2.part_type,"p")==0){
                        char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                        fprintf(archivo1, "%s", lq);
                        }else if(strcmp(mbr01->mbr_partition2.part_type,"e")==0){
                            char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                            fprintf(archivo1, "%s", lq);
                        }
                    }else{
                        char *lq="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                        fprintf(archivo1, "%s", lq);
                        if(strcmp(mbr01->mbr_partition2.part_type,"p")==0){
                        char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                        fprintf(archivo1, "%s", lq);
                        }else if(strcmp(mbr01->mbr_partition2.part_type,"e")==0){
                            char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                            fprintf(archivo1, "%s", lq);
                        }

                    }

                    if(part2<mbr01->mbr_tamanio){
                        char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                        fprintf(archivo1, "%s", lq);
                    }
                }
                    else if( part1<mbr01->mbr_tamanio){
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                    fprintf(archivo1, "%s", lq);

                }
            }

        }

        /***********************************************************************************************************/


    }

  //para cuatro menor
    if((parti4<parti1 || size1<1) && (parti4<parti3|| size3<1) && (parti4<parti2|| size2<1) && mbr01->mbr_partition4.part_size>0){
        //printf("\nDENTRO DE IF\n");
        if(parti4==0){
            if(strcmp(mbr01->mbr_partition4.part_type,"p")==0){
            char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
            fprintf(archivo1, "%s", lq);
            }else if(strcmp(mbr01->mbr_partition4.part_type,"e")==0){
                char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                fprintf(archivo1, "%s", lq);
            }
        }else{
            char *lq="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
            fprintf(archivo1, "%s", lq);
            if(strcmp(mbr01->mbr_partition4.part_type,"p")==0){
            char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
            fprintf(archivo1, "%s", lq);
            }else if(strcmp(mbr01->mbr_partition4.part_type,"e")==0){
                char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                fprintf(archivo1, "%s", lq);
            }

        }

        if(size1<1 && size3<1 && size2<1 && part4<mbr01->mbr_tamanio){
            char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
            fprintf(archivo1, "%s", lq);
        }
        //particion 1 menor
        if((parti1<parti2|| size2<1) &&(parti1<parti3|| size3<1) && mbr01->mbr_partition1.part_size>0){
            int j= part4-parti1;
            if(j==0){
                if(strcmp(mbr01->mbr_partition1.part_type,"p")==0){
                char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                fprintf(archivo1, "%s", lq);
                }else if(strcmp(mbr01->mbr_partition1.part_type,"e")==0){
                    char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                    fprintf(archivo1, "%s", lq);
                }
            }else{
                char *lq="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                fprintf(archivo1, "%s", lq);
                if(strcmp(mbr01->mbr_partition1.part_type,"p")==0){
                char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                fprintf(archivo1, "%s", lq);
                }else if(strcmp(mbr01->mbr_partition1.part_type,"e")==0){
                    char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                    fprintf(archivo1, "%s", lq);
                }

            }
            if(size3<1 && size2<1 && part1<mbr01->mbr_tamanio){
                char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                fprintf(archivo1, "%s", lq);
            }
            //particion 3 menor
            if((parti3<parti2 || size2<1) && mbr01->mbr_partition3.part_size>0){
                int j= parti3-part1;
                if(j==0){
                    if(strcmp(mbr01->mbr_partition3.part_type,"p")==0){
                    char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                    fprintf(archivo1, "%s", lq);
                    }else if(strcmp(mbr01->mbr_partition3.part_type,"e")==0){
                        char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                        fprintf(archivo1, "%s", lq);
                    }
                }else{
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                    fprintf(archivo1, "%s", lq);
                    if(strcmp(mbr01->mbr_partition3.part_type,"p")==0){
                    char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                    fprintf(archivo1, "%s", lq);
                    }else if(strcmp(mbr01->mbr_partition3.part_type,"e")==0){
                        char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                        fprintf(archivo1, "%s", lq);
                    }

                }
                if(size2<1 && part3<mbr01->mbr_tamanio){
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                    fprintf(archivo1, "%s", lq);
                }    else  if(mbr01->mbr_partition2.part_size>0){
                    int j= parti2-part3;
                    if(j==0){
                        if(strcmp(mbr01->mbr_partition2.part_type,"p")==0){
                        char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                        fprintf(archivo1, "%s", lq);
                        }else if(strcmp(mbr01->mbr_partition2.part_type,"e")==0){
                            char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                            fprintf(archivo1, "%s", lq);
                        }
                    }else{
                        char *lq="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                        fprintf(archivo1, "%s", lq);
                        if(strcmp(mbr01->mbr_partition2.part_type,"p")==0){
                        char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                        fprintf(archivo1, "%s", lq);
                        }else if(strcmp(mbr01->mbr_partition2.part_type,"e")==0){
                            char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                            fprintf(archivo1, "%s", lq);
                        }

                    }

                    if(part2<mbr01->mbr_tamanio){
                        char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                        fprintf(archivo1, "%s", lq);
                    }
                }
                     else if(part3<mbr01->mbr_tamanio){
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                    fprintf(archivo1, "%s", lq);
                }
            }
            //particion2 menor
            if((parti2<parti3 || size3<1) && mbr01->mbr_partition2.part_size>0){
                int j= parti2-part1;
                if(j==0){
                    if(strcmp(mbr01->mbr_partition2.part_type,"p")==0){
                    char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                    fprintf(archivo1, "%s", lq);
                    }else if(strcmp(mbr01->mbr_partition2.part_type,"e")==0){
                        char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                        fprintf(archivo1, "%s", lq);
                    }
                }else{
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                    fprintf(archivo1, "%s", lq);
                    if(strcmp(mbr01->mbr_partition2.part_type,"p")==0){
                    char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                    fprintf(archivo1, "%s", lq);
                    }else if(strcmp(mbr01->mbr_partition2.part_type,"e")==0){
                        char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                        fprintf(archivo1, "%s", lq);
                    }

                }
                if(size3<1 && part2<mbr01->mbr_tamanio){
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                    fprintf(archivo1, "%s", lq);
                }    else  if(mbr01->mbr_partition3.part_size>0){
                    int j= parti3-part2;
                    if(j==0){
                        if(strcmp(mbr01->mbr_partition3.part_type,"p")==0){
                        char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                        fprintf(archivo1, "%s", lq);
                        }else if(strcmp(mbr01->mbr_partition3.part_type,"e")==0){
                            char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                            fprintf(archivo1, "%s", lq);
                        }
                    }else{
                        char *lq="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                        fprintf(archivo1, "%s", lq);
                        if(strcmp(mbr01->mbr_partition3.part_type,"p")==0){
                        char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                        fprintf(archivo1, "%s", lq);
                        }else if(strcmp(mbr01->mbr_partition3.part_type,"e")==0){
                            char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                            fprintf(archivo1, "%s", lq);
                        }

                    }

                    if(part3<mbr01->mbr_tamanio){
                        char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                        fprintf(archivo1, "%s", lq);
                    }
                }
                    else if( part2<mbr01->mbr_tamanio){
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                    fprintf(archivo1, "%s", lq);

                }
            }

        }


        /**********************************************************************************************************************/
        //particion 3 menor
        if((parti3<parti2|| size2<1) &&(parti3<parti1|| size1<1) && mbr01->mbr_partition3.part_size>0){
            int j= part4-parti3;
            if(j==0){
                if(strcmp(mbr01->mbr_partition3.part_type,"p")==0){
                char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                fprintf(archivo1, "%s", lq);
                }else if(strcmp(mbr01->mbr_partition3.part_type,"e")==0){
                    char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                    fprintf(archivo1, "%s", lq);
                }
            }else{
                char *lq="<TD RO3SPAN=\"3\">LIBRE</TD >\n";
                fprintf(archivo1, "%s", lq);
                if(strcmp(mbr01->mbr_partition3.part_type,"p")==0){
                char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                fprintf(archivo1, "%s", lq);
                }else if(strcmp(mbr01->mbr_partition3.part_type,"e")==0){
                    char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                    fprintf(archivo1, "%s", lq);
                }

            }
            if(size1<1 && size2<1 && part3<mbr01->mbr_tamanio){
                char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                fprintf(archivo1, "%s", lq);
            }
            //particion 1 menor
            if((parti1<parti2 || size2<1) && mbr01->mbr_partition1.part_size>0){
                int j= parti1-part3;
                if(j==0){
                    if(strcmp(mbr01->mbr_partition1.part_type,"p")==0){
                    char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                    fprintf(archivo1, "%s", lq);
                    }else if(strcmp(mbr01->mbr_partition1.part_type,"e")==0){
                        char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                        fprintf(archivo1, "%s", lq);
                    }
                }else{
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                    fprintf(archivo1, "%s", lq);
                    if(strcmp(mbr01->mbr_partition1.part_type,"p")==0){
                    char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                    fprintf(archivo1, "%s", lq);
                    }else if(strcmp(mbr01->mbr_partition1.part_type,"e")==0){
                        char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                        fprintf(archivo1, "%s", lq);
                    }

                }
                if(size2<1 && part1<mbr01->mbr_tamanio){
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                    fprintf(archivo1, "%s", lq);
                }    else  if(mbr01->mbr_partition2.part_size>0){
                    int j= parti2-part1;
                    if(j==0){
                        if(strcmp(mbr01->mbr_partition2.part_type,"p")==0){
                        char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                        fprintf(archivo1, "%s", lq);
                        }else if(strcmp(mbr01->mbr_partition2.part_type,"e")==0){
                            char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                            fprintf(archivo1, "%s", lq);
                        }
                    }else{
                        char *lq="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                        fprintf(archivo1, "%s", lq);
                        if(strcmp(mbr01->mbr_partition2.part_type,"p")==0){
                        char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                        fprintf(archivo1, "%s", lq);
                        }else if(strcmp(mbr01->mbr_partition2.part_type,"e")==0){
                            char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                            fprintf(archivo1, "%s", lq);
                        }

                    }

                    if(part2<mbr01->mbr_tamanio){
                        char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                        fprintf(archivo1, "%s", lq);
                    }
                }
                     else if(part1<mbr01->mbr_tamanio){
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                    fprintf(archivo1, "%s", lq);
                }
            }
            //particion2 menor
            if((parti2<parti1 || size1<1) && mbr01->mbr_partition2.part_size>0){
                int j= parti2-part3;
                if(j==0){
                    if(strcmp(mbr01->mbr_partition2.part_type,"p")==0){
                    char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                    fprintf(archivo1, "%s", lq);
                    }else if(strcmp(mbr01->mbr_partition2.part_type,"e")==0){
                        char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                        fprintf(archivo1, "%s", lq);
                    }
                }else{
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                    fprintf(archivo1, "%s", lq);
                    if(strcmp(mbr01->mbr_partition2.part_type,"p")==0){
                    char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                    fprintf(archivo1, "%s", lq);
                    }else if(strcmp(mbr01->mbr_partition2.part_type,"e")==0){
                        char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                        fprintf(archivo1, "%s", lq);
                    }

                }
                if(size1<1 && part2<mbr01->mbr_tamanio){
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                    fprintf(archivo1, "%s", lq);
                }    else  if(mbr01->mbr_partition1.part_size>0){
                    int j= parti1-part2;
                    if(j==0){
                        if(strcmp(mbr01->mbr_partition1.part_type,"p")==0){
                        char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                        fprintf(archivo1, "%s", lq);
                        }else if(strcmp(mbr01->mbr_partition1.part_type,"e")==0){
                            char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                            fprintf(archivo1, "%s", lq);
                        }
                    }else{
                        char *lq="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                        fprintf(archivo1, "%s", lq);
                        if(strcmp(mbr01->mbr_partition3.part_type,"p")==0){
                        char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                        fprintf(archivo1, "%s", lq);
                        }else if(strcmp(mbr01->mbr_partition3.part_type,"e")==0){
                            char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                            fprintf(archivo1, "%s", lq);
                        }

                    }

                    if(part1<mbr01->mbr_tamanio){
                        char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                        fprintf(archivo1, "%s", lq);
                    }
                }
                    else if( part2<mbr01->mbr_tamanio){
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                    fprintf(archivo1, "%s", lq);

                }
            }

        }
        /**********************************************************************************************************************/


        /***********************************************************************************************************/
        //particion 2 menor
        if((parti2<parti1|| size1<1) &&(parti2<parti3|| size3<1) && mbr01->mbr_partition2.part_size>0){
            int j= part4-parti2;
            if(j==0){
                if(strcmp(mbr01->mbr_partition2.part_type,"p")==0){
                char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                fprintf(archivo1, "%s", lq);
                }else if(strcmp(mbr01->mbr_partition2.part_type,"e")==0){
                    char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                    fprintf(archivo1, "%s", lq);
                }
            }else{
                char *lq="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                fprintf(archivo1, "%s", lq);
                if(strcmp(mbr01->mbr_partition2.part_type,"p")==0){
                char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                fprintf(archivo1, "%s", lq);
                }else if(strcmp(mbr01->mbr_partition2.part_type,"e")==0){
                    char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                    fprintf(archivo1, "%s", lq);
                }

            }
            if(size3<1 && size1<1 && part2<mbr01->mbr_tamanio){
                char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                fprintf(archivo1, "%s", lq);
            }
            //particion 3 menor
            if((parti3<parti1 || size1<1) && mbr01->mbr_partition3.part_size>0){
                int j= parti3-part2;
                if(j==0){
                    if(strcmp(mbr01->mbr_partition3.part_type,"p")==0){
                    char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                    fprintf(archivo1, "%s", lq);
                    }else if(strcmp(mbr01->mbr_partition3.part_type,"e")==0){
                        char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                        fprintf(archivo1, "%s", lq);
                    }
                }else{
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                    fprintf(archivo1, "%s", lq);
                    if(strcmp(mbr01->mbr_partition3.part_type,"p")==0){
                    char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                    fprintf(archivo1, "%s", lq);
                    }else if(strcmp(mbr01->mbr_partition3.part_type,"e")==0){
                        char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                        fprintf(archivo1, "%s", lq);
                    }

                }
                if(size1<1 && part3<mbr01->mbr_tamanio){
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                    fprintf(archivo1, "%s", lq);
                }    else  if(mbr01->mbr_partition1.part_size>0){
                    int j= parti1-part3;
                    if(j==0){
                        if(strcmp(mbr01->mbr_partition1.part_type,"p")==0){
                        char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                        fprintf(archivo1, "%s", lq);
                        }else if(strcmp(mbr01->mbr_partition1.part_type,"e")==0){
                            char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                            fprintf(archivo1, "%s", lq);
                        }
                    }else{
                        char *lq="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                        fprintf(archivo1, "%s", lq);
                        if(strcmp(mbr01->mbr_partition1.part_type,"p")==0){
                        char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                        fprintf(archivo1, "%s", lq);
                        }else if(strcmp(mbr01->mbr_partition1.part_type,"e")==0){
                            char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                            fprintf(archivo1, "%s", lq);
                        }

                    }

                    if(part1<mbr01->mbr_tamanio){
                        char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                        fprintf(archivo1, "%s", lq);
                    }
                }
                     else if(part3<mbr01->mbr_tamanio){
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                    fprintf(archivo1, "%s", lq);
                }
            }
            //particion1 menor
            if((parti1<parti3 || size3<1) && mbr01->mbr_partition1.part_size>0){
                int j= parti1-part2;
                if(j==0){
                    if(strcmp(mbr01->mbr_partition1.part_type,"p")==0){
                    char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                    fprintf(archivo1, "%s", lq);
                    }else if(strcmp(mbr01->mbr_partition1.part_type,"e")==0){
                        char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                        fprintf(archivo1, "%s", lq);
                    }
                }else{
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                    fprintf(archivo1, "%s", lq);
                    if(strcmp(mbr01->mbr_partition1.part_type,"p")==0){
                    char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                    fprintf(archivo1, "%s", lq);
                    }else if(strcmp(mbr01->mbr_partition1.part_type,"e")==0){
                        char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                        fprintf(archivo1, "%s", lq);
                    }

                }
                if(size3<1 && part1<mbr01->mbr_tamanio){
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                    fprintf(archivo1, "%s", lq);
                }    else  if(mbr01->mbr_partition3.part_size>0){
                    int j= parti3-part1;
                    if(j==0){
                        if(strcmp(mbr01->mbr_partition3.part_type,"p")==0){
                        char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                        fprintf(archivo1, "%s", lq);
                        }else if(strcmp(mbr01->mbr_partition3.part_type,"e")==0){
                            char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                            fprintf(archivo1, "%s", lq);
                        }
                    }else{
                        char *lq="<TD ROWSPAN=\"3\">LIBRE</TD >\n";
                        fprintf(archivo1, "%s", lq);
                        if(strcmp(mbr01->mbr_partition3.part_type,"p")==0){
                        char *lq="<TD ROWSPAN=\"3\">PRIMARIA</TD>";
                        fprintf(archivo1, "%s", lq);
                        }else if(strcmp(mbr01->mbr_partition3.part_type,"e")==0){
                            char *lq="<TD COLSPAN=\"50\">EXTENDIDA</TD>";
                            fprintf(archivo1, "%s", lq);
                        }

                    }

                    if(part3<mbr01->mbr_tamanio){
                        char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                        fprintf(archivo1, "%s", lq);
                    }
                }
                    else if( part1<mbr01->mbr_tamanio){
                    char *lq="<TD ROWSPAN=\"3\">LIBRE</TD>";
                    fprintf(archivo1, "%s", lq);

                }
            }

        }

        /***********************************************************************************************************/


    }



        if(listalogicas->tam>0){
            char *cq9 = "</TR><TR>";
            fprintf(archivo1, "%s", cq9);
            int aa =listalogicas->tam;

            while(aa>=1){

                char *cq9 = "<TD>EBR</TD><TD>Logica</TD>";
                fprintf(archivo1, "%s", cq9);
                aa--;
            }




        }

    char *cq9 = "</TR></TABLE>>];\n";
    fprintf(archivo1, "%s", cq9);
    fclose(archivo1);




}
void vaciarlistadereportes(){
    reportedisk1->primero=NULL;
    reportedisk1->ultimo=NULL;
    reportedisk1->tam=0;
}
void vaciarlisebr(){
    listalogicas->primero=NULL;
    listalogicas->ultimo=NULL;
    listalogicas->tam=0;
}
void lisebr(char*fit[10],char*name[30],int next,int size, int start, int status){
    nodolistaebr*nuevo =(nodolistaebr*)malloc(sizeof(nodolistaebr));
    nuevo->anterior=NULL;
    nuevo->siguiente=NULL;
    strcpy(nuevo->part_fit,fit);
    strcpy(nuevo->part_name,name);
    nuevo->part_next=next;
    nuevo->part_size=size;
    nuevo->part_start=start;
    nuevo->part_status=status;
    if(listalogicas->tam==0){
        listalogicas->primero=listalogicas->ultimo=nuevo;
        listalogicas->tam++;
    }else{
        nuevo->anterior=listalogicas->ultimo;
        listalogicas->ultimo->siguiente=nuevo;
        listalogicas->ultimo=nuevo;
        listalogicas->tam++;
    }


}
//FIN METODOS FASE 1

//***************************************************************************************************************************************************************************************************************

//INICIO METODOS FASE 2

int mkfs (char *token){




    //comprobando validez del comando
    if(!strstr(comprobarcadena,"-id")){ printf("\nNo cuenta con parametros minimos\n");return 0;}
    if(strstr(comprobarcadena,"unit") && !strstr(comprobarcadena,"add") ){ printf("\nFalta complemento de unit (falta add)\n");return 0;}
    if((strstr(comprobarcadena,"type") || strstr(comprobarcadena,"fs") ) && strstr(comprobarcadena,"add") ){ printf("\nParametros incompatibles\n");return 0;}

    //parametros a evaluar
    char id1[10]="-id";
    char type1[10]="+type";
    char add1[20]="+add";
    char unit1[5]="+unit";
    char fs1[5]="+fs";

    //Parametros a resivir
    char id[10];
    char type[10];
    char add[20];
    int  add2=0;
    char unit[5];
    char fs[5];

    //colocando valores opcionales
    strcpy(unit,"k");
    strcpy(fs,"3fs");

    //Banderas

    int modifica_espacio=0;
    int espacio_negativo=0;

    //divido la cadena para sacar los parametros verdaderos
    while(token!=NULL){

        token=strtok(NULL," :");
        if(token==NULL){ break;}//si el token es nulo se sale del while
        int caso=0;
        if(strstr(token,id1))        {caso=1;}
        else if(strstr(token,type1)) {caso=2;}
        else if(strstr(token,add1))  {caso=3;}
        else if(strstr(token,unit1)) {caso=4;}
        else if(strstr(token,fs1))   {caso=5;}

        switch(caso){
            case 1:
                token = strtok(NULL," :");
                if(token==NULL){printf("\nerror en el comando\n"); return 0;}
                strcpy(id,token);
                //comprobando que exista el id
                registro* aux =(registro *)malloc(sizeof(registro));
                aux=re->primero;
                int n=re->tam;
                int encontrado=0;
                while(n>=1){

                    if(strcmp(id,aux->id)==0){
                        encontrado=1;
                        break;
                    }
                    aux=aux->siguiente;
                    n--;
                }

                if(encontrado==0){printf("\nLa particion no esta montada\n"); return 0;}


                break;

            case 2:
                token = strtok(NULL," :");
                if(token==NULL){printf("\nerror en el comando\n"); return 0;}
                strcpy(type,token);
                break;

            case 3:
                token = strtok(NULL," :");
                if(token==NULL){printf("\nerror en el comando\n"); return 0;}
                strcpy(add,token);

                //colocando banderas
                modifica_espacio=1;
                if(strstr(add,"-")){espacio_negativo=1;}

                //asignando el valor al entero add
                char * token_auxiliar;
                if(strstr(add,"-")){
                token_auxiliar=strtok(add,"-");
                add2=atoi(token_auxiliar)*(-1);
                }else{
                    add2=atoi(add);
                }

                break;

            case 4:
                token = strtok(NULL," :");
                if(token==NULL){printf("\nerror en el comando\n"); return 0;}
                strcpy(unit,token);
                break;

            case 5:
                token = strtok(NULL," :");
                if(token==NULL){printf("\nerror en el comando\n"); return 0;}
                strcpy(fs,token);
                break;


        }



    }

    if(modifica_espacio==0){

        //recuperamos el path para abrir el disco y poder traer la particion.
        registro* aux =(registro *)malloc(sizeof(registro));
        aux=re->primero;
        int n=re->tam;
        int encontrado=0;
        char pathp[200];

        while(n>=1){
            if(strcmp(id,aux->id)==0){
                strcpy(pathp,aux->path);
                break;
            }

            aux=aux->siguiente;
            n--;
        }

        //abrimos el archivo.
        FILE *disco=fopen(pathp,"r+");
        if(!disco){printf("\nel disco fue eliminado .\n"); return 0;}//si el archivo no existe

        mbr *mbraux =(mbr*)malloc(sizeof(mbr));//utilizamos este mbr para recuperar las particiones y formatearlas

        fread(mbraux,sizeof(mbr),1,disco);//leemos el mbr



        int no_particion=0;//numero de particion a formatear
        int inicio_sistema_ext=-1;//este valor guarda el inicio en que empezaremos a escribir el sistema de arrchivos
        int tam_particion_sistema=-1;//esta variable contiene el valor del tama;o de la particion que formatearemos

        if(strcmp(mbraux->mbr_partition1.part_name,aux->name)==0)     {no_particion=1; inicio_sistema_ext=mbraux->mbr_partition1.part_start; tam_particion_sistema=mbraux->mbr_partition1.part_size;}
        else if(strcmp(mbraux->mbr_partition2.part_name,aux->name)==0){no_particion=2; inicio_sistema_ext=mbraux->mbr_partition2.part_start; tam_particion_sistema=mbraux->mbr_partition2.part_size;}
        else if(strcmp(mbraux->mbr_partition3.part_name,aux->name)==0){no_particion=3; inicio_sistema_ext=mbraux->mbr_partition3.part_start; tam_particion_sistema=mbraux->mbr_partition3.part_size;}
        else if(strcmp(mbraux->mbr_partition4.part_name,aux->name)==0){no_particion=4; inicio_sistema_ext=mbraux->mbr_partition4.part_start; tam_particion_sistema=mbraux->mbr_partition4.part_size;}

        //si no se encontro la prticion evaluamos si alguna particion es extendida.. si es asi evaluamos sus logicas
        if(no_particion==0){

            int no_part_extendida=0;//si no se encuentra la particion tomaremos la extendida para ver si el id pertenece a una logica
            partition particion; //en esta variable guardo los datos de la particion extendida si en dado caso existiera
            if(strcmp(mbraux->mbr_partition1.part_type,"e")){no_part_extendida=1;  particion=mbraux->mbr_partition1;}
            else if(strcmp(mbraux->mbr_partition2.part_type,"e")){no_part_extendida=2;  particion=mbraux->mbr_partition2;}
            else if(strcmp(mbraux->mbr_partition3.part_type,"e")){no_part_extendida=3;  particion=mbraux->mbr_partition3;}
            else if(strcmp(mbraux->mbr_partition4.part_type,"e")){no_part_extendida=4;  particion=mbraux->mbr_partition4;}

            if(no_part_extendida==0){printf("\nNo se encontro la particion solicitada\n"); fclose(disco); return 0; }//si no se encuentra la particion ni en logicas/
            inicio_sistema_ext= get_inicio_logica(particion.part_name,pathp,particion); //mando a traer el inicio de donde es que tengo que escribirel inicio del istema de archivos
            if(inicio_sistema_ext<=0){printf("\nError al escribir la particion.\n"); fclose(disco); return 0;}//si existe un error al traer el valor del inicio del sistema de arhivos.
            tam_particion_sistema=tam_logica;
            tam_logica=-1;
        }

        int num_inodos;
        int num_bloques;
        num_inodos=numero_estructuras(tam_particion_sistema);//se obtiene el numero de inodos
        num_bloques=num_inodos*3;//se obtiene el numero de bloques
        //estructuras a escribir en la particion
        super_bloque *sbloque=(super_bloque*)malloc(sizeof(super_bloque));//super_bloque que escribiremos en la particion
        journaling *journal=(journaling*)malloc(sizeof(journaling));//journaling  que escribiremos en la particion

        //en esta parte llenaremos el super bloque con la informacion inicial
        sbloque->s_filesytem_type=1;//revisar
        sbloque->s_inodes_count=num_inodos;
        sbloque->s_blocks_count=num_bloques;
        sbloque->s_free_blocks_count=num_bloques;
        sbloque->s_free_inodes_count=num_inodos;
        fecha_sistema();//mandamos a actualizar la variable fecha
        strcpy(sbloque->s_mtime,fecha);
        sbloque->s_mnt_count=1;
        strcpy(sbloque->s_magic,"0xEF53");
        sbloque->s_inode_size=sizeof(inodo);
        sbloque->s_block_size=sizeof(bloque_apuntador);
        sbloque->s_first_ino=0;
        sbloque->s_first_blo=0;
            printf("\nINICIO DISCO:  %d\n",inicio_sistema_ext);
        //en esta parte veo si se activa o no el journaling
        if(strcmp(fs,"3fs")==0){//si es ext2 no activamos el journal
            sbloque->ext3=1;
            journal->activo=0;//activo cero osea NO ESTA ACTIVO
        }
        else{//si es ext3 activamos el journal
            sbloque->ext3=0;

            journal->activo=1;//activo 1 osea si esta ativo
        }


                addjournal(comprobarcadena,id,pathp,"se dio formato a particion",-1);



        fseek(disco,inicio_sistema_ext,SEEK_SET);//posiciono el puntero en el lugar donde empezare a escribir el sistema de archivos

        fwrite(sbloque,sizeof(super_bloque),1,disco);//escribo el super bloque en la particion
        fwrite(journal,sizeof(journaling),1,disco);//escribo el journaling en la particion

        char rellenar[1];//este char sive para rellenar tanto el bitmap de inodos como el de bloques
        strcpy(rellenar,"0");//le asignamos el valor inicial que indicara que todo esta vacio y se hace co un 0

        int i=0;

        int inicio_bitmap_inodos=ftell(disco);//guarda el inicio del puntero del bitmal de ploques

        for(i;i<=num_inodos;i++){
            fwrite(rellenar,sizeof(char),1,disco);//rellenamos todo el bitmap de inodos con cero
        }

        int inicio_bitmap_bloques=ftell(disco);//guardara el inicio  del bitmap de bloques que en este caso sera el fin del bitmap de inodos

        int ia=0;

        for(ia;ia<=num_bloques;ia++){
            fwrite(rellenar,sizeof(char),1,disco);//rellenamos todo el bitmap de bloques con ceros
        }

        int inicio_tabla_inodos=ftell(disco);//se asigna el valor del puntero donde inicia la tabla de inodos
        int j=0;
       // for(j;j<=num_inodos;j++){
            fseek(disco,sizeof(inodo)*num_inodos,SEEK_CUR);//se corre el espacio total que ocuparian todos los inodos si estubieran escritos
       // }
        int inicio_tabla_bloques=ftell(disco);//se asigna el valor del puntero donde inicia la tabla de bloques

        //terminamos de asignar valores al super bloque

        sbloque->s_bm_inode_start=inicio_bitmap_inodos;
        sbloque->s_bm_block_start=inicio_bitmap_bloques;
        sbloque->s_inode_start=inicio_tabla_inodos;
        sbloque->s_block_start=inicio_tabla_bloques;

        //reescribimos el bloque con todos los datos completos
        fseek(disco,inicio_sistema_ext,SEEK_SET);//posiciono el puntero en el lugar donde empezare a escribir el sistema de archivos

        fwrite(sbloque,sizeof(super_bloque),1,disco);//escribo el super bloque en la particion

        //pruebas
        fseek(disco,inicio_sistema_ext,SEEK_SET);
        super_bloque *sbloque1=(super_bloque*)malloc(sizeof(super_bloque));
        fread(sbloque1,sizeof(super_bloque),1,disco);


        //estableciendo valores de inodo carpeta
        inodo *inodo1=(inodo*)malloc(sizeof(inodo));
        int jj;
        for(jj=0;jj<15;jj++){inodo1->i_block[jj]=-1;}
        inodo1->i_type=1;
        inodo1->i_perm=777;
        inodo1->num_inodo=0;

        inodo1->i_block[0]=0;
        inodo1->i_link=0;
        strcpy(inodo1->i_pathlink,"");
        inodo1->i_ui=1;
        inodo1->i_gid=1;
        inodo1->i_size=0;
        fecha_sistema();
        strcpy(inodo1->i_time,fecha);
        fecha_sistema();
        strcpy(inodo1->i_ctime,fecha);
        fecha_sistema();
        strcpy(inodo1->i_mtime,fecha);

        //estableciendo valores de bloque

        bloque_carpeta *bloqueca=(bloque_carpeta*)malloc(sizeof(bloque_carpeta));
        bloqueca->num_bloque=0;


        bloqueca->b_content[0].b_nodo=0;
        bloqueca->b_content[1].b_nodo=0;
        bloqueca->b_content[2].b_nodo=1;
        bloqueca->b_content[3].b_nodo=-1;
        strcpy(bloqueca->esbloque,"si");
        strcpy(bloqueca->b_content[0].b_name,".");
        strcpy(bloqueca->b_content[1].b_name,"..");
        strcpy(bloqueca->b_content[2].b_name,"users.txt");




        //escribiendo el primer bloque y el primer inodo
        fseek(disco,sbloque1->s_block_start,SEEK_SET);
        strcpy(bloqueca->esbloque,"si");
        fwrite(bloqueca,sizeof(bloque_carpeta),1,disco);

        //escribiendo el primer inodo
        fseek(disco,sbloque1->s_inode_start,SEEK_SET);
        fwrite(inodo1,sizeof(inodo),1,disco);

        //haciendo archivo de usuarios

        usa_bloque(sbloque1,sbloque1->s_bm_block_start,pathp,inicio_sistema_ext);
        usa_inodos(sbloque1,sbloque1->s_bm_inode_start,pathp,inicio_sistema_ext);
        usa_bloque(sbloque1,sbloque1->s_bm_block_start,pathp,inicio_sistema_ext);
        usa_inodos(sbloque1,sbloque1->s_bm_inode_start,pathp,inicio_sistema_ext);

        inodo * i2=(inodo*)malloc(sizeof(inodo));
        bloque_archivo *b2 =(bloque_archivo*)malloc(sizeof(bloque_archivo));
            strcpy(b2->esbloque,"si");
        //estableciendo valores de inodo archivo
        for(jj=0;jj<15;jj++){i2->i_block[jj]=-1;}
        i2->i_block[0]=1;
        i2->i_link=0;
        strcpy(i2->i_pathlink,"");
        i2->i_ui=1;
        i2->i_gid=1;
        i2->i_size=0;
        fecha_sistema();
        strcpy(i2->i_time,fecha);
        fecha_sistema();
        strcpy(i2->i_ctime,fecha);
        fecha_sistema();
        strcpy(i2->i_mtime,fecha);
       // int jj;

        i2->i_type=0;
        i2->i_perm=777;
        i2->num_inodo=1;

        //escribiendo bloque archivo usarios
        strcpy(b2->esbloque,"si");
        b2->num_bloque=1;
        strcpy(b2->b_content,"1,g,root 1,u,root,root,201314158");

        fseek(disco,sbloque1->s_block_start,SEEK_SET);
        fseek(disco,sizeof(bloque_carpeta),SEEK_CUR);
        strcpy(b2->esbloque,"si");
        fwrite(b2,sizeof(bloque_archivo),1,disco);
        //escribiendo inodo de texto
        fseek(disco,sbloque1->s_inode_start,SEEK_SET);
        fseek(disco,sizeof(inodo),SEEK_CUR);
        fwrite(i2,sizeof(inodo),1,disco);

        bloque_carpeta*bloque1=(bloque_carpeta*)malloc(sizeof(bloque_carpeta));
        fseek(disco,sbloque1->s_block_start,SEEK_SET);
        fread(bloque1,sizeof(bloque_carpeta),1,disco);

        bloque_archivo*blo1=(bloque_archivo*)malloc(sizeof(bloque_archivo));
        strcpy(blo1->esbloque,"si");
        fseek(disco,sbloque1->s_block_start,SEEK_SET);
        fseek(disco,sizeof(bloque_archivo),SEEK_CUR);
        fread(blo1,sizeof(bloque_archivo),1,disco);


        int ii;
       // for(ii=0;ii<4;ii++){
           // printf("\n--- num inodo: %d , contenido: %s",bloque1->b_content[ii].b_nodo,bloque1->b_content[ii].b_name);
        //}
       // printf("\n CONTENIDO %s\n",blo1->b_content);
       // printf("primer bloque libre:  %d\n",sbloque1->s_first_blo);
      //  printf("primer inodo libre:  %d\n",sbloque1->s_first_ino);
         i=0;

        bloque_carpeta*blca=(bloque_carpeta*)malloc(sizeof(bloque_carpeta));
        fseek(disco,sbloque1->s_block_start,SEEK_SET);
        for(i;i<8;i++){
            fread(blca,sizeof(bloque_carpeta),1,disco);
               // printf("\nEs bloque: %s  --- %d --- %s\n",blca->esbloque,blca->num_bloque,blca->b_content[2].b_name);
              //  printf("\nEs bloque: %d\n",blca->num_bloque);
              //  fseek(disco,-sizeof(bloque_carpeta),SEEK_SET);
        }

        fclose(disco);//cerramos el fichero disco

    }

    return 1;
}































int numero_estructuras(int tam_part){

    int n_inodos=43;

    n_inodos=(tam_part-sizeof(super_bloque)-sizeof(ebr))/(4+sizeof(journaling)+sizeof(inodo)+(3*sizeof(bloque_carpeta)));

    return floor(n_inodos);    //devuelvo el numero de inodos que contendra la particion.
}
int get_inicio_logica(char *name1[20],char path1[200], partition particion){

        int inicio =particion.part_start;

          FILE *archivo=fopen(path1,"rb+");
          fseek(archivo,0,SEEK_SET);
          mbr *aux2 =(mbr *)malloc(sizeof(mbr));
          ebr *aux3=(ebr *)malloc(sizeof(ebr));
          fread(aux2,sizeof(mbr),1,archivo);
          fseek(archivo,inicio,SEEK_CUR);
          fread(aux3,sizeof(ebr),1,archivo);

          int o;
          int bandera=0;

          while(bandera==0){

            o=aux3->part_size;
            fseek(archivo,o,SEEK_CUR);
             if( strcmp(aux3->part_name,name1)==0){
                    tam_logica=aux3->part_size;
                    int inicio_log=ftell(archivo);
                    fclose(archivo);
                    return inicio_log;
                    }

             else if(aux3->part_next==-1){
             break;
             }
             else{
                 fseek(archivo,0,SEEK_CUR);
                 fread(aux3,sizeof(ebr),1,archivo);
             }
          }

          fclose(archivo);



    return 0;
}
void fecha_sistema(){

    timer_t tiempo = time(0);
    struct tm *tlocal=localtime(&tiempo);
    char output[128];
    strftime(output,128,"%d/%m/%y %H:%M:%S",tlocal);

    strcpy(fecha,output);

}
int usa_bloque(super_bloque *superB, int i_bm_b, char path[200], int i_sis){//Esta funcion retorna el numero de bloque a usar, y ademas escribe el primer bloque libre


    FILE *sistema =fopen(path,"rb+");
   //inicialiso todo pra leer el bitmap de bloques
    int bandera=0;//bandera.
    int i=0;//inicio for
    char aux[2]="";//char donde guardate los 1 o ceros que leere
    int num_blocks=superB->s_blocks_count;//asigo el numero de bloques que hay
    int num_bloque_usado;

    fseek(sistema,i_bm_b,SEEK_SET);//posiciono el puntero al inicio del bitmap de bloques
   // printf("\n");
    for(i;i<=num_blocks;i++){//lee caracter a caracter a partir del inicio del bitmap de bloques

     //   printf("%s,",aux);
        fread(aux,sizeof(char),1,sistema);//leo caracter a caracte

        if(strcmp(aux,"0")==0 && bandera==0){//si encuentro cero ingreso a este if
            num_bloque_usado=i;//asigno el numero de bloque que utilizare
            fseek(sistema,-(sizeof(char)),SEEK_CUR);//me posiciono enfrente del cero que acabo de encontrar
            char ocupado[1]="1";//char a escribir
            fwrite(ocupado,sizeof(char[1]),1,sistema);//escribo un uno donde habia un cero para indicar que esta ocupado
            bandera=1;//asigno 1 a bandera para seguir recorriendo el bitmap
            strcpy(aux,"");
        }

        if(strcmp(aux,"0")==0 && bandera==1){//si encuentra el primer inodo libre
            superB->s_first_blo=i;//escribo el primer inodo libre que queda
           fseek(sistema,i_sis,SEEK_SET);
           fwrite(superB,sizeof(super_bloque),1,sistema);
          // printf("\n bloque libre %d\n",i);
            break;//me salgo del bucle
        }

    }

    fclose(sistema);//cierro el archivo sistema
    return num_bloque_usado;//retorno el numero de bloque que utilizare

}
int usa_inodos(super_bloque *superB, int inicio_inodos, char path[200],int i_sis){//Esta funcion retorna el numero de bloque a usar, y ademas escribe el primer bloque libre


    FILE *sistema =fopen(path,"rb+");

    int i =0;//variable de for
    int bandera=0;//variable que indica el primer inodo libre en el bitmap
    char aux[2]="";//variable para almacenar ell caracter
    int num_inodos=superB->s_inodes_count;
    fseek(sistema,inicio_inodos,SEEK_CUR);//posiciono el puntero al inicio del bitmap de inodos
    int inodo_usado;
    for(i;i<=num_inodos;i++){
    //lee caracter a caracter a partir del inicio del bitmap de inodos

    fread(aux,sizeof(char),1,sistema);//leo caracter a caracte

    if(strcmp(aux,"0")==0 && bandera==0){//si encuentro cero ingreso a este if

        fseek(sistema,-(sizeof(char)),SEEK_CUR);//me posiciono enfrente del cero que acabo de encontrar
        inodo_usado=i;
        char ocupado[1]="1";//char a escribir
        fwrite(ocupado,sizeof(char),1,sistema);//escribo un uno donde habia un cero para indicar que esta ocupado
        bandera=1;//asigno 1 a bandera para seguir recorriendo el bitmap
        strcpy(aux,"");
    }

    if(strcmp(aux,"0")==0 && bandera==1){//si encuentra el primer inodo libre
        superB->s_first_ino=i;//escribo el primer inodo libre que queda
        fseek(sistema,i_sis,SEEK_SET);
        fwrite(superB,sizeof(super_bloque),1,sistema);

       //printf("\n inodo libre %d\n",i);
        break;//me salgo del bucle
    }


    }

    fclose(sistema);//cierro el archivo sistema
    return inodo_usado;//retorno el numero de bloque que utilizare

}

int numero_path(char path[200]){//devuelve el numero de carpetas a ingresar ejemplo /home/wilson/mia devuelve 3

    int num_carpeta=0;

    char *tok=strtok(path,"/");

    while(tok!=NULL){
        num_carpeta++;
        tok=strtok(NULL,"/");

    }
    return num_carpeta;

}
int numero_path2(char pat[200]){//devuelve el numero de carpetas a ingresar ejemplo /home/wilson/mia devuelve 3
    FILE*as=fopen("as.txt","w");
    char p[200]="";
    strcpy(p,pat);
    fprintf(as,"%s",p);

    fclose(as);
    FILE*as1=fopen("as.txt","r");
    char path[200]="";

    fread(path,sizeof(char),200,as1);

    fclose(as1);


    int num_carpeta=0;

    char *tok=strtok(path,"/");

    while(tok!=NULL){
        num_carpeta++;
        tok=strtok(NULL,"/");

    }
    return num_carpeta;

}

int crea_carpeta(char path[200], int iti, int inodo_buscar, char path_buscar[200], int pplus,int num_inodos, super_bloque *sb,int i_sis,int i_usa,char idie[20], char pathie[200],int ar){//este metodo retorna el bloque que contendra el inodo crear
    FILE *disco =fopen(path,"rb+");
    char path_b[200];
    strcpy(path_b,path_buscar);
    char path_bb[200];
    strcpy(path_bb,path_buscar);
    //char *tok=strtok(path_b,"/");
    int ultimo =0;
    char pathq[200];
    strcpy(pathq,path_buscar);
    int num_path=numero_path(pathq);
    inodo *inodo_aux=(inodo*)malloc(sizeof(inodo));
    fseek(disco,iti,SEEK_SET);
    inodo *inodo1=(inodo*)malloc(sizeof(inodo));
    padre=0;
    char nombre_carpeta[100]="";
    //nombre_ar(&nombre_carpeta,pathw,num_path);
        int po=1;
    while(num_path>0){
        strcpy(path_b,path_buscar);
        nombre_ar(&nombre_carpeta,path_b,po);


        if(num_path==1){
            int j=0;
            FILE *disco =fopen(path,"rb+");

            fseek(disco,iti,SEEK_SET);
            int aux=-1;
            for(j;j<num_inodos;j++){
                    fread(inodo_aux,sizeof(inodo),1,disco);

                  //  printf("\ninodo: %d buscando: %d    j=%d tiempo %s --%s\n",inodo_aux->num_inodo,inodo_buscar,j,inodo_aux->i_time,inodo_aux->i_mtime);

                    if(inodo_aux->num_inodo!=inodo_buscar){continue;}
                    int i=0;
                    int espacio1=-34534;
                    for(i;i<15;i++){

                      /**********************************************************************************************************************/
                        if(inodo_aux->i_block[i]==-1){//si hay que agregar un nuevo bloque al inodo

                        fseek(disco,-sizeof(inodo),SEEK_CUR);

                        int bloque_u=usa_bloque(sb,sb->s_bm_block_start,path,i_sis);
                        inodo_aux->i_block[i]=bloque_u;

                        fwrite(inodo_aux,sizeof(inodo),1,disco);//escribiendo el inodo con su nuevo bloque


                       padre =inodo_buscar;//coloca padre como -1 para tener referencia a NULL
                        bloque_carpeta *bloq=(bloque_carpeta*)malloc(sizeof(bloque_carpeta));//bloque a escribir
                        //colocando valores a bloque a escribir
                        bloq->num_bloque=bloque_u;
                        bloq->b_content[0].b_nodo=-1;
                        bloq->b_content[1].b_nodo=-1;
                        bloq->b_content[2].b_nodo=-1;
                        bloq->b_content[3].b_nodo=-1;
                        //bloque auxiliar para lectura
                        bloque_carpeta *bloq2=(bloque_carpeta*)malloc(sizeof(bloque_carpeta));
                        int mk=0;
                        fseek(disco,sb->s_block_start,SEEK_SET);

                        for(mk;mk<sb->s_blocks_count;mk++){
                            fread(bloq2,sizeof(bloque_carpeta),1,disco);
                                     char jas[222];
                                     strcpy(jas,bloq2->esbloque);
                                     strcpy(jas,jas);
                            if(bloq2->num_bloque>8000000){
                                fseek(disco,-sizeof(bloque_carpeta),SEEK_CUR);
                                strcpy(bloq->esbloque,"si");
                                fwrite(bloq,sizeof(bloque_carpeta),1,disco);
                                break;
                            }
                        }

                        /*********************/
                            int pp=0;
                            fseek(disco,sb->s_block_start,SEEK_SET);
                            bloque_carpeta *b5=(bloque_carpeta*)malloc(sizeof(bloque_carpeta));
                            for(pp;pp<sb->s_blocks_count;pp++){
                                fread(b5,sizeof(bloque_carpeta),1,disco);

                                if(b5->num_bloque==3){

                                   // printf("\nENCONTRADO\n");
                                }


                            }

                        /********************/

                        return bloque_u;
                        }
                        else{

                           // printf("\nmando a busar espacio\n");
                            int jpjp=inodo_aux->i_block[i];
                            jpjp;
                            espacio1=espacio(path,sb->s_block_start,jpjp,sb->s_blocks_count);


                            printf("\nespacio %d\n", espacio1);
                            if(espacio1==1){

                                fclose(disco);

                                return inodo_aux->i_block[i];
                            }


                        }

                    }
            }
            fclose(disco);

            printf("\nSISTEMA NO ES CAPAZ DE ALVERGAR MAS CARPETAS\n");


            return -1; //si ya no hay espacio en el inodo para alvergar mas

        }else{

            int j=0;
            FILE *disco =fopen(path,"rb+");

            fseek(disco,iti,SEEK_SET);
              int aux=-1;
            for(j;j<num_inodos;j++){
                fread(inodo_aux,sizeof(inodo),1,disco);

                if(inodo_aux->num_inodo==inodo_buscar){

                    int existe_nombre=buscar_nombre(inodo_aux,nombre_carpeta,path,sb->s_block_start,sb->s_blocks_count);
                    if(existe_nombre!=-10){
                        inodo_buscar=existe_nombre;
                        aux =5;
                        break;
                    }
                    else if(existe_nombre==-10 && pplus==1){
                       char pr[200];
                       strcpy(path_bb,path_buscar);
                        nombre_r(&pr,path_bb,po);
                        mkdir1(NULL,idie,pr,1,1,0);
                        existe_nombre=buscar_nombre(inodo_aux,nombre_carpeta,path,sb->s_block_start,sb->s_blocks_count);
                        inodo_buscar=existe_nombre;
                        aux =5;
                        break;


                    }else{

                        fclose(disco);

                        printf("\nno se puede crear carpeta\n");
                        return -1;
                    }



                }

            }
            fclose(disco);

        }
        if(num_path==2){
        padre=inodo_buscar;
        }


        po++;
        num_path--;
    }

    fclose(disco);
}

int buscar_nombre(inodo *aux, char name[20],char path[200],int itb, int num_b){
    FILE *disco =fopen(path,"rb+");

    bloque_carpeta *bc=(bloque_carpeta*)malloc(sizeof(bloque_carpeta));

    int l=0;
    int numb;
    int contador=1;
    int existe;
    for(l;l<15;l++){

        numb=aux->i_block[l];
        fseek(disco,itb,SEEK_SET);

        while(contador<num_b){
            fread(bc,sizeof(bloque_carpeta),1,disco);

            if(numb==bc->num_bloque){
                int m=0;
                for(m;m<4;m++){
                    if(strcmp( bc->b_content[m].b_name,name)==0){

                        return bc->b_content[m].b_nodo;
                    }
                }
                break;
            }
            contador++;
        }
    }

    fclose(disco);


    return -10;


}
 espacio(char path[200],int itb, int num_b1,int cuantos){
    FILE *disco =fopen(path,"rb+");

    bloque_carpeta *bc=(bloque_carpeta*)malloc(sizeof(bloque_carpeta));
    int contador=1;

    fseek(disco,itb,SEEK_SET);

     while(contador<cuantos){
            fread(bc,sizeof(bloque_carpeta),1,disco);
           // printf("\n NUMERO BLOQUE %d\n",bc->num_bloque);
            if(num_b1==bc->num_bloque){

                int m=0;
                for(m;m<4;m++){
                    if(bc->b_content[m].b_nodo==-1){
                        fclose(disco);
                        return 1;
                    }
                }
            }
            contador++;
     }


    fclose(disco);


    return -500;


}
//mkdir
int mkdir1(char*token,char idie[20], char pathie[200], int pie,int recu, int ar){
     //  ar=0;
    int op =0;
    int p=0;
    char id[10];
    char path[200];
    char path_disc [200];
    char nombre_part[30];
    int inicio_part;


    if(recu==0){
    while(token !=NULL){
        //printf("\nentro a while\n");
        token=strtok(NULL," :");
        if(token==NULL) break;


        if(strstr(token,"-path"))   op=1;
        if(strstr(token,"-id"))     op=2;
        if(strstr(token,"+p"))     op=3;

        //printf("\nVAL TOKEN: %s , %d\n",token,op);

     switch(op){
        case 1:
            token=strtok(NULL,":\"\n");
            strcpy(path,"/");
            char *tok;
            if(strstr(token,"\"")){

                strcpy(path,tok);
            }else{
                strcpy(path,token);
            }




            break;
        case 2:

            token=strtok(NULL," :");
            strcpy(id,token);
            registro* aux =(registro *)malloc(sizeof(registro));
            registro* aux1 =(registro *)malloc(sizeof(registro));
            registro* aux2 =(registro *)malloc(sizeof(registro));
            aux=re->primero;
            int n=re->tam;
            int encontrado=0;
            while(n>=1){

                if(strcmp(id,aux->id)==0){
                    encontrado=1;
                    break;
                }
                aux=aux->siguiente;
                n--;
            }
            strcpy(path_disc,aux->path);
            inicio_part=aux->start;
            strcpy(nombre_part,aux->name);

            if(encontrado!=1){
                printf("\nNo existe una particion montada con ese id\n");
                return -1;
            }


            break;

        case 3:
            if(strstr(comprobarcadena,"+p")){p=1;}

            break;

        default:
            printf("\ncomandos invalidos\n");
            return -1;
            break;
        }


    }
    }else{
        strcpy(id,idie);
        strcpy(path,pathie);
        p=pie;
       // strcpy(id,token);
        registro* aux =(registro *)malloc(sizeof(registro));
        registro* aux1 =(registro *)malloc(sizeof(registro));
        registro* aux2 =(registro *)malloc(sizeof(registro));
        aux=re->primero;
        int n=re->tam;
        int encontrado=0;
        while(n>=1){

            if(strcmp(id,aux->id)==0){
                encontrado=1;
                break;
            }
            aux=aux->siguiente;
            n--;
        }
        strcpy(path_disc,aux->path);
        inicio_part=aux->start;
        strcpy(nombre_part,aux->name);

        if(encontrado!=1){
            printf("\nNo existe una particion montada con ese id\n");
            return -1;
        }




    }

    //printf("\nmagico \n");
    FILE *disco =fopen(path_disc,"rb+");

    mbr *mbraux=(mbr*)malloc(sizeof(mbr));
    fseek(disco,0,SEEK_SET);
    fread(mbraux,sizeof(mbr),1,disco);
    if(strcmp(nombre_part,mbraux->mbr_partition1.part_name)==0){inicio_part=mbraux->mbr_partition1.part_start;}
    else if(strcmp(nombre_part,mbraux->mbr_partition2.part_name)==0){inicio_part=mbraux->mbr_partition2.part_start;}
    else if(strcmp(nombre_part,mbraux->mbr_partition3.part_name)==0){inicio_part=mbraux->mbr_partition3.part_start;}
    else if(strcmp(nombre_part,mbraux->mbr_partition4.part_name)==0){inicio_part=mbraux->mbr_partition4.part_start;}
    else{
        if(strcmp("e",mbraux->mbr_partition1.part_type)==0){inicio_part=get_inicio_logica(nombre_part,path_disc,mbraux->mbr_partition1);}
        else if(strcmp("e",mbraux->mbr_partition2.part_type)==0){inicio_part=get_inicio_logica(nombre_part,path_disc,mbraux->mbr_partition2);}
        else if(strcmp("e",mbraux->mbr_partition3.part_type)==0){inicio_part=get_inicio_logica(nombre_part,path_disc,mbraux->mbr_partition3);}
        else if(strcmp("e",mbraux->mbr_partition4.part_type)==0){inicio_part=get_inicio_logica(nombre_part,path_disc,mbraux->mbr_partition4);}

    }


    super_bloque *sb=(super_bloque*)malloc(sizeof(super_bloque));
    fseek(disco,inicio_part,SEEK_SET);
    fread(sb,sizeof(super_bloque),1,disco);
    char pathw[200];
    strcpy(pathw,path);
    if(sb->ext3==1){
        if(ar==0){
            addjournal(comprobarcadena,id,path,"Crear Carpeta",1);
        }else{
            addjournal(comprobarcadena,id,path,"Crear archivo",0);
        }

    }

    int bloque_inicio=crea_carpeta(path_disc,sb->s_inode_start,0,path,p,sb->s_inodes_count,sb,inicio_part,9,id,path,ar);
    int bloque_usa;
    if(ar==0){
     bloque_usa=usa_bloque(sb,sb->s_bm_block_start,path_disc,inicio_part);
   }
    int inodo_usa=usa_inodos(sb,sb->s_bm_inode_start,path_disc,inicio_part);
    in=inodo_usa;
/*********************/
    int pp=0;
    fseek(disco,sb->s_block_start,SEEK_SET);
    bloque_carpeta *b5=(bloque_carpeta*)malloc(sizeof(bloque_carpeta));
    for(pp;pp<sb->s_blocks_count;pp++){
        fread(b5,sizeof(bloque_carpeta),1,disco);

        if(b5->num_bloque==3){

            printf("\nENCONTRADO\n");
        }


    }

/********************/


    bloque_carpeta *bloque=(bloque_carpeta*)malloc(sizeof(bloque_carpeta));
    fseek(disco,sb->s_block_start,SEEK_SET);
    int num_b=sb->s_blocks_count;
    int pos_libre=-1;
    int cas=0;
    while(num_b>0){
        fread(bloque,sizeof(bloque_carpeta),1,disco);
       // if(cas<10)printf("\n%d               %d\n",bloque_inicio,bloque->num_bloque);
        if(bloque->num_bloque==bloque_inicio){

            break;
        }
        cas++;
        num_b--;
    }
    int j=0;

    for(j;j<4;j++){

        if(bloque->b_content[j].b_nodo==-1){
         pos_libre=j;
         bloque->b_content[j].b_nodo=inodo_usa;
            break;
        }
    }

    bloque->b_content[j].b_nodo=inodo_usa;
    char nombre_carpeta[100]="";
    nombre_ca(&nombre_carpeta,pathw);
    strcpy(bloque->b_content[j].b_name,nombre_carpeta);
    fseek(disco,sb->s_block_start,SEEK_SET);
    num_b=sb->s_blocks_count;
    strcpy(bloque->esbloque,"si");
    int sas=1;

    fseek(disco,sb->s_block_start,SEEK_SET);
    bloque_carpeta *blaux=(bloque_carpeta*)malloc(sizeof(bloque_carpeta));
    sas=1;
    for(sas;sas<sb->s_blocks_count;sas++){

        fread(blaux,sizeof(bloque_carpeta),1,disco);

        if(blaux->num_bloque==bloque_inicio){

          fseek(disco,-sizeof(bloque_carpeta),SEEK_CUR);
            blaux->b_content[j].b_nodo=inodo_usa;
            strcpy(blaux->b_content[j].b_name,nombre_carpeta);
            strcpy(blaux->esbloque,"si");
            if(blaux==5){ return-1;}
            fwrite(bloque,sizeof(bloque_carpeta),1,disco);

            break;
        }



    }








    int i=0;
    bloque_carpeta*blca=(bloque_carpeta*)malloc(sizeof(bloque_carpeta));


    //asignando valores al nuevo bloque de carpeta
    if(ar==0){
    if(padre==-230){
        blca->num_bloque=bloque_usa;
        blca->b_content[0].b_nodo=-1;
        blca->b_content[1].b_nodo=-1;
        blca->b_content[2].b_nodo=-1;
        blca->b_content[3].b_nodo=-1;
        strcpy(blca->b_content[0].b_name,"");
        strcpy(blca->b_content[1].b_name,"");



    }else{
    blca->num_bloque=bloque_usa;
    blca->b_content[0].b_nodo=padre;
    blca->b_content[1].b_nodo=inodo_usa;
    blca->b_content[2].b_nodo=-1;
    blca->b_content[3].b_nodo=-1;

    strcpy(blca->b_content[1].b_name,".");
    strcpy(blca->b_content[0].b_name,"..");
    }
   }

    bloque_apuntador* bap=(bloque_apuntador*)malloc(sizeof(bloque_apuntador));
    bloque_archivo* ba=(bloque_archivo*)malloc(sizeof(bloque_archivo));
    bloque_carpeta* bc=(bloque_carpeta*)malloc(sizeof(bloque_carpeta));
    i=0;
   if(ar==0){
       fseek(disco,sb->s_block_start,SEEK_SET);
    for(i;i<sb->s_blocks_count;i++){
            fread(bap,sizeof(bloque_apuntador),1,disco);
            fseek(disco,-sizeof(bloque_apuntador),SEEK_CUR);
            fread(ba,sizeof(bloque_archivo),1,disco);
            fseek(disco,-sizeof(bloque_archivo),SEEK_CUR);
            fread(bc,sizeof(bloque_carpeta),1,disco);


    if(strcmp(bap->esbloque,"si")!=0 && strcmp(ba->esbloque,"si")!=0 && strcmp(bc->esbloque,"si")!=0){
        fseek(disco,-sizeof(bloque_carpeta),SEEK_CUR);
        strcpy(blca->esbloque,"si");
        if(ar==0){
        fwrite(blca,sizeof(bloque_carpeta),1,disco);


        }
        break;

    }





    }
    }
    /**************************************************************************/

        inodo * i2=(inodo*)malloc(sizeof(inodo));

        //estableciendo valores de inodo archivo
        int jj;
        for(jj=0;jj<15;jj++){i2->i_block[jj]=-1;}
        if(ar==0){
        i2->i_block[0]=bloque_usa;
         }
        i2->i_link=0;
        strcpy(i2->i_pathlink,path);
        i2->i_ui=1;
        i2->i_gid=1;
        i2->i_size=0;
        fecha_sistema();
        strcpy(i2->i_time,fecha);
        fecha_sistema();
        strcpy(i2->i_ctime,fecha);
        fecha_sistema();
        strcpy(i2->i_mtime,fecha);


        i2->i_type=1;

        if(ar==3){
            i2->i_type=0;
        }

        i2->i_perm=664;
        i2->num_inodo=inodo_usa;
        /*ESCRiBIENDO EL INODO i2*/

        fseek(disco,sb->s_inode_start,SEEK_SET);

        inodo * i3=(inodo*)malloc(sizeof(inodo));
        int ka=1;
        for(ka;ka<sb->s_inodes_count;ka++){
            fread(i3,sizeof(inodo),1,disco);

            if(i3->num_inodo>8000000){
                fseek(disco,-sizeof(inodo),SEEK_CUR);
                fwrite(i2,sizeof(inodo),1,disco);
                break;
            }

        }

    /***************************************************************************/


       fclose(disco);
       return 0;
}

void nombre_ca(char *nombre, char path[200]){
        char pathw[200];
        strcpy(pathw,path);
    int num=numero_path(pathw);
    char *tok=strtok(path,"/");

    while(num>0){
        strcpy(nombre,tok);
        tok=strtok(NULL,"/");
        num--;
    }

}
void nombre_ar(char *nombre, char path[200],int n){
        char pathw[200];
        strcpy(pathw,path);
    int num=numero_path(pathw);
    char *tok=strtok(path,"/");

    while(n>0){
        strcpy(nombre,tok);
        tok=strtok(NULL,"/");
        n--;
    }

}
void nombre_r(char *nombre, char path[200],int n){
        char pathw[200];
        strcpy(pathw,path);
    int num=numero_path(pathw);
    char *tok=strtok(path,"/");
char todo[200]="";
    while(n>0){
        strcpy(nombre,tok);
        strcat(todo,"/");
        strcat(todo,nombre);
        tok=strtok(NULL,"/");
        n--;
    }

    strcpy(nombre,todo);

}
void rep2f(char name1[200],char dpth3[200],char mandar[30],char path_mandar[200], char ide[20]){//nom. reporte, direccion imagen, name particion,path disco



    FILE *disco=fopen(path_mandar,"rb+");
    //trayendo el inicio de la particion,,, donde esta el super bloque
    mbr *mbraux=(mbr*)malloc(sizeof(mbr));
    int inicio_part =-1;
    fseek(disco,0,SEEK_SET);
    fread(mbraux,sizeof(mbr),1,disco);
         if(strcmp(mandar,mbraux->mbr_partition1.part_name)==0){inicio_part=mbraux->mbr_partition1.part_start;}
    else if(strcmp(mandar,mbraux->mbr_partition2.part_name)==0){inicio_part=mbraux->mbr_partition2.part_start;}
    else if(strcmp(mandar,mbraux->mbr_partition3.part_name)==0){inicio_part=mbraux->mbr_partition3.part_start;}
    else if(strcmp(mandar,mbraux->mbr_partition4.part_name)==0){inicio_part=mbraux->mbr_partition4.part_start;}
    else{
             if(strcmp("e",mbraux->mbr_partition1.part_type)==0){inicio_part=get_inicio_logica(mandar,path_mandar,mbraux->mbr_partition1);}
        else if(strcmp("e",mbraux->mbr_partition2.part_type)==0){inicio_part=get_inicio_logica(mandar,path_mandar,mbraux->mbr_partition2);}
        else if(strcmp("e",mbraux->mbr_partition3.part_type)==0){inicio_part=get_inicio_logica(mandar,path_mandar,mbraux->mbr_partition3);}
        else if(strcmp("e",mbraux->mbr_partition4.part_type)==0){inicio_part=get_inicio_logica(mandar,path_mandar,mbraux->mbr_partition4);}

    }
    fseek(disco,inicio_part,SEEK_SET);
    char grafico[500]="";
    FILE *r2=fopen("r2.dot","w");

    super_bloque *sb =(super_bloque*)malloc(sizeof(super_bloque));
    fread(sb,sizeof(super_bloque),1,disco);

    if(strcmp(name1,"sb")==0){
        fprintf(r2,"digraph html{ label=\"REPORTE SUPER BLOQUE\"; inodo0[ shape=none, margin=0, label = <<table border=\"2px\" ><tr><td><b>NOMBRE</b></td><td><b>VALOR</b></td></tr><tr><td>s_inodes_count</td><td>%d</td></tr><tr><td>s_blocks_count</td><td>%d</td></tr><tr><td>s_free_blocks_count</td><td>%d</td></tr><tr><td>s_free_inodes_count</td><td>%d</td></tr><tr><td>s_mtime</td><td>%s</td></tr><tr><td>s_umtime</td><td>%s</td></tr><tr><td>s_mnt_count</td><td>%d</td></tr><tr><td>s_magic</td><td>%s</td></tr><tr><td>s_inode_size</td><td>%d</td></tr><tr><td>s_block_size</td><td>%d</td></tr><tr><td>s_first_ino</td><td>%d</td></tr><tr><td>s_first_blo</td><td>%d</td></tr><tr><td>s_bm_inode_start</td><td>%d</td></tr><tr><td>s_bm_block_start</td><td>%d</td></tr><tr><td>s_inode_start</td><td>%d</td></tr><tr><td>s_block_start</td><td>%d</td></tr></table>>];}",sb->s_inodes_count,sb->s_blocks_count,sb->s_free_blocks_count,sb->s_free_inodes_count,sb->s_mtime,sb->s_utime,sb->s_mnt_count,sb->s_magic,sb->s_inode_size,sb->s_block_size,sb->s_first_ino,sb->s_first_blo,sb->s_bm_inode_start,sb->s_bm_block_start,sb->s_inode_start,sb->s_block_start);
    }
    else if(strcmp(name1,"inode")==0){
        int guarda=0;
        int as=0;
        int b=1;

        inodo *iaux=(inodo*)malloc(sizeof(inodo));

        fseek(disco,sb->s_inode_start,SEEK_SET);

        fprintf(r2,"digraph{ rankdir = LR; node [shape = record]; subgraph clustercir4{ label = \"REPORTE INODE\"; color=black;");

         for(b;b<sb->s_inodes_count;b++){
            fread(iaux,sizeof(inodo),1,disco);


            if(iaux->num_inodo<8000000){


                if(as>0){fprintf(r2,"i%d->i%d;",guarda,b);}
                guarda=b;
                as++;
                fprintf(r2,"i%d[label=\"inodo %d|i_uid   %d|i_size  %d|i_atime   %s|.|i_block_1   %d|i_block_2   %d|i_block_3   %d|i_block_4   %d|i_block5   %d|i_block_6   %d|i_block_7   %d|i_block_8   %d|i_block_9   %d|i_block_10   %d|i_block_11   %d|i_block_12   %d|i_block_13   %d|i_block_14   %d|i_block_15   %d|.|i_perm   %d|i_nlink %d|i_pathlink %s|i_ctime %s|i_mtime %s|i_type %d\"];",b,iaux->num_inodo,iaux->i_ui,iaux->i_size,iaux->i_time,iaux->i_block[0],iaux->i_block[1],iaux->i_block[2],iaux->i_block[3],iaux->i_block[4],iaux->i_block[5],iaux->i_block[6],iaux->i_block[7],iaux->i_block[8],iaux->i_block[9],iaux->i_block[10],iaux->i_block[11],iaux->i_block[12],iaux->i_block[13],iaux->i_block[14],iaux->i_perm,iaux->i_link,iaux->i_pathlink,iaux->i_ctime,iaux->i_mtime,iaux->i_type);

            }
        }

        fprintf(r2,"}}");
    }
    else if (strcmp(name1,"block")==0){
    int guarda=0;
        int as=0;
        int b=1;

    bloque_apuntador* bap=(bloque_apuntador*)malloc(sizeof(bloque_apuntador));
    bloque_archivo* ba=(bloque_archivo*)malloc(sizeof(bloque_archivo));
    bloque_carpeta* bc=(bloque_carpeta*)malloc(sizeof(bloque_carpeta));
        fseek(disco,sb->s_block_start,SEEK_SET);
//        tipo=0;
        fprintf(r2,"digraph{ rankdir = LR; node [shape = record]; subgraph clustercir4{ label = \"REPORTE BLOCK\"; color=black;");

         for(b;b<sb->s_blocks_count;b++){
            fread(bap,sizeof(bloque_apuntador),1,disco);
            fseek(disco,-sizeof(bloque_apuntador),SEEK_CUR);
            fread(ba,sizeof(bloque_archivo),1,disco);
            fseek(disco,-sizeof(bloque_archivo),SEEK_CUR);
            fread(bc,sizeof(bloque_carpeta),1,disco);

           /* if(strcmp(bap->esbloque,"si")==0){

            if(as>0){fprintf(r2,"i%d->i%d;",guarda,b);}
                guarda=b;
                as++;
                fprintf(r2,"i%d[label=\"BLOQUE APUNTADOR %d|%d, %d, %d, %d, %d, \\n %d, %d, %d, %d, %d,\\n  %d, %d, %d, %d, %d\"];",b,bap->num_bloque,bap->b_pointers[0],bap->b_pointers[1],bap->b_pointers[2],bap->b_pointers[3],bap->b_pointers[4],bap->b_pointers[5],bap->b_pointers[6],bap->b_pointers[7],bap->b_pointers[8],bap->b_pointers[9],bap->b_pointers[10],bap->b_pointers[11],bap->b_pointers[12],bap->b_pointers[13],bap->b_pointers[14]);


            }*/
            if(strcmp(ba->esbloque,"si")==0){
            if(as>0){fprintf(r2,"i%d->i%d;",guarda,b);}
                guarda=b;
                as++;
                fprintf(r2,"i%d[label=\"BLOQUE ARCHIVO %d| %s\"];",b,ba->num_bloque,ba->b_content);

            }
            if(strcmp(bc->esbloque,"si")==0){

            if(as>0){fprintf(r2,"i%d->i%d;",guarda,b);}
                guarda=b;
                as++;
                fprintf(r2,"i%d[label=\"BLOQUE CARPETA %d|%s----%d |%s----%d|%s----%d|%s----%d\"];",b,bc->num_bloque,bc->b_content[0].b_name,bc->b_content[0].b_nodo,bc->b_content[1].b_name,bc->b_content[1].b_nodo,bc->b_content[2].b_name,bc->b_content[2].b_nodo,bc->b_content[3].b_name,bc->b_content[3].b_nodo);

            }


        }

        fprintf(r2,"}}");
    }
    else if (strcmp(name1,"tree")==0){

        int guarda=0;
        int as=0;
        int b=1;

        inodo *iaux=(inodo*)malloc(sizeof(inodo));

        fseek(disco,sb->s_inode_start,SEEK_SET);

        fprintf(r2,"digraph{ rankdir = LR; node [shape = record]; subgraph clustercir4{ label = \"REPORTE TREE\"; color=black;");

         for(b;b<sb->s_inodes_count;b++){
            fread(iaux,sizeof(inodo),1,disco);

            if(iaux->num_inodo<8000000){
                fprintf(r2,"i%d[color = blue label=\"<a98>inodo %d|i_ui   %d|i_size  %d|i_atime   %s|.|<f%d> i_block_1   %d|<f%d> i_block_2   %d|<f%d> i_block_3   %d|<f%d> i_block_4   %d| <f%d> i_block5   %d| <f%d> i_block_6   %d|<f%d> i_block_7   %d|<f%d> i_block_8   %d|<f%d> i_block_9   %d|<f%d>  i_block_10   %d|<f%d> i_block_11   %d|<f%d> i_block_12   %d|<f%d> i_block_13   %d|<f%d> i_block_14   %d|<f%d> i_block_15   %d|i_perm   %d|i_nlink %d|i_pathlink %s|i_ctime %s|i_mtime %s|i_type %d\"];",iaux->num_inodo,iaux->num_inodo,iaux->i_ui,iaux->i_size,iaux->i_time,iaux->i_block[0],iaux->i_block[0],iaux->i_block[1],iaux->i_block[1],iaux->i_block[2],iaux->i_block[2],iaux->i_block[3],iaux->i_block[3],iaux->i_block[4],iaux->i_block[4],iaux->i_block[5],iaux->i_block[5],iaux->i_block[6],iaux->i_block[6],iaux->i_block[7],iaux->i_block[7],iaux->i_block[8],iaux->i_block[8],iaux->i_block[9],iaux->i_block[9],iaux->i_block[10],iaux->i_block[10],iaux->i_block[11],iaux->i_block[11],iaux->i_block[12],iaux->i_block[12],iaux->i_block[13],iaux->i_block[13],iaux->i_block[14],iaux->i_block[14],iaux->i_perm,iaux->i_link,iaux->i_pathlink,iaux->i_ctime,iaux->i_mtime,iaux->i_type);
                int ass=0;
                for(ass;ass<15;ass++){ if(iaux->i_block[ass]!=-1){
                        fprintf(r2,"\"i%d\":f%d->\"b%d\":f%d;",iaux->num_inodo,iaux->i_block[ass],iaux->i_block[ass],iaux->i_block[ass]);
                    }
                }
            }
        }


         /**********************************************************************************/
         b=1;
         bloque_apuntador* bap=(bloque_apuntador*)malloc(sizeof(bloque_apuntador));
         bloque_archivo* ba=(bloque_archivo*)malloc(sizeof(bloque_archivo));
         bloque_carpeta* bc=(bloque_carpeta*)malloc(sizeof(bloque_carpeta));
         fseek(disco,sb->s_block_start,SEEK_SET);

         for(b;b<sb->s_blocks_count;b++){
            fread(bap,sizeof(bloque_apuntador),1,disco);
            fseek(disco,-sizeof(bloque_apuntador),SEEK_CUR);
            fread(ba,sizeof(bloque_archivo),1,disco);
            fseek(disco,-sizeof(bloque_archivo),SEEK_CUR);
            fread(bc,sizeof(bloque_carpeta),1,disco);

            if(strcmp(bap->esbloque,"si")==0){

            //if(as>0){fprintf(r2,"i%d->i%d;",guarda,b);}
                guarda=b;
                as++;
                fprintf(r2,"b%d[color =yellow label=\"BLOQUE APUNTADOR %d|%d, %d, %d, %d, %d, \\n %d, %d, %d, %d, %d,\\n  %d, %d, %d, %d, %d\"];",bap->num_bloque,bap->num_bloque,bap->b_pointers[0],bap->b_pointers[1],bap->b_pointers[2],bap->b_pointers[3],bap->b_pointers[4],bap->b_pointers[5],bap->b_pointers[6],bap->b_pointers[7],bap->b_pointers[8],bap->b_pointers[9],bap->b_pointers[10],bap->b_pointers[11],bap->b_pointers[12],bap->b_pointers[13],bap->b_pointers[14]);


            }
            if(strcmp(ba->esbloque,"si")==0){
           // if(as>0){fprintf(r2,"i%d->i%d;",guarda,b);}
                guarda=b;
                as++;
                fprintf(r2,"b%d[color=green label=\"BLOQUE ARCHIVO %d| %s\"];",ba->num_bloque,ba->num_bloque,ba->b_content);

            }
            if(strcmp(bc->esbloque,"si")==0){
                int asdasf =bc->num_bloque;




                int ko=bc->b_content[2].b_nodo;
                ko=ko;

                asdasf = asdasf;
           // if(as>0){fprintf(r2,"i%d->i%d;",guarda,b);}
                guarda=b;
                as++;
                fprintf(r2,"b%d[color=orange label=\"BLOQUE CARPETA %d|<f%d> %s----%d |<f%d> %s----%d|<f%d> %s----%d|<f%d> %s----%d\"];",bc->num_bloque,bc->num_bloque,bc->b_content[0].b_nodo,bc->b_content[0].b_name,bc->b_content[0].b_nodo,bc->b_content[1].b_nodo,bc->b_content[1].b_name,bc->b_content[1].b_nodo,bc->b_content[2].b_nodo,bc->b_content[2].b_name,bc->b_content[2].b_nodo,bc->b_content[3].b_nodo,bc->b_content[3].b_name,bc->b_content[3].b_nodo);
                int y=0;

                for(y;y<4;y++){
                   if(bc->b_content[y].b_nodo!=-1 &&bc->b_content[y].b_nodo!=0 && strcmp(bc->b_content[y].b_name,".")!=0&& strcmp(bc->b_content[y].b_name,"..")!=0  ){
                       char mria[200] ;
                       strcpy(mria,bc->b_content[y].b_name);





                       strcpy(mria,mria);
                      int jua= bc->b_content[y].b_nodo;
                      jua=jua;
                       fprintf(r2,"\"b%d\":f%d->\"i%d\":a98;",bc->num_bloque,bc->b_content[y].b_nodo,bc->b_content[y].b_nodo);
                   }

                }


            }


        }


         /***********************************************************************************/

         fprintf(r2,"}}");


    }
    else if(strcmp(name1,"bm_inode")==0){

        FILE*bm_inode=fopen(dpth3,"w");
        fseek(bm_inode,0,SEEK_SET);
        fseek(disco,sb->s_bm_inode_start,SEEK_SET);
        char bits[20]="";
        int f=0;
        int diez=1;
        for(f;f<sb->s_inodes_count;f++){
            fread(bits,sizeof(char),1,disco);
            fprintf(bm_inode,"%s",bits);
            strcpy(bits,"");
            if(diez==10){
                fprintf(bm_inode,"\n");
                diez=1;
            }
            diez++;

        }

        fclose(bm_inode);
        //fclose(disco);
        //fclose(r2);
        //return -1;
    }
    else if(strcmp(name1,"bm_block")==0){

        FILE*bm_inode=fopen(dpth3,"w");
        fseek(bm_inode,0,SEEK_SET);
        fseek(disco,sb->s_bm_block_start,SEEK_SET);
        char bits[20]="";
        int f=0;
        int diez=1;
        for(f;f<sb->s_blocks_count;f++){
            fread(bits,sizeof(char),1,disco);
            fprintf(bm_inode,"%s",bits);
            strcpy(bits,"");
            if(diez==10){
                fprintf(bm_inode,"\n");
                diez=1;
            }
            diez++;

        }

        fclose(bm_inode);
       // fclose(disco);
       // fclose(r2);
        //return -1;
    }
    else if(strcmp(name1,"journaling")==0){
        FILE*jou=fopen(dpth3,"w");
        nodojournal*nj=(nodojournal*)malloc(sizeof(nodojournal));
        nj=lj->primero;
        int t=lj->tam;
        char kl[30];
        //strcpy(kl,nj->disco);
        while (t>0) {
            if(strcmp(nj->disco,ide)==0){
                fprintf(jou,"Tipo operacion: %s \n",nj->journal_tipo_operacion);
                fprintf(jou,"Tipo: %d \n",nj->jornal_tipo);
                fprintf(jou,"Nombre: %s \n",nj->jornal_nombre);
                fprintf(jou,"Contenido: %s \n",nj->journal_contenido);
                fprintf(jou,"Fecha: %s \n",nj->jurnal_fecha);
                fprintf(jou,"Propietario: %s \n",nj->journal_propietario);
                fprintf(jou,"permisos: %s \n\n\n",nj->journal_permisos);
            }
            nj=nj->siguiente;
            //strcpy(kl,nj->disco);
            t--;
        }


        fclose(jou);

    }


    fclose(r2);


    //EJECUTANDO DOT
    char ja[80] = "dot -Tjpg r2.dot -o ";
         strcat(ja,"\"");
         strcat(ja,dpth3);
         strcat(ja,"\"");
         system(ja);

         printf("\n %s\nReporte creado con exito.\n",ja);

     /*******************/
         vaciarlistadereportes();
         vaciarlisebr();
         char ja1[100]="firefox ";
         strcat(ja1,"\"");
        strcat(ja1,dpth3);
        strcat(ja1,"\"");
        system(ja1);


    fclose(disco);

}

int mkfile(char*token){

    int op =0;
    int p=0;
    char id[10];
    char path[200];
    char path_disc [200];
    char nombre_part[30];
    int inicio_part;
    int size=0;
    char cont[200]="";


    while(token !=NULL){
        //printf("\nentro a while\n");
        token=strtok(NULL," :");
        if(token==NULL) break;


        if(strstr(token,"-path"))   op=1;
        if(strstr(token,"-id"))     op=2;
        if(strstr(token,"+p"))      op=3;
        if(strstr(token,"+size"))   op=4;
        if(strstr(token,"+cont"))   op=5;

        //printf("\nVAL TOKEN: %s , %d\n",token,op);

     switch(op){
     case 5:
         token=strtok(NULL,":\"\n");
         strcpy(cont,"/");
         char *tok;
         if(strstr(token,"\"")){

             strcpy(cont,tok);
         }else{
             strcpy(cont,token);
         }
         FILE *arch=fopen(cont,"rb+");
         if(arch){}else{printf("\nARCHIVO A ESCRIBIR NO EXISTE\n"); return -1;}
         fclose(arch);




         break;
        case 1:
            token=strtok(NULL,":\"\n");
            strcpy(path,"/");
            //char *tok;
            if(strstr(token,"\"")){

                strcpy(path,tok);
            }else{
                strcpy(path,token);
            }




            break;
        case 2:

            token=strtok(NULL," :");
            strcpy(id,token);
            registro* aux =(registro *)malloc(sizeof(registro));
            registro* aux1 =(registro *)malloc(sizeof(registro));
            registro* aux2 =(registro *)malloc(sizeof(registro));
            aux=re->primero;
            int n=re->tam;
            int encontrado=0;
            while(n>=1){

                if(strcmp(id,aux->id)==0){
                    encontrado=1;
                    break;
                }
                aux=aux->siguiente;
                n--;
            }
            strcpy(path_disc,aux->path);
            inicio_part=aux->start;
            strcpy(nombre_part,aux->name);

            if(encontrado!=1){
                printf("\nNo existe una particion montada con ese id\n");
                return -1;
            }


            break;

        case 3:
            if(strstr(comprobarcadena,"+p")){p=1;}

            break;
        case 4:
         token=strtok(NULL," :");
         size=atoi(token);

         break;

        default:
            printf("\ncomandos invalidos\n");
            return -1;
            break;
        }


    }


    FILE *disco =fopen(path_disc,"rb+");
    fseek(disco,0,SEEK_SET);
    mbr *mbraux=(mbr*)malloc(sizeof(mbr));
    fseek(disco,0,SEEK_SET);
    fread(mbraux,sizeof(mbr),1,disco);
    if(strcmp(nombre_part,mbraux->mbr_partition1.part_name)==0){inicio_part=mbraux->mbr_partition1.part_start;}
    else if(strcmp(nombre_part,mbraux->mbr_partition2.part_name)==0){inicio_part=mbraux->mbr_partition2.part_start;}
    else if(strcmp(nombre_part,mbraux->mbr_partition3.part_name)==0){inicio_part=mbraux->mbr_partition3.part_start;}
    else if(strcmp(nombre_part,mbraux->mbr_partition4.part_name)==0){inicio_part=mbraux->mbr_partition4.part_start;}
    else{

        if(strcmp("e",mbraux->mbr_partition1.part_type)==0){inicio_part=get_inicio_logica(nombre_part,path_disc,mbraux->mbr_partition1);}
        else if(strcmp("e",mbraux->mbr_partition2.part_type)==0){inicio_part=get_inicio_logica(nombre_part,path_disc,mbraux->mbr_partition2);}
        else if(strcmp("e",mbraux->mbr_partition3.part_type)==0){inicio_part=get_inicio_logica(nombre_part,path_disc,mbraux->mbr_partition3);}
        else if(strcmp("e",mbraux->mbr_partition4.part_type)==0){inicio_part=get_inicio_logica(nombre_part,path_disc,mbraux->mbr_partition4);}

    }

    super_bloque *sb=(super_bloque*)malloc(sizeof(super_bloque));
    fseek(disco,inicio_part,SEEK_SET);
    fread(sb,sizeof(super_bloque),1,disco);
    char pathw[200];
    strcpy(pathw,path);






    mkdir1(NULL,id,path,p,1,3);

   FILE *co=fopen(cont,"r");

    char nombre_carpeta[100]="";
    nombre_ca(&nombre_carpeta,pathw);
    if(co){
        if(in==5){
            printf("%d",in);

        }
       fseek(co,0,SEEK_END);
       int final=ftell(co);
       char oc[65]="";
       fseek(co,0,SEEK_SET);
       int total=1;
       inodo*ino=(inodo*)malloc(sizeof(inodo));

       fseek(disco,sb->s_inode_start,SEEK_SET);
       int ma=1;
       for(ma;ma<sb->s_inodes_count;ma++){

            fread(ino,sizeof(inodo),1,disco);
            if(ino->num_inodo==in){
                break;}
       }
        int me=1;
        bloque_archivo*ba=(bloque_archivo*)malloc(sizeof(bloque_archivo));
        bloque_carpeta*blaux2=(bloque_carpeta*)malloc(sizeof(bloque_carpeta));
       while(1){
            fread(oc,sizeof(char),64,co);
            me=1;
            fseek(disco,sb->s_block_start,SEEK_SET);
            for(me;me<sb->s_blocks_count;me++){
                fread(blaux2,sizeof(bloque_carpeta),1,disco);
                fseek(disco,-sizeof(bloque_carpeta),disco);
                fread(ba,sizeof(bloque_archivo),1,disco);
                char aul[39];




                if(strcmp(ba->esbloque,"si")!=0 && strcmp(blaux2->esbloque,"si")!=0 ){
                    fseek(disco,-sizeof(bloque_archivo),SEEK_CUR);
                    int bu=usa_bloque(sb,sb->s_bm_block_start,path_disc,inicio_part);
                    ba->num_bloque=bu;
                    strcpy(ba->b_content,oc);
                    strcpy(ba->esbloque,"si");
                    fwrite(ba,sizeof(bloque_archivo),1,disco);
                    int yu=0;
                    for(yu;yu<15;yu++){
                        if(ino->i_block[yu]==-1){
                            ino->i_block[yu]=bu;
                            break;
                        }
                    }

                    break;
                }


            }

            if(final<=ftell(co)){break;}
            total++;
            if(total==16){
            printf("\nYa No cabe mas informacione en archivo\n");
            break;
             }
       }
      fclose(co);

             ma=1;
             inodo*inoq=(inodo*)malloc(sizeof(inodo));

             fseek(disco,sb->s_inode_start,SEEK_SET);
             for(ma;ma<sb->s_inodes_count;ma++){

                  fread(inoq,sizeof(inodo),1,disco);
                  if(inoq->num_inodo==in){
                     fseek(disco,-sizeof(inodo),SEEK_CUR) ;
                     fwrite(ino,sizeof(inodo),1,disco);
                      break;
                  }
             }
    }
    fclose(disco);


     return 0;
}
int cat(char *token){

    int op =0;
    int p=0;
    char id[10];
    char path[200];
    char path_disc [200];
    char nombre_part[30];
    int inicio_part;





    FILE *bup=fopen("bup.txt","r");
    char back[200];
    fread(back,sizeof(char),200,bup);
    fclose(bup);
    char *yu =strtok(back," ");
    /******************************************/
    while(yu !=NULL){
        //printf("\nentro a while\n");
        yu=strtok(NULL," :");
        if(yu==NULL) break;

        if(strstr(yu,"-filen")){   op=1;}
        else if(strstr(yu,"-id")){     op=2;}
        else{op=-1;}


        //printf("\nVAL TOKEN: %s , %d\n",token,op);

     switch(op){

        case 1:
         yu=strtok(NULL,":\"\n");
            break;
        case 2:

            yu=strtok(NULL," :");
            strcpy(id,yu);
            registro* aux =(registro *)malloc(sizeof(registro));
            registro* aux1 =(registro *)malloc(sizeof(registro));
            registro* aux2 =(registro *)malloc(sizeof(registro));
            aux=re->primero;
            int n=re->tam;
            int encontrado=0;
            while(n>=1){

                if(strcmp(id,aux->id)==0){
                    encontrado=1;
                    break;
                }
                aux=aux->siguiente;
                n--;
            }
            strcpy(path_disc,aux->path);
            inicio_part=aux->start;
            strcpy(nombre_part,aux->name);

            if(encontrado!=1){
                printf("\nNo existe una particion montada con ese id\n");
                return -1;
            }


            break;

        default:

            break;
        }


    }










    /*IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIID*/
    FILE *bup1=fopen("bup2.txt","r");
    char back1[200];
    fread(back1,sizeof(char),200,bup1);
    fclose(bup1);

    char *ya=strtok(back1," ");



FILE *disco =fopen(path_disc,"rb+");
FILE *disco1 =fopen(path_disc,"rb+");
int jey;
char pr1[50]="";
char pr2[50]="";
char pr3[50]="";
char pr4[50]="";
char pr5[50]="";
char pr6[50]="";
char pr7[50]="";
char pr8[50]="";
char pr9[50]="";
char pr10[50]="";
char pr11[50]="";
char pr12[50]="";
char pr13[50]="";
char pr14[50]="";
int contar=1;
    while(ya !=NULL){
        //printf("\nentro a while\n");
        ya=strtok(NULL," :-");
        if(ya==NULL) break;


        if(strstr(ya,"filen")){   op=1;}
        else if(strstr(ya,"-id")){     op=2;}
        else{op=-1;}

        //printf("\nVAL TOKEN: %s , %d\n",token,op);

     switch(op){

        case 1:
            ya=strtok(NULL,":\"\n");
            strcpy(path,"/");
            char *tok;
            if(strstr(ya,"\"")){

                strcpy(path,tok);
            }else{
                strcpy(path,ya);
            }
            if(contar==1){strcpy(pr1,path);}
            if(contar==2){strcpy(pr2,path);}
            if(contar==3){strcpy(pr3,path);}
            if(contar==4){strcpy(pr4,path);}
            if(contar==5){strcpy(pr5,path);}
            if(contar==6){strcpy(pr6,path);}
            if(contar==7){strcpy(pr7,path);}
            if(contar==8){strcpy(pr8,path);}
            if(contar==9){strcpy(pr9,path);}
            if(contar==10){strcpy(pr10,path);}
            if(contar==11){strcpy(pr11,path);}
            if(contar==12){strcpy(pr12,path);}
            if(contar==13){strcpy(pr13,path);}
            if(contar==14){strcpy(pr14,path);}
            if (contar<14){
            contar++;
            }




            break;
        case 2:
             ya=strtok(NULL," :");

            break;

        default:

            break;
        }


    }
    printf("\npath= %s path_disc: %s  id:%s\n",path,path_disc,id);
    int cul=1;
while(cul<contar){

        if(cul==1){strcpy(path,pr1);}
        if(cul==2){strcpy(path,pr2);}
        if(cul==3){strcpy(path,pr3);}
        if(cul==4){strcpy(path,pr4);}
        if(cul==5){strcpy(path,pr5);}
        if(cul==6){strcpy(path,pr6);}
        if(cul==7){strcpy(path,pr7);}
        if(cul==8){strcpy(path,pr8);}
        if(cul==9){strcpy(path,pr9);}
        if(cul==10){strcpy(path,pr10);}
        if(cul==11){strcpy(path,pr11);}
        if(cul==12){strcpy(path,pr12);}
        if(cul==13){strcpy(path,pr13);}
        if(cul==14){strcpy(path,pr14);}

cul++;


        fseek(disco,0,SEEK_SET);
        mbr *mbraux=(mbr*)malloc(sizeof(mbr));
        fseek(disco,0,SEEK_SET);
        fread(mbraux,sizeof(mbr),1,disco);
        if(strcmp(nombre_part,mbraux->mbr_partition1.part_name)==0){inicio_part=mbraux->mbr_partition1.part_start;}
        else if(strcmp(nombre_part,mbraux->mbr_partition2.part_name)==0){inicio_part=mbraux->mbr_partition2.part_start;}
        else if(strcmp(nombre_part,mbraux->mbr_partition3.part_name)==0){inicio_part=mbraux->mbr_partition3.part_start;}
        else if(strcmp(nombre_part,mbraux->mbr_partition4.part_name)==0){inicio_part=mbraux->mbr_partition4.part_start;}
        else{

        if(strcmp("e",mbraux->mbr_partition1.part_type)==0){inicio_part=get_inicio_logica(nombre_part,path_disc,mbraux->mbr_partition1);}
        else if(strcmp("e",mbraux->mbr_partition2.part_type)==0){inicio_part=get_inicio_logica(nombre_part,path_disc,mbraux->mbr_partition2);}
        else if(strcmp("e",mbraux->mbr_partition3.part_type)==0){inicio_part=get_inicio_logica(nombre_part,path_disc,mbraux->mbr_partition3);}
        else if(strcmp("e",mbraux->mbr_partition4.part_type)==0){inicio_part=get_inicio_logica(nombre_part,path_disc,mbraux->mbr_partition4);}

        }

        super_bloque *sb=(super_bloque*)malloc(sizeof(super_bloque));
        fseek(disco,inicio_part,SEEK_SET);
        fread(sb,sizeof(super_bloque),1,disco);
        int itb=sb->s_block_start;
        int bcc=sb->s_blocks_count;
        int iti=sb->s_inode_start;
        int ic=sb->s_inodes_count;
        inodo*ino=(inodo*)malloc(sizeof(inodo));
        bloque_archivo*ba=(bloque_archivo*)malloc(sizeof(bloque_archivo));
        bloque_carpeta*bc=(bloque_carpeta*)malloc(sizeof(bloque_carpeta));

       fseek(disco,iti,SEEK_SET);
        int f=1;
        int buscar=0;
        char nombre[20];
        char path_b[200]="";
        char pathw[200]="";
        char nombre_carpeta[20]="";
        strcpy(path_b,path);
        strcpy(pathw,path);
        int po=1;
        int num_pal=numero_path2(pathw);
        for(f;f<ic;f++){
            fread(ino,sizeof(inodo),1,disco);
            if(ino->num_inodo==buscar){
                if(num_pal==0){break;}
                strcpy(path_b,path);
                nombre_ar(&nombre_carpeta,path_b,po);
                num_pal--;
                po++;

                int y=0;
                for(y;y<15;y++){

                    if(ino->i_block[y]!=-1){
                        int nmb=ino->i_block[y];
                        fseek(disco1,itb,SEEK_SET);
                        int x=1;
                        for(x;x<bcc;x++){
                            fread(bc,sizeof(bloque_carpeta),1,disco1);
                            if(nmb==bc->num_bloque){
                                int z=0;
                                for(z;z<4;z++){
                                    if(strcmp(bc->b_content[z].b_name,nombre_carpeta)==0){
                                           buscar=bc->b_content[z].b_nodo;
                                    }

                                }
                            }

                        }

                    }

                }


            }

        }
        int g=0;
        printf("\nDATOS ARCHIVO:\n");
        int blq=-1;
        for(g;g<15;g++){
            if(ino->i_block[g]!=-1){
                blq=ino->i_block[g];
                fseek(disco,itb,SEEK_SET);
                int j=1;
                for(j;j<bcc;j++){
                    fread(ba,sizeof(bloque_archivo),1,disco);
                    if(strcmp(ba->esbloque,"si")==0 &&ba->num_bloque==blq){
                        printf("%s ",ba->b_content);
                    }
                }
            }

        }
        printf("\n");

}
    fclose(disco1);
    fclose(disco);

}









/******LIB******/
int libera_bloque(super_bloque *superB, int i_bm_b, char path[200], int i_sis, int libera){


    FILE *sistema =fopen(path,"rb+");
    int bandera=0;//bandera.
    int i=0;//inicio for
    char aux[2]="";//char donde guardate los 1 o ceros que leere
    int num_blocks=superB->s_blocks_count;//asigo el numero de bloques que hay
    int num_bloque_usado;

    fseek(sistema,i_bm_b,SEEK_SET);//posiciono el puntero al inicio del bitmap de bloques
   // printf("\n");
    for(i;i<=num_blocks;i++){


        fread(aux,sizeof(char),1,sistema);

        if(i==libera){
            num_bloque_usado=i;
            fseek(sistema,-(sizeof(char)),SEEK_CUR);
            char ocupado[1]="0";
            fwrite(ocupado,sizeof(char[1]),1,sistema);
            bandera=1;
            strcpy(aux,"");
            superB->s_free_blocks_count=superB->s_free_blocks_count+1;
           fseek(sistema,i_sis,SEEK_SET);
           fwrite(superB,sizeof(super_bloque),1,sistema);
           break;
        }

    }

    fclose(sistema);//cierro el archivo sistema
    return num_bloque_usado;//retorno el numero de bloque que utilizare

}
int libera_inodos(super_bloque *superB, int inicio_inodos, char path[200],int i_sis,int libera){


    FILE *sistema =fopen(path,"rb+");

    int i =0;//variable de for
    int bandera=0;//variable que indica el primer inodo libre en el bitmap
    char aux[2]="";//variable para almacenar ell caracter
    int num_inodos=superB->s_inodes_count;
    fseek(sistema,inicio_inodos,SEEK_CUR);//posiciono el puntero al inicio del bitmap de inodos
    int inodo_usado;
    for(i;i<=num_inodos;i++){

    fread(aux,sizeof(char),1,sistema);

    if(i==libera){

        fseek(sistema,-(sizeof(char)),SEEK_CUR);
        inodo_usado=i;
        char ocupado[1]="0";
        fwrite(ocupado,sizeof(char),1,sistema);
        bandera=1;
        strcpy(aux,"");
        superB->s_free_inodes_count=superB->s_free_inodes_count+1;
        fseek(sistema,i_sis,SEEK_SET);
        fwrite(superB,sizeof(super_bloque),1,sistema);
        break;
    }


    }

    fclose(sistema);
    return inodo_usado;

}


int rm(char *token){
    char *tokaux=token;
    int op =0;
    int p=0;
    char id[10];
    char path[200];
    char path_disc [200];
    char nombre_part[30];
    int inicio_part;
    int rf;

    while(token !=NULL){
        //printf("\nentro a while\n");
        token=strtok(NULL," :");
        if(token==NULL) break;


        if(strstr(token,"-path"))   op=1;
        if(strstr(token,"-id"))     op=2;
        if(strstr(token,"+rf"))     op=3;

        //printf("\nVAL TOKEN: %s , %d\n",token,op);

     switch(op){

        case 1:
            token=strtok(NULL,":\"\n");
            strcpy(path,"/");
            char *tok;
            if(strstr(token,"\"")){

                strcpy(path,tok);
            }else{
                strcpy(path,token);
            }




            break;
        case 2:

            token=strtok(NULL," :");
            strcpy(id,token);
            registro* aux =(registro *)malloc(sizeof(registro));
            registro* aux1 =(registro *)malloc(sizeof(registro));
            registro* aux2 =(registro *)malloc(sizeof(registro));
            aux=re->primero;
            int n=re->tam;
            int encontrado=0;
            while(n>=1){

                if(strcmp(id,aux->id)==0){
                    encontrado=1;
                    break;
                }
                aux=aux->siguiente;
                n--;
            }
            strcpy(path_disc,aux->path);
            inicio_part=aux->start;
            strcpy(nombre_part,aux->name);

            if(encontrado!=1){
                printf("\nNo existe una particion montada con ese id\n");
                return -1;
            }


            break;
     case 3:
         rf=1;

         break;

        default:
            printf("\ncomandos invalidos\n");
            return -1;
            break;
        }


    }

printf("\npath= %s path_disc: %s  id:%s\n",path,path_disc,id);

    FILE *disco =fopen(path_disc,"rb+");
    fseek(disco,0,SEEK_SET);
    mbr *mbraux=(mbr*)malloc(sizeof(mbr));
    fseek(disco,0,SEEK_SET);
    fread(mbraux,sizeof(mbr),1,disco);
    if(strcmp(nombre_part,mbraux->mbr_partition1.part_name)==0){inicio_part=mbraux->mbr_partition1.part_start;}
    else if(strcmp(nombre_part,mbraux->mbr_partition2.part_name)==0){inicio_part=mbraux->mbr_partition2.part_start;}
    else if(strcmp(nombre_part,mbraux->mbr_partition3.part_name)==0){inicio_part=mbraux->mbr_partition3.part_start;}
    else if(strcmp(nombre_part,mbraux->mbr_partition4.part_name)==0){inicio_part=mbraux->mbr_partition4.part_start;}
    else{

    if(strcmp("e",mbraux->mbr_partition1.part_type)==0){inicio_part=get_inicio_logica(nombre_part,path_disc,mbraux->mbr_partition1);}
    else if(strcmp("e",mbraux->mbr_partition2.part_type)==0){inicio_part=get_inicio_logica(nombre_part,path_disc,mbraux->mbr_partition2);}
    else if(strcmp("e",mbraux->mbr_partition3.part_type)==0){inicio_part=get_inicio_logica(nombre_part,path_disc,mbraux->mbr_partition3);}
    else if(strcmp("e",mbraux->mbr_partition4.part_type)==0){inicio_part=get_inicio_logica(nombre_part,path_disc,mbraux->mbr_partition4);}

    }

    super_bloque *sb=(super_bloque*)malloc(sizeof(super_bloque));
    fseek(disco,inicio_part,SEEK_SET);
    fread(sb,sizeof(super_bloque),1,disco);
    int itb=sb->s_block_start;
    int bcc=sb->s_blocks_count;
    int iti=sb->s_inode_start;
    int ic=sb->s_inodes_count;
    inodo*ino=(inodo*)malloc(sizeof(inodo));
    bloque_archivo*ba=(bloque_archivo*)malloc(sizeof(bloque_archivo));
    bloque_carpeta*bc=(bloque_carpeta*)malloc(sizeof(bloque_carpeta));
    FILE *disco1 =fopen(path_disc,"rb+");
    fseek(disco,iti,SEEK_SET);
    int f=1;
    int buscar=0;
    char nombre[20];
    char path_b[200]="";
    char pathw[200]="";
    char nombre_carpeta[20]="";
    strcpy(path_b,path);
    strcpy(pathw,path);
    int po=1;
    int num_pal=numero_path(pathw);
    int lugar=0;
    for(f;f<ic;f++){
        fread(ino,sizeof(inodo),1,disco);
        if(ino->num_inodo==buscar){
            if(num_pal==0){break;}
            strcpy(path_b,path);
            nombre_ar(&nombre_carpeta,path_b,po);

            po++;

            int y=0;

            for(y;y<15;y++){

                if(ino->i_block[y]!=-1){
                    int nmb=ino->i_block[y];
                    fseek(disco1,itb,SEEK_SET);
                    int x=1;
                    for(x;x<bcc;x++){
                        fread(bc,sizeof(bloque_carpeta),1,disco1);
                        if(nmb==bc->num_bloque){
                            int z=0;
                            for(z;z<4;z++){
                                if(strcmp(bc->b_content[z].b_name,nombre_carpeta)==0){
                                       buscar=bc->b_content[z].b_nodo;
                                       if(num_pal==1){
                                            bc->b_content[z].b_nodo=-1;
                                            strcpy(bc->b_content[z].b_name,"");
                                            fseek(disco1,-sizeof(bloque_carpeta),SEEK_CUR);
                                            fwrite(bc,sizeof(bloque_carpeta),1,disco1);
                                       }
                                       //break;
                                }

                            }
                           // break;
                        }

                    }
                   //break;
                }

            }
            num_pal--;

        }

    }

    bloque_carpeta*bb=(bloque_carpeta*)malloc(sizeof(bloque_carpeta));



    fclose(disco1);
    int g=0;
   // printf("\nDATOS ARCHIVO:\n");
 int tipo =ino->num_inodo;
 char culo[23];
 char culo2[23];
 strcpy(culo,ino->i_mtime);
 strcpy(culo2,ino->i_ctime);

 if(sb->ext3==1){
     if(ino->i_type==1){
     addjournal(comprobarcadena,id,path,"Eliminar carpeta",1);

     }else{
         addjournal(comprobarcadena,id,path,"Eliminar archivo",0);
     }
 }


 eliminar(ino,itb,iti,path_disc,inicio_part,bcc,ic,sb);

    fclose(disco);
}

void eliminar(inodo*ino,int itb,int iti,char path[200],int isis, int nb, int ni,super_bloque* sb){
   int tipo =ino->i_type;
    int m=0;
    FILE*disco=fopen(path,"rb+");
    FILE*di=fopen(path,"rb+");
    bloque_carpeta*bc=(bloque_carpeta*)malloc(sizeof(bloque_carpeta));
    bloque_archivo*ba=(bloque_archivo*)malloc(sizeof(bloque_archivo));
    inodo* i=(inodo*)malloc(sizeof(inodo));
    if(ino->i_type==1){
        for(m;m<15;m++){//escaneo bloques de inodo
            if(ino->i_block[m]!=-1){
                fseek(disco,itb,SEEK_SET);
                int z=1;
                for(z;z<nb;z++){//busco bloque
                    fread(bc,sizeof(bloque_carpeta),1,disco);
                    if(bc->num_bloque==ino->i_block[m]){
                        int y=0;
                        for(y;y<4;y++){//escaneo bloques
                            if(bc->b_content[y].b_nodo!=-1 && strcmp(bc->b_content[y].b_name,".")!=0 && strcmp(bc->b_content[y].b_name,"..")!=0){
                                fseek(di,iti,SEEK_SET) ;
                                int w=1;
                                for(w;w<ni;w++){//numero de inodos
                                    fread(i,sizeof(inodo),1,di);
                                    if(i->num_inodo==bc->b_content[y].b_nodo){
                                        eliminar(i,itb,iti,path,isis,nb,ni,sb);
                                        fseek(di,-sizeof(inodo),SEEK_CUR);
                                        libera_inodos(sb,sb->s_bm_inode_start,path,isis,i->num_inodo);
                                        i->num_inodo=8888888;
                                        fwrite(i,sizeof(inodo),1,di);

                                    }
                                }
                            }

                        }
                        fseek(disco,-sizeof(bloque_carpeta),SEEK_CUR);
                        libera_bloque(sb,sb->s_bm_block_start,path,isis,bc->num_bloque);
                        bc->num_bloque=8888888;
                        strcpy(bc->esbloque,"NO");
                        fwrite(bc,sizeof(bloque_carpeta),1,disco);

                    }

                }


            }

        }
        fseek(di,iti,SEEK_SET) ;
        int w=1;
        for(w;w<ni;w++){//numero de inodos
            fread(i,sizeof(inodo),1,di);
            if(i->num_inodo==ino->num_inodo){
                fseek(di,-sizeof(inodo),SEEK_CUR);
                libera_inodos(sb,sb->s_bm_inode_start,path,isis,i->num_inodo);
                i->num_inodo=8888888;
                fwrite(i,sizeof(inodo),1,di);

            }
        }
    }else{
        int blq=-1;
        int g=0;
    for(g;g<15;g++){
        if(ino->i_block[g]!=-1){
            blq=ino->i_block[g];
            fseek(disco,itb,SEEK_SET);
            int j=1;
            for(j;j<nb;j++){
                fread(ba,sizeof(bloque_archivo),1,disco);
                if(strcmp(ba->esbloque,"si")==0 &&ba->num_bloque==blq){
                    libera_bloque(sb,sb->s_bm_block_start,path,isis,ba->num_bloque);
                    strcpy(ba->b_content,"");
                    strcpy(ba->esbloque,"NO");
                    ba->num_bloque=8631312;
                    fseek(disco,-sizeof(bloque_archivo),SEEK_CUR);
                    fwrite(ba,sizeof(bloque_archivo),1,disco);

                }
            }
        }

    }


    int z=0;

    z=1;
    fseek(disco,iti,SEEK_SET);
    int c=ino->num_inodo;
    for(z;z<ni;z++){
        fread(ino,sizeof(inodo),1,disco);

        if(ino->num_inodo==c){
            libera_inodos(sb,sb->s_bm_inode_start,path,isis,c);
            z=0;
                for(z;z<15;z++){
                    ino->i_block[z]=-1;
                }
                ino->num_inodo=8888888;
                fseek(disco,-sizeof(inodo),SEEK_CUR);
                fwrite(ino,sizeof(inodo),1,disco);

            break;
        }

    }

    }
    /****/



    fclose(disco);
    fclose(di);

}


int ln(char *token){
    char *tokaux=token;
    int op =0;
    int p=0;
    char id[10];
    char path[200];
    char path_disc [200];
    char nombre_part[30];
    int inicio_part;
    int rf;

    while(token !=NULL){
        //printf("\nentro a while\n");
        token=strtok(NULL," :");
        if(token==NULL) break;


        if(strstr(token,"-path"))   op=1;
        if(strstr(token,"-id"))     op=2;
        if(strstr(token,"+rf"))     op=3;

        //printf("\nVAL TOKEN: %s , %d\n",token,op);

     switch(op){

        case 1:
            token=strtok(NULL,":\"\n");
            strcpy(path,"/");
            char *tok;
            if(strstr(token,"\"")){

                strcpy(path,tok);
            }else{
                strcpy(path,token);
            }




            break;
        case 2:

            token=strtok(NULL," :");
            strcpy(id,token);
            registro* aux =(registro *)malloc(sizeof(registro));
            registro* aux1 =(registro *)malloc(sizeof(registro));
            registro* aux2 =(registro *)malloc(sizeof(registro));
            aux=re->primero;
            int n=re->tam;
            int encontrado=0;
            while(n>=1){

                if(strcmp(id,aux->id)==0){
                    encontrado=1;
                    break;
                }
                aux=aux->siguiente;
                n--;
            }
            strcpy(path_disc,aux->path);
            inicio_part=aux->start;
            strcpy(nombre_part,aux->name);

            if(encontrado!=1){
                printf("\nNo existe una particion montada con ese id\n");
                return -1;
            }


            break;
     case 3:
         rf=1;

         break;

        default:
            printf("\ncomandos invalidos\n");
            return -1;
            break;
        }


    }

printf("\npath= %s path_disc: %s  id:%s\n",path,path_disc,id);

    FILE *disco =fopen(path_disc,"rb+");
    fseek(disco,0,SEEK_SET);
    mbr *mbraux=(mbr*)malloc(sizeof(mbr));
    fseek(disco,0,SEEK_SET);
    fread(mbraux,sizeof(mbr),1,disco);
    if(strcmp(nombre_part,mbraux->mbr_partition1.part_name)==0){inicio_part=mbraux->mbr_partition1.part_start;}
    else if(strcmp(nombre_part,mbraux->mbr_partition2.part_name)==0){inicio_part=mbraux->mbr_partition2.part_start;}
    else if(strcmp(nombre_part,mbraux->mbr_partition3.part_name)==0){inicio_part=mbraux->mbr_partition3.part_start;}
    else if(strcmp(nombre_part,mbraux->mbr_partition4.part_name)==0){inicio_part=mbraux->mbr_partition4.part_start;}
    else{

    if(strcmp("e",mbraux->mbr_partition1.part_type)==0){inicio_part=get_inicio_logica(nombre_part,path_disc,mbraux->mbr_partition1);}
    else if(strcmp("e",mbraux->mbr_partition2.part_type)==0){inicio_part=get_inicio_logica(nombre_part,path_disc,mbraux->mbr_partition2);}
    else if(strcmp("e",mbraux->mbr_partition3.part_type)==0){inicio_part=get_inicio_logica(nombre_part,path_disc,mbraux->mbr_partition3);}
    else if(strcmp("e",mbraux->mbr_partition4.part_type)==0){inicio_part=get_inicio_logica(nombre_part,path_disc,mbraux->mbr_partition4);}

    }

    super_bloque *sb=(super_bloque*)malloc(sizeof(super_bloque));
    fseek(disco,inicio_part,SEEK_SET);
    fread(sb,sizeof(super_bloque),1,disco);
    int itb=sb->s_block_start;
    int bcc=sb->s_blocks_count;
    int iti=sb->s_inode_start;
    int ic=sb->s_inodes_count;
    inodo*ino=(inodo*)malloc(sizeof(inodo));
    bloque_archivo*ba=(bloque_archivo*)malloc(sizeof(bloque_archivo));
    bloque_carpeta*bc=(bloque_carpeta*)malloc(sizeof(bloque_carpeta));
    FILE *disco1 =fopen(path_disc,"rb+");
    fseek(disco,iti,SEEK_SET);
    int f=1;
    int buscar=0;
    char nombre[20];
    char path_b[200]="";
    char pathw[200]="";
    char nombre_carpeta[20]="";
    strcpy(path_b,path);
    strcpy(pathw,path);
    int po=1;
    int num_pal=numero_path(pathw);
    int lugar=0;
    for(f;f<ic;f++){
        fread(ino,sizeof(inodo),1,disco);
        if(ino->num_inodo==buscar){
            if(num_pal==0){break;}
            strcpy(path_b,path);
            nombre_ar(&nombre_carpeta,path_b,po);

            po++;

            int y=0;

            for(y;y<15;y++){

                if(ino->i_block[y]!=-1){
                    int nmb=ino->i_block[y];
                    fseek(disco1,itb,SEEK_SET);
                    int x=1;
                    for(x;x<bcc;x++){
                        fread(bc,sizeof(bloque_carpeta),1,disco1);
                        if(nmb==bc->num_bloque){
                            int z=0;
                            for(z;z<4;z++){
                                if(strcmp(bc->b_content[z].b_name,nombre_carpeta)==0){
                                       buscar=bc->b_content[z].b_nodo;
                                       if(num_pal==1){
                                       bc->b_content[z].b_nodo=-1;
                                       strcpy(bc->b_content[z].b_name,"");
                                       fseek(disco1,-sizeof(bloque_carpeta),SEEK_CUR);
                                       fwrite(bc,sizeof(bloque_carpeta),1,disco1);
                                       }
                                }

                            }
                        }

                    }

                }

            }
            num_pal--;

        }

    }

    bloque_carpeta*bb=(bloque_carpeta*)malloc(sizeof(bloque_carpeta));



    fclose(disco1);
    int g=0;
    printf("\nDATOS ARCHIVO:\n");
    int blq=-1;
   // if(ino->i_type==0){
    for(g;g<15;g++){
        if(ino->i_block[g]!=-1){
            blq=ino->i_block[g];
            fseek(disco,itb,SEEK_SET);
            int j=1;
            for(j;j<bcc;j++){
                fread(ba,sizeof(bloque_archivo),1,disco);
                if(strcmp(ba->esbloque,"si")==0 &&ba->num_bloque==blq){
                    libera_bloque(sb,sb->s_bm_block_start,path_disc,inicio_part,ba->num_bloque);
                    strcpy(ba->b_content,"");
                    strcpy(ba->esbloque,"NO");
                    ba->num_bloque=8631312;
                    fseek(disco,-sizeof(bloque_archivo),SEEK_CUR);
                    fwrite(ba,sizeof(bloque_archivo),1,disco);

                }
            }
        }

    }


    int z=0;

    z=1;
    fseek(disco,iti,SEEK_SET);
    for(z;z<ic;z++){
        fread(ino,sizeof(inodo),1,disco);
        int c=ino->num_inodo;
        if(ino->num_inodo==buscar){
            libera_inodos(sb,sb->s_bm_inode_start,path_disc,inicio_part,buscar);
            z=0;
                for(z;z<15;z++){
                    ino->i_block[z]=-1;
                }
                ino->num_inodo=8888888;
                fseek(disco,-sizeof(inodo),SEEK_CUR);
                fwrite(ino,sizeof(inodo),1,disco);

            break;
        }

    }

    //}
    printf("\n");

    fclose(disco);
}

int ren(char *token){
    char *tokaux=token;
    int op =0;
    int p=0;
    char id[10];
    char path[200];
    char path_disc [200];
    char nombre_part[30];
    int inicio_part;
    char names[20];
    int rf;

    while(token !=NULL){
        //printf("\nentro a while\n");
        token=strtok(NULL," :");
        if(token==NULL) break;


        if(strstr(token,"-path"))   op=1;
        if(strstr(token,"-id"))     op=2;
        if(strstr(token,"-name"))     op=3;

        //printf("\nVAL TOKEN: %s , %d\n",token,op);

     switch(op){

        case 1:
            token=strtok(NULL,":\"\n");
            strcpy(path,"/");
            char *tok;
            if(strstr(token,"\"")){

                strcpy(path,tok);
            }else{
                strcpy(path,token);
            }




            break;
        case 2:

            token=strtok(NULL," :");
            strcpy(id,token);
            registro* aux =(registro *)malloc(sizeof(registro));
            registro* aux1 =(registro *)malloc(sizeof(registro));
            registro* aux2 =(registro *)malloc(sizeof(registro));
            aux=re->primero;
            int n=re->tam;
            int encontrado=0;
            while(n>=1){

                if(strcmp(id,aux->id)==0){
                    encontrado=1;
                    break;
                }
                aux=aux->siguiente;
                n--;
            }
            strcpy(path_disc,aux->path);
            inicio_part=aux->start;
            strcpy(nombre_part,aux->name);

            if(encontrado!=1){
                printf("\nNo existe una particion montada con ese id\n");
                return -1;
            }


            break;
     case 3:
         token=strtok(NULL,":\"\n");
         strcpy(names,"/");
         //char *tok;
         if(strstr(token,"\"")){

             strcpy(names,tok);
         }else{
             strcpy(names,token);
         }
         break;

        default:
            printf("\ncomandos invalidos\n");
            return -1;
            break;
        }


    }

printf("\npath= %s path_disc: %s  id:%s\n",path,path_disc,id);

    FILE *disco =fopen(path_disc,"rb+");
    fseek(disco,0,SEEK_SET);
    mbr *mbraux=(mbr*)malloc(sizeof(mbr));
    fseek(disco,0,SEEK_SET);
    fread(mbraux,sizeof(mbr),1,disco);
    if(strcmp(nombre_part,mbraux->mbr_partition1.part_name)==0){inicio_part=mbraux->mbr_partition1.part_start;}
    else if(strcmp(nombre_part,mbraux->mbr_partition2.part_name)==0){inicio_part=mbraux->mbr_partition2.part_start;}
    else if(strcmp(nombre_part,mbraux->mbr_partition3.part_name)==0){inicio_part=mbraux->mbr_partition3.part_start;}
    else if(strcmp(nombre_part,mbraux->mbr_partition4.part_name)==0){inicio_part=mbraux->mbr_partition4.part_start;}
    else{

    if(strcmp("e",mbraux->mbr_partition1.part_type)==0){inicio_part=get_inicio_logica(nombre_part,path_disc,mbraux->mbr_partition1);}
    else if(strcmp("e",mbraux->mbr_partition2.part_type)==0){inicio_part=get_inicio_logica(nombre_part,path_disc,mbraux->mbr_partition2);}
    else if(strcmp("e",mbraux->mbr_partition3.part_type)==0){inicio_part=get_inicio_logica(nombre_part,path_disc,mbraux->mbr_partition3);}
    else if(strcmp("e",mbraux->mbr_partition4.part_type)==0){inicio_part=get_inicio_logica(nombre_part,path_disc,mbraux->mbr_partition4);}

    }

    super_bloque *sb=(super_bloque*)malloc(sizeof(super_bloque));
    fseek(disco,inicio_part,SEEK_SET);
    fread(sb,sizeof(super_bloque),1,disco);
    if(sb->ext3==1){

            addjournal(comprobarcadena,id,path,"Renombrar",1);



    }

    int itb=sb->s_block_start;
    int bcc=sb->s_blocks_count;
    int iti=sb->s_inode_start;
    int ic=sb->s_inodes_count;
    inodo*ino=(inodo*)malloc(sizeof(inodo));
    bloque_archivo*ba=(bloque_archivo*)malloc(sizeof(bloque_archivo));
    bloque_carpeta*bc=(bloque_carpeta*)malloc(sizeof(bloque_carpeta));
    FILE *disco1 =fopen(path_disc,"rb+");

    int f=1;
    int buscar=0;
    char nombre[20];
    char path_b[200]="";
    char pathw[200]="";
    char nombre_carpeta[20]="";
    strcpy(path_b,path);
    strcpy(pathw,path);
    int po=1;
    int num_pal=numero_path(pathw);
    fseek(disco,iti,SEEK_SET);
    int lugar=0;





    for(f;f<ic;f++){
        fread(ino,sizeof(inodo),1,disco);
        int moa=ino->num_inodo;
        if(ino->num_inodo==buscar){
            if(num_pal==0){break;}
            strcpy(path_b,path);
            nombre_ar(&nombre_carpeta,path_b,po);

            po++;

            int y=0;

            for(y;y<15;y++){

                if(ino->i_block[y]!=-1){
                    int nmb=ino->i_block[y];
                    fseek(disco1,itb,SEEK_SET);
                    int x=1;
                    for(x;x<bcc;x++){
                        fread(bc,sizeof(bloque_carpeta),1,disco1);
                        if(nmb==bc->num_bloque){
                            int z=0;
                            for(z;z<4;z++){
                                if(strcmp(bc->b_content[z].b_name,nombre_carpeta)==0){

                                    buscar=bc->b_content[z].b_nodo;

                                       if(num_pal==1){

                                       strcpy(bc->b_content[z].b_name,names);
                                       fseek(disco1,-sizeof(bloque_carpeta),SEEK_CUR);
                                       fwrite(bc,sizeof(bloque_carpeta),1,disco1);
                                     //  break;
                                       }
                                }

                            }
                       // break;
                        }

                    }
                 //   break;
                }

            }
            num_pal--;

        }

    }






    fclose(disco1);

    fclose(disco);
}

int tune2f(char *token){

    int op =0;
    char id[10];
    char path[200];
    char path_disc [200];
    char nombre_part[30];
    int inicio_part;


    while(token !=NULL){

        token=strtok(NULL," :");
        if(token==NULL) break;
        if(strstr(token,"-id"))     op=2;

     switch(op){


        case 2:

            token=strtok(NULL," :");
            strcpy(id,token);
            registro* aux =(registro *)malloc(sizeof(registro));
            registro* aux1 =(registro *)malloc(sizeof(registro));
            registro* aux2 =(registro *)malloc(sizeof(registro));
            aux=re->primero;
            int n=re->tam;
            int encontrado=0;
            while(n>=1){

                if(strcmp(id,aux->id)==0){
                    encontrado=1;
                    break;
                }
                aux=aux->siguiente;
                n--;
            }
            strcpy(path_disc,aux->path);
            inicio_part=aux->start;
            strcpy(nombre_part,aux->name);

            if(encontrado!=1){
                printf("\nNo existe una particion montada con ese id\n");
                return -1;
            }


            break;

        default:
            printf("\ncomandos invalidos\n");
            return -1;
            break;
        }


    }


    FILE *disco =fopen(path_disc,"rb+");
    fseek(disco,0,SEEK_SET);
    mbr *mbraux=(mbr*)malloc(sizeof(mbr));
    fseek(disco,0,SEEK_SET);
    fread(mbraux,sizeof(mbr),1,disco);
    if(strcmp(nombre_part,mbraux->mbr_partition1.part_name)==0){inicio_part=mbraux->mbr_partition1.part_start;}
    else if(strcmp(nombre_part,mbraux->mbr_partition2.part_name)==0){inicio_part=mbraux->mbr_partition2.part_start;}
    else if(strcmp(nombre_part,mbraux->mbr_partition3.part_name)==0){inicio_part=mbraux->mbr_partition3.part_start;}
    else if(strcmp(nombre_part,mbraux->mbr_partition4.part_name)==0){inicio_part=mbraux->mbr_partition4.part_start;}
    else{

    if(strcmp("e",mbraux->mbr_partition1.part_type)==0){inicio_part=get_inicio_logica(nombre_part,path_disc,mbraux->mbr_partition1);}
    else if(strcmp("e",mbraux->mbr_partition2.part_type)==0){inicio_part=get_inicio_logica(nombre_part,path_disc,mbraux->mbr_partition2);}
    else if(strcmp("e",mbraux->mbr_partition3.part_type)==0){inicio_part=get_inicio_logica(nombre_part,path_disc,mbraux->mbr_partition3);}
    else if(strcmp("e",mbraux->mbr_partition4.part_type)==0){inicio_part=get_inicio_logica(nombre_part,path_disc,mbraux->mbr_partition4);}

    }

    super_bloque *sb=(super_bloque*)malloc(sizeof(super_bloque));
    fseek(disco,inicio_part,SEEK_SET);
    fread(sb,sizeof(super_bloque),1,disco);
    if(sb->ext3==1){
        printf("\nParticion Ya ex ext3\n");
    }else{
        sb->ext3=1;
        printf("\nParticion Tuneada con exito\n");
        fseek(disco,inicio_part,SEEK_SET);
        fwrite(sb,sizeof(super_bloque),1,disco);
    }

    fclose(disco);
}



void addjournal(char comando[100],char id[10],char nombre[50],char operacion[20],int tipo){
    nodojournal *aux=(nodojournal*)malloc(sizeof(nodojournal));
    strcpy(aux->comando,comando);
    strcpy(aux->disco,id);
    strcpy(aux->jornal_nombre,nombre);
    strcpy(aux->journal_contenido,"");
    fecha_sistema();
    strcpy(aux->jurnal_fecha,fecha);
    strcpy(aux->journal_tipo_operacion,operacion);
    aux->jornal_tipo=tipo;
    strcpy(aux->journal_propietario,"");
    strcpy(aux->journal_permisos,"rw-rw-r--");
    aux->siguiente=NULL;
    aux->anterior=NULL;


    if(lj->tam==0){
        lj->primero=lj->ultimo=aux;
        lj->tam++;
    }
    else{
        aux->anterior=lj->ultimo;
       lj->ultimo->siguiente=aux;
       lj->ultimo=aux;
       lj->tam++;
    }

}




/*****************************************/
int loss(char *token){

    int op =0;
    char id[10];
    char path[200];
    char path_disc [200];
    char nombre_part[30];
    int inicio_part;


    while(token !=NULL){

        token=strtok(NULL," :");
        if(token==NULL) break;
        if(strstr(token,"-id"))     op=2;

     switch(op){


        case 2:

            token=strtok(NULL," :");
            strcpy(id,token);
            registro* aux =(registro *)malloc(sizeof(registro));
            registro* aux1 =(registro *)malloc(sizeof(registro));
            registro* aux2 =(registro *)malloc(sizeof(registro));
            aux=re->primero;
            int n=re->tam;
            int encontrado=0;
            while(n>=1){

                if(strcmp(id,aux->id)==0){
                    encontrado=1;
                    break;
                }
                aux=aux->siguiente;
                n--;
            }
            strcpy(path_disc,aux->path);
            inicio_part=aux->start;
            strcpy(nombre_part,aux->name);

            if(encontrado!=1){
                printf("\nNo existe una particion montada con ese id\n");
                return -1;
            }


            break;

        default:
            printf("\ncomandos invalidos\n");
            return -1;
            break;
        }


    }


    FILE *disco =fopen(path_disc,"rb+");
    fseek(disco,0,SEEK_SET);
    mbr *mbraux=(mbr*)malloc(sizeof(mbr));
    fseek(disco,0,SEEK_SET);
    fread(mbraux,sizeof(mbr),1,disco);
    if(strcmp(nombre_part,mbraux->mbr_partition1.part_name)==0){inicio_part=mbraux->mbr_partition1.part_start;}
    else if(strcmp(nombre_part,mbraux->mbr_partition2.part_name)==0){inicio_part=mbraux->mbr_partition2.part_start;}
    else if(strcmp(nombre_part,mbraux->mbr_partition3.part_name)==0){inicio_part=mbraux->mbr_partition3.part_start;}
    else if(strcmp(nombre_part,mbraux->mbr_partition4.part_name)==0){inicio_part=mbraux->mbr_partition4.part_start;}
    else{

    if(strcmp("e",mbraux->mbr_partition1.part_type)==0){inicio_part=get_inicio_logica(nombre_part,path_disc,mbraux->mbr_partition1);}
    else if(strcmp("e",mbraux->mbr_partition2.part_type)==0){inicio_part=get_inicio_logica(nombre_part,path_disc,mbraux->mbr_partition2);}
    else if(strcmp("e",mbraux->mbr_partition3.part_type)==0){inicio_part=get_inicio_logica(nombre_part,path_disc,mbraux->mbr_partition3);}
    else if(strcmp("e",mbraux->mbr_partition4.part_type)==0){inicio_part=get_inicio_logica(nombre_part,path_disc,mbraux->mbr_partition4);}

    }

    super_bloque *sb=(super_bloque*)malloc(sizeof(super_bloque));
    fseek(disco,inicio_part,SEEK_SET);
    fread(sb,sizeof(super_bloque),1,disco);
    limpia_bloque(sb,sb->s_bm_block_start,path_disc,inicio_part);
    limpia_inodos(sb,sb->s_bm_inode_start,path_disc,inicio_part);

    bloque_archivo*bba=(bloque_archivo*)malloc(sizeof(bloque_archivo));
    bloque_carpeta*bbc=(bloque_carpeta*)malloc(sizeof(bloque_carpeta));

    fseek(disco,sb->s_block_start,SEEK_SET);
    int z=1;
    for(z;z<sb->s_blocks_count;z++){
        fread(bba,sizeof(bloque_archivo),1,disco);
        fseek(disco,-sizeof(bloque_archivo),SEEK_CUR);
        fread(bbc,sizeof(bloque_carpeta),1,disco);

        if(strcmp(bba->esbloque,"si")==0 ){
            fseek(disco,-sizeof(bloque_carpeta),SEEK_CUR);
            strcpy(bba->esbloque,"NO");
            bba->num_bloque=8888888;
            fwrite(bba,sizeof(bloque_archivo),1,disco);
        }else if(strcmp(bbc->esbloque,"si")==0){
            fseek(disco,-sizeof(bloque_carpeta),SEEK_CUR);
            strcpy(bbc->esbloque,"NO");
            bbc->num_bloque=8888888;
            fwrite(bbc,sizeof(bloque_carpeta),1,disco);
        }


    }
    inodo* iin=(inodo*)malloc(sizeof(inodo));
    fseek(disco,sb->s_inode_start,SEEK_SET);
    int ku=1;
    for(ku;ku<sb->s_inodes_count;ku++){
        fread(iin,sizeof(inodo),1,disco);
        fseek(disco,-sizeof(inodo),SEEK_CUR);
        iin->num_inodo=888888888;
        fwrite(iin,sizeof(inodo),1,disco);


    }

    fclose(disco);
}


/******LIB******/
int limpia_bloque(super_bloque *superB, int i_bm_b, char path[200], int i_sis){


    FILE *sistema =fopen(path,"rb+");
    int bandera=0;//bandera.
    int i=0;//inicio for
    char aux[2]="";//char donde guardate los 1 o ceros que leere
    int num_blocks=superB->s_blocks_count;//asigo el numero de bloques que hay
    int num_bloque_usado;

    fseek(sistema,i_bm_b,SEEK_SET);//posiciono el puntero al inicio del bitmap de bloques
   // printf("\n");
    for(i;i<=num_blocks;i++){


        fread(aux,sizeof(char),1,sistema);



            fseek(sistema,-(sizeof(char)),SEEK_CUR);
            char ocupado[1]="0";
            fwrite(ocupado,sizeof(char[1]),1,sistema);
            bandera=1;

        if(strcmp(aux,"1")==0){
            superB->s_free_blocks_count=superB->s_free_blocks_count+1;



        }
        strcpy(aux,"");
    }
    fseek(sistema,i_sis,SEEK_SET);
    fwrite(superB,sizeof(super_bloque),1,sistema);

    fclose(sistema);//cierro el archivo sistema
    return num_bloque_usado;//retorno el numero de bloque que utilizare

}
int limpia_inodos(super_bloque *superB, int inicio_inodos, char path[200],int i_sis){


    FILE *sistema =fopen(path,"rb+");

    int i =0;//variable de for
    int bandera=0;//variable que indica el primer inodo libre en el bitmap
    char aux[2]="";//variable para almacenar ell caracter
    int num_inodos=superB->s_inodes_count;
    fseek(sistema,inicio_inodos,SEEK_CUR);//posiciono el puntero al inicio del bitmap de inodos
    int inodo_usado;
    for(i;i<=num_inodos;i++){

    fread(aux,sizeof(char),1,sistema);

        fseek(sistema,-(sizeof(char)),SEEK_CUR);
        char ocupado[1]="0";
        fwrite(ocupado,sizeof(char),1,sistema);
        bandera=1;


        if(strcmp(aux,"1")==0){
            superB->s_free_blocks_count=superB->s_free_blocks_count+1;
        }
        strcpy(aux,"");

    }

    fseek(sistema,i_sis,SEEK_SET);
    fwrite(superB,sizeof(super_bloque),1,sistema);

    fclose(sistema);
    return inodo_usado;

}


int recovery(char *token){

    int op =0;
    char id[10];
    char path[200];
    char path_disc [200];
    char nombre_part[30];
    int inicio_part;


    while(token !=NULL){

        token=strtok(NULL," :");
        if(token==NULL) break;
        if(strstr(token,"-id"))     op=2;

     switch(op){


        case 2:

            token=strtok(NULL," :");
            strcpy(id,token);
            registro* aux =(registro *)malloc(sizeof(registro));
            registro* aux1 =(registro *)malloc(sizeof(registro));
            registro* aux2 =(registro *)malloc(sizeof(registro));
            aux=re->primero;
            int n=re->tam;
            int encontrado=0;
            while(n>=1){

                if(strcmp(id,aux->id)==0){
                    encontrado=1;
                    break;
                }
                aux=aux->siguiente;
                n--;
            }
            strcpy(path_disc,aux->path);
            inicio_part=aux->start;
            strcpy(nombre_part,aux->name);

            if(encontrado!=1){
                printf("\nNo existe una particion montada con ese id\n");
                return -1;
            }


            break;

        default:
            printf("\ncomandos invalidos\n");
            return -1;
            break;
        }


    }


    FILE *disco =fopen(path_disc,"rb+");
    fseek(disco,0,SEEK_SET);
    mbr *mbraux=(mbr*)malloc(sizeof(mbr));
    fseek(disco,0,SEEK_SET);
    fread(mbraux,sizeof(mbr),1,disco);
    if(strcmp(nombre_part,mbraux->mbr_partition1.part_name)==0){inicio_part=mbraux->mbr_partition1.part_start;}
    else if(strcmp(nombre_part,mbraux->mbr_partition2.part_name)==0){inicio_part=mbraux->mbr_partition2.part_start;}
    else if(strcmp(nombre_part,mbraux->mbr_partition3.part_name)==0){inicio_part=mbraux->mbr_partition3.part_start;}
    else if(strcmp(nombre_part,mbraux->mbr_partition4.part_name)==0){inicio_part=mbraux->mbr_partition4.part_start;}
    else{

    if(strcmp("e",mbraux->mbr_partition1.part_type)==0){inicio_part=get_inicio_logica(nombre_part,path_disc,mbraux->mbr_partition1);}
    else if(strcmp("e",mbraux->mbr_partition2.part_type)==0){inicio_part=get_inicio_logica(nombre_part,path_disc,mbraux->mbr_partition2);}
    else if(strcmp("e",mbraux->mbr_partition3.part_type)==0){inicio_part=get_inicio_logica(nombre_part,path_disc,mbraux->mbr_partition3);}
    else if(strcmp("e",mbraux->mbr_partition4.part_type)==0){inicio_part=get_inicio_logica(nombre_part,path_disc,mbraux->mbr_partition4);}

    }

    super_bloque *sb=(super_bloque*)malloc(sizeof(super_bloque));
    fseek(disco,inicio_part,SEEK_SET);
    fread(sb,sizeof(super_bloque),1,disco);

    /***************************************************************************/
    FILE*jou=fopen("recuperacion.sh","w");
    nodojournal*nj=(nodojournal*)malloc(sizeof(nodojournal));
    nj=lj->primero;
    int t=lj->tam;

    while (t>0) {
        if(strcmp(nj->disco,id)==0){
            fprintf(jou,"%s\n",nj->comando);
        }
        nj=nj->siguiente;

        t--;
    }


    fclose(jou);
    /*****************************************************************************/


    FILE *file1;

   char *c1;
   char cadena[200]="";


   file1=fopen("recuperacion.sh","r");

   if(file1 ==NULL){
       printf("\nArchivo de Script inexistente\n");
       return -1;
   }

   while(c1=fgets(cadena,200,file1)){

   /***********************************************************************************************************/
       int  HaySalto=0;
       char CadenaSalto[200];
       char Comando1[200];

       strcpy(CadenaSalto,"");

       while(strstr(cadena,"\\")){
           char *token1 =strtok(cadena,"\\");
           strcat(CadenaSalto,token1);
           strcat(CadenaSalto," ");
           fgets(cadena,200,file1);
           HaySalto=1;
       }

       if(HaySalto==1){
           strcat(CadenaSalto,cadena);
           strcat(cadena,CadenaSalto);
           //strcat(Comando,cadena);
       }

       strcpy(CadenaSalto,"");

   /*********************************************************************************************************************************/



   if(strstr(cadena,"-")||strstr(cadena,"m")||strstr(cadena,"i")||strstr(cadena,"u")||strstr(cadena,"s")||strstr(cadena,"+")||strstr(cadena,"f")){}
   else{break;}
      printf("%s\n",cadena);
      char *quitarsalto=strtok(cadena,"\n\t");

       strcpy(cadena,quitarsalto);

       EsExec==1;
      // printf("COMANDOS LEIDOS!!!!!!--%s\n",cadena);
      leerComandos(cadena);

       EsExec=0;
   }
      fclose(file1);




    fclose(disco);
}



















































